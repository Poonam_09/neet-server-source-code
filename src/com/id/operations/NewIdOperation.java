/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.operations;

import com.db.DbConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Aniket
 */
public class NewIdOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private Statement stmt = null;
    
    public int getNewId(String tableName){
        int newId = 0;
        try {
            conn = new DbConnection().getConnection();
            stmt = conn.createStatement();
            String query = "SELECT COUNT(*) FROM "+tableName;
            rs=stmt.executeQuery(query);
            while (rs.next()) {
                newId = rs.getInt(1);
            }
            newId ++;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(stmt != null)
                    stmt.close();
                if(conn != null)
                    conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return newId;
    }
    
    public int getNewMaxId(String tableName,String columnName){
        int newId = 0;
        try {
            conn = new DbConnection().getConnection();
            stmt = conn.createStatement();
            String query = "SELECT MAX("+columnName+") FROM "+tableName;
            rs=stmt.executeQuery(query);
            while (rs.next()) {
                newId = rs.getInt(1);
            }
            newId ++;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(stmt != null)
                    stmt.close();
                if(conn != null)
                    conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return newId;
    }
    
    public int getNewId1(String tableName){
        int newId = 0;
        try {
            conn = new DbConnection().getConnection();
            stmt = conn.createStatement();
            String query = "SELECT COUNT(*) FROM "+tableName;
            rs=stmt.executeQuery(query);
            while (rs.next()) {
                newId = rs.getInt(1);
            }
            newId ++;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(stmt != null)
                    stmt.close();
                if(conn != null)
                    conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return newId;
    }
}

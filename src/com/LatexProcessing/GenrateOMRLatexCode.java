/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LatexProcessing;

/**
 *
 * @author Aniket
 */
public class GenrateOMRLatexCode {
    private int counter;
    private String optionText;
    private int stNo;
    
    public String getOmrString(int number,int startNo) {
        String start = "\\begin{tabular}{cccccccc}";
        String end = "\\end{tabular}";
        optionText = "\\sagar{\\text{\\tiny A}}& \\sagar{\\text{\\tiny B}}& \\sagar{\\text{\\tiny C}}& \\sagar{\\text{\\tiny D}}";
        counter = number;
        stNo = startNo;
        String returnString = start;
        if(number > 100) {
            
            for(int i=0; i<4; i++) {
                returnString += getFirstPage();
                if(i<3)
                    returnString += "&";
            }
            returnString += end;
            int diff = number-100;
            if(diff <= 180) {
                returnString += "\\newpage";
                returnString += start;
                for(int i=0; i<4;i++) {
                    returnString += getNormalPage();
                    if(i<3)
                        returnString += "&";
                }
                returnString += end;
            } else {
                int modValue = diff/180;
                if ((modValue * 180) >= diff) {
                    for (int j=1;j<=modValue;j++) {
                        returnString += "\\newpage";
                        returnString += start;
                        for(int i=0; i<4;i++) {
                            returnString += getNormalPage();
                            if(i<3)
                                returnString += "&";
                        }
                        returnString += end;
                    }
                } else {
                    for(int j=1;j<=modValue;j++) {
                        returnString += "\\newpage";
                        returnString += start;
                        for(int i=0; i<4;i++) {
                            returnString += getNormalPage();
                            if(i<3)
                                returnString += "&";
                        }
                        returnString += end;
                    }
                    returnString += "\\newpage";
                    returnString += start;
                    for(int i=0; i<4;i++) {
                        returnString += getNormalPage();
                        if(i<3)
                            returnString += "&";
                    }
                    returnString += end;
                }
            }
        } else {
          
            for(int i=0; i<4; i++) {
                returnString += getFirstPage();
                if(i<3)
                    returnString += "&";
            }
            returnString += end;
        }
        System.out.println("returnString="+returnString);
        return returnString;
    }
    
    private String getFirstPage() {
        String startNumber = "\\begin{tabular}{c}";
        String startOption = "\\begin{tabular}{cccc}";
        String end = "\\end{tabular}";
        String numberString = startNumber;
        String optionString = startOption;
        int cnt = 1;
        for(int i=1;i<=25;i++) {
            if(counter != 0) {
                numberString += (stNo++);
                optionString += optionText;
                counter--;
            } 
            if(cnt == 10) {
                numberString += "\\\\";
                optionString += "\\\\";
                cnt = 1;
            } else {
                cnt++;
            }
            
            numberString += "\\\\";
            optionString += "\\\\";
        }
        numberString += end;
        optionString += end;
        return numberString+"&"+optionString;
    }
    
    private String getNormalPage() {
        String startNumber = "\\begin{tabular}{c}";
        String startOption = "\\begin{tabular}{cccc}";
        String end = "\\end{tabular}";
        String numberString = startNumber;
        String optionString = startOption;
        int cnt = 1;
        for(int i=1;i<=45;i++) {
            if(counter != 0) {
                numberString += (stNo++);
                optionString += optionText;
                counter--;
            } 

            if(cnt == 10) {
                numberString += "\\\\";
                optionString += "\\\\";
                cnt = 1;
            } else {
                cnt++;
            }

            numberString += "\\\\";
            optionString += "\\\\";
        }
        numberString += end;
        optionString += end;
        return numberString+"&"+optionString;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.ClientInfoBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aniket
 */
public class ClientOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private Statement stmt = null;
    
    public int getNewId(String tableName){
        int newId = 0;
        try {
            conn = new DbConnection().getConnection();
            stmt = conn.createStatement();
            String query = "SELECT COUNT(*) FROM "+tableName;
            rs=stmt.executeQuery(query);
            while (rs.next()) {
                newId = rs.getInt(1);
            }
            newId ++;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(stmt != null)
                    stmt.close();
                if(conn != null)
                    conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return newId;
    }
    
    public  boolean SetClientList(ClientInfoBean clientBean) throws SQLException {
         boolean return1=false ;
        try {
            
            conn = new DbConnection().getConnection();
            String query = "INSERT INTO CLIENT_INFO "
                    + "(CLIENT_ID,CLIENTPCNAME,CLIENTTCP_IP_ADDRESS,DATETIME,STATUS,USER_ID)"
                    + "Values(?,?,?,?,?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, clientBean.getClientId());
            ps.setString(2, clientBean.getClientName());
            ps.setString(3, clientBean.getClientIp());
            ps.setString(4,clientBean.getDateTime());
            ps.setBoolean(5,true);
            ps.setInt(6, 0);
            ps.executeUpdate();
            return1=true;
            
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.close();
        }
       return return1;
    }
    
    public  ArrayList<ClientInfoBean> getClientList() throws SQLException {
          ArrayList<ClientInfoBean> ClientList = new ArrayList<ClientInfoBean>();
          ClientInfoBean clientBean=null;
        try {
        
            conn =new DbConnection().getConnection();
            String query = "SELECT * FROM CLIENT_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();            
            while(rs.next()) {                 
                 clientBean =new ClientInfoBean();
                 clientBean.setClientId(rs.getInt(1));
                 clientBean.setClientName(rs.getString(2));
                 clientBean.setClientIp(rs.getString(3));
                 clientBean.setDateTime(rs.getString(4));
                 System.out.println(clientBean.getClientId());                   
                 ClientList.add(clientBean);               
            }
                     
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
             
            conn.close();
        }
       return ClientList;
    }
    
    public boolean ResetClientList() {
         conn = new DbConnection().getConnection();
         boolean Status=false;
        try {
            if (conn != null) {
                String sql = "DELETE FROM CLIENT_INFO";
                 ps = conn.prepareStatement(sql);
                 ps.executeUpdate();
                 Status=true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClientOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ClientOperation.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return Status;
    }
}

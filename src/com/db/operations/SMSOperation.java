/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.Model.SwapData;
import com.bean.RegistrationBean;
import com.bean.SMSBean;
import com.bean.SenderIdBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class SMSOperation {
    
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private Statement stmt = null;
    
    public  ArrayList<SMSBean > getSMSInfoList() throws SQLException {
          ArrayList<SMSBean > smsList = new ArrayList<SMSBean>();
          SMSBean smsBean=null;
        try {
        
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM SMS_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();            
            while(rs.next()) {                 
                 smsBean =new SMSBean();
                 smsBean.setSid(rs.getInt(1));
                 smsBean.setPackages(rs.getInt(2));
                 smsBean.setSend(rs.getInt(3));
                 smsBean.setRemaining(rs.getInt(4));                  
                 smsList.add(smsBean);
            }
                      
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.close();
        }
       return smsList;
    }
    public int getNewSId(){
        int newSId = 0;
        try {
            conn = new DbConnection().getConnection();
            stmt = conn.createStatement();
            String query = "SELECT COUNT(*) FROM SMS_INFO";
            rs=stmt.executeQuery(query);
            while (rs.next()) {
                newSId = rs.getInt(1);
            }
            newSId ++;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(stmt != null)
                    stmt.close();
                if(conn != null)
                    conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return newSId;
    }
    
     public  boolean InsertSMSInfo(SMSBean smsBean) throws SQLException {
         boolean return1=false ;
        try {
            
            conn = new DbConnection().getConnection();
            String query = "INSERT INTO SMS_INFO"
                    + "(SID,PACKAGE,SEND,REAMAING)"
                    + "Values(?,?,?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, smsBean.getSid());
            ps.setInt(2, smsBean.getPackages());
            ps.setInt(3, smsBean.getSend());
            ps.setInt(4, smsBean.getRemaining());
            ps.executeUpdate();
            return1=true;
            
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.close();
        }
       return return1;
    }
    
    public boolean updateSmsPackage( SMSBean smsBean) throws SQLException {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE SMS_INFO SET PACKAGE = ? ,SEND = ? , REAMAING = ? WHERE SID = ?";
//            String query = "UPDATE SMS_INFO SET PACKAGE = ? WHERE SID = ?";
            ps = conn.prepareStatement(query);            
            ps.setInt(1, smsBean.getPackages());
            ps.setInt(2, smsBean.getSend());
            ps.setInt(3, smsBean.getRemaining());
            ps.setInt(4, smsBean.getSid());
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            conn.close();
        }
        return returnValue;
    }
}

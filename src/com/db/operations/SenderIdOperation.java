/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.ClientInfoBean;
import com.bean.SMSBean;
import com.bean.SenderIdBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class SenderIdOperation {
    
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private Statement stmt = null;
    
     public  String getSenderId(String SenderId1) throws SQLException {
          String SenderId=null;
          
        try {
        
            conn = new DbConnection().getConnection();
            String query = "SELECT SENDER_ID FROM SENDERID_INFO WHERE SENDER_ID= '"+SenderId1+"'";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();            
            while(rs.next()) {                 
                SenderId= rs.getString(1);
            }
                      
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.close();
        }
       return SenderId;
    }
    
    public  ArrayList<SenderIdBean> getSenderIdList() throws SQLException {
          ArrayList<SenderIdBean> senderIdList = new ArrayList<SenderIdBean>();
          SenderIdBean senderIdBean=null;
//          boolean flag=true;
        try {
        
            conn = new DbConnection().getConnection();
//            String query = "SELECT * FROM SENDERID_INFO WHERE FLAG="+flag;
            String query = "SELECT * FROM SENDERID_INFO ";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();            
            while(rs.next()) {                 
                 senderIdBean =new SenderIdBean();
                 senderIdBean.setSid(rs.getInt(1));
                 senderIdBean.setSenderId(rs.getString(2));
                 senderIdBean.setUserName(rs.getString(3));
                 senderIdBean.setPassword(rs.getString(4)); 
                 senderIdBean.setFlag(rs.getBoolean(5)); 
                 senderIdList.add(senderIdBean);
            }
                      
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.close();
        }
       return senderIdList;
    }
    public int getNewSId(){
        int newSId = 0;
        try {
            conn = new DbConnection().getConnection();
            stmt = conn.createStatement();
            String query = "SELECT COUNT(*) FROM SENDERID_INFO";
            rs=stmt.executeQuery(query);
            while (rs.next()) {
                newSId = rs.getInt(1);
            }
            newSId ++;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(stmt != null)
                    stmt.close();
                if(conn != null)
                    conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return newSId;
    }
    
     public  ArrayList<SenderIdBean> getSenderIdbyflagList() throws SQLException {
          ArrayList<SenderIdBean> senderIdList = new ArrayList<SenderIdBean>();
          SenderIdBean senderIdBean=null;
          boolean flag=true;
        try {
        
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM SENDERID_INFO WHERE FLAG="+flag;
           
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();            
            while(rs.next()) {                 
                 senderIdBean =new SenderIdBean();
                 senderIdBean.setSid(rs.getInt(1));
                 senderIdBean.setSenderId(rs.getString(2));
                 senderIdBean.setUserName(rs.getString(3));
                 senderIdBean.setPassword(rs.getString(4)); 
                 senderIdBean.setFlag(rs.getBoolean(5)); 
                 senderIdList.add(senderIdBean);
            }
                      
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.close();
        }
       return senderIdList;
    }
    
     public  boolean InsertSenderIdInfo(SenderIdBean senderIdBean) throws SQLException {
         boolean return1=false ;
        try {
            
            conn = new DbConnection().getConnection();
            String query = "INSERT INTO SENDERID_INFO"
                    + "(SID,SENDER_ID,USERNAME,PASSWORD,FLAG)"
                    + "Values(?,?,?,?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, senderIdBean.getSid());
            ps.setString(2, senderIdBean.getSenderId());
            ps.setString(3, senderIdBean.getUserName());
            ps.setString(4,senderIdBean.getPassword());
            ps.setBoolean(5,senderIdBean.isFlag());
            ps.executeUpdate();
            return1=true;
            
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.close();
        }
       return return1;
    }
    
    public boolean updateSenderId( SenderIdBean senderIdBean) throws SQLException {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE SENDERID_INFO SET USERNAME=?,PASSWORD=? WHERE SENDER_ID = ?";
//            String query = "UPDATE SMS_INFO SET PACKAGE = ? WHERE SID = ?";
            ps = conn.prepareStatement(query);            
            ps.setString(1,senderIdBean.getUserName());
            ps.setString(2,senderIdBean.getPassword());
            ps.setString(3,senderIdBean.getSenderId());
           
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            conn.close();
        }
        return returnValue;
    }
     
     public boolean updateSenderIdStatus( String SenderId) throws SQLException {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE SENDERID_INFO SET FLAG=? WHERE SENDER_ID = '"+SenderId+"'";
//            String query = "UPDATE SMS_INFO SET PACKAGE = ? WHERE SID = ?";
            ps = conn.prepareStatement(query);            
            ps.setBoolean(1,true);
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            conn.close();
        }
        return returnValue;
    }
    
    public boolean updateAllSenderIdStatus() throws SQLException {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE SENDERID_INFO SET FLAG=? WHERE FLAG ";

            ps = conn.prepareStatement(query);            
            ps.setBoolean(1,false);
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            conn.close();
        }
        return returnValue;
    }
}

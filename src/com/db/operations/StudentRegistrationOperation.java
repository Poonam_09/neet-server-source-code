/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.PrintedTestBean;
import com.bean.RankBean;
import com.bean.RegistrationBean;
import com.bean.StudentBean;
import com.db.DbConnection;
import javax.swing.*;
import com.id.operations.NewIdOperation;
import com.pages.HomePage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

/**
 *
 * @author Aniket
 */
public class StudentRegistrationOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null,ps1=null,ps2=null;
    private Statement stmt=null,stmt1=null;
    
    public ArrayList<StudentBean> getStudentInfo() {
        ArrayList<StudentBean> studentList = new ArrayList<StudentBean>();
        conn = new DbConnection().getConnection();
        try {
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("select * from Student_Info order by rollno");
            while (rs.next()) {
            //VARCHAR(30) NOT NULL,academicYear                
                StudentBean studentBean1 = new StudentBean();
                studentBean1.setRollno(rs.getInt(1));
                studentBean1.setFullname(rs.getString(2));
                studentBean1.setPass(rs.getString(3));
                studentBean1.setStandard(rs.getString(4));
                studentBean1.setDivision(rs.getString(5));
                studentBean1.setAcademicYear(rs.getString(6));
                studentBean1.setStudentName(rs.getString(7));
                studentBean1.setMobileNo(rs.getString(8));
                studentList.add(studentBean1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return studentList;
    }
    
//    public boolean checkStudentName(String fullname, String std, String div) {
//        conn = new DbConnection().getConnection();
//        boolean ret = true;
//        try {
//            Statement ss = conn.createStatement();
//            ResultSet rr = ss.executeQuery("Select * from Student_Info where standard='" + std + "' and division = '" + div + "' ");
//            while (rr.next()) {
//                if (rr.getString(2).equals(fullname)) {
//                    ret = false;
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//        return ret;
//    }
    
    
    public boolean checkStudentName(String fullname) {
        conn = new DbConnection().getConnection();
        boolean ret = true;
        try {
            Statement ss = conn.createStatement();
            ResultSet rr = ss.executeQuery("Select * from Student_Info where FULLNAME='" + fullname + "'");
            while (rr.next()) {
              
                    ret = false;
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return ret;
    }
     public void deleteAll() throws SQLException{
         String query;
          conn = new DbConnection().getConnection();
          query = "DELETE FROM STUDENT_INFO";
          Statement st = conn.createStatement();
          st.executeUpdate(query);
    }
    
    public boolean insertStudentRegistration(StudentBean studentBean1) {
        boolean receivedValue = false;       
        String FullName=studentBean1.getFullname();
        String Std=studentBean1.getStandard();
        String Div=studentBean1.getDivision();
        receivedValue = checkStudentName(FullName);
        int RollNo1=studentBean1.getRollno();
        
        if (receivedValue == true) {
                  conn = new DbConnection().getConnection();
        try {
            if (conn != null) {
            Statement s = conn.createStatement();
            String query="Select * from Student_Info where rollno=?";
            ps = conn.prepareStatement(query);
            ps.setInt(1,RollNo1);
            ResultSet rs = ps.executeQuery();
            ResultSet rs1;
            
            if (!rs.next()) 
            {
               query="insert into Student_Info values(?,?,?,?,?,?,?,?)";
               ps = conn.prepareStatement(query);
               ps.setInt(1,RollNo1);
               ps.setString(2,studentBean1.getFullname());
               ps.setString(3,studentBean1.getPass());               
               ps.setString(4,studentBean1.getStandard());
               ps.setString(5,studentBean1.getDivision());
               ps.setString(6,studentBean1.getAcademicYear());
               ps.setString(7,studentBean1.getStudentName());
               ps.setString(8,studentBean1.getMobileNo());
               int executeUpdate = ps.executeUpdate();
                
                if (executeUpdate > 0) {
                            String lastalpha = "";
                            int maxroll = 0;
                            rs1 = s.executeQuery("select max(rollno) from Set_Rollno_Distribution");
                            //setInitials
                            if (rs1.next()) {
                                maxroll = rs1.getInt(1);
                            }
                            Statement s2 = conn.createStatement();
                            ResultSet rs2 = s2.executeQuery("select setInitials from Set_Rollno_Distribution where rollno=" + maxroll);
                            if (rs2.next()) {
                                lastalpha = rs2.getString(1);
                            }
                            if (lastalpha == null) {
                                lastalpha = "A";
                            }
                            if (lastalpha.equals("A")) {
                                lastalpha = "B";
                            } else if (lastalpha.equals("B")) {
                                lastalpha = "C";
                            } else if (lastalpha.equals("C")) {
                                lastalpha = "D";
                            } else if (lastalpha.equals("D")) {
                                lastalpha = "A";
                            } else {
                                lastalpha = "A";
                            }
                            s.execute("insert into Set_Rollno_Distribution values(" +RollNo1+ ",'" + lastalpha + "')");
//                          
                            HomePage homepage = new HomePage(); 
                            homepage.clearFields();
//                            JOptionPane.showMessageDialog(null, "Student registered Successfully");
                            return true;
                            
                        }else {
                            JOptionPane.showMessageDialog(null, "Unable to register Student. Please Try again");
                            return false;
                              }
            }else {
                        JOptionPane.showMessageDialog(null, "Student with this LoginID is already registered");
                  }
            }else {
                    JOptionPane.showMessageDialog(null, "Unable to register Student. Please Try again");
                   }
            
            
        } catch (SQLException ex) {
                Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if (conn!= null) {
                        conn.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "This UserID is Already Registered.");
        }
        return false;
        
    }
    
    public int setAutoRollNo() {
        conn = new DbConnection().getConnection();
        int rollno = 0;
        try {
            
            if (conn != null) {
                Statement s = conn.createStatement();
                ResultSet rs = s.executeQuery("Select MAX(rollno) from Student_Info");
                if (rs.next()) {
                    rollno = rs.getInt(1);
                }
                rollno++;
               // txtRollNo.setText(rollno + "");
               return rollno;
            } else {
                JOptionPane.showMessageDialog(null, "Unable to register Student. Please Try again");
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return rollno;
    }
    
    public String getNameOfStudent(int rollNo) {
        String name = "";
        conn = new DbConnection().getConnection();

        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select fullname from Student_Info where rollno=" + rollNo);
            if (rs.next()) {
                name = rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return name;
    }
    
    public ArrayList<String> getStudentdetails(int rollno) {
        ArrayList<String> studDetails = new ArrayList<String>();
        String name = "";
        conn = new DbConnection().getConnection();
        try {
            stmt = conn.createStatement();
            //rollno,name,pwd,std,div,year
             rs = stmt.executeQuery("select * from Student_Info where rollno=" + rollno);
            if (rs.next()) {
                studDetails.add(rs.getString(1));
                studDetails.add(rs.getString(2));
                studDetails.add(rs.getString(3));
                studDetails.add(rs.getString(4));
                studDetails.add(rs.getString(5));
                studDetails.add(rs.getString(6));
                studDetails.add(rs.getString(7));
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return studDetails;
    }
    
    public StudentBean getStudentdetails1(int rollno) {
       StudentBean studDetails = new StudentBean();
       String RollNO=Integer.toString(rollno);
        String name = "";
        conn = new DbConnection().getConnection();
        try {
            stmt1 = conn.createStatement();
            
            stmt = conn.createStatement();
            //rollno,name,pwd,std,div,year
             rs = stmt.executeQuery("select * from Student_Info where FULLNAME='" + RollNO+ "'");
            if (rs.next()) {
                studDetails.setRollno(rs.getInt(1));
                studDetails.setFullname(rs.getString(2));
                studDetails.setPass(rs.getString(3));
                studDetails.setStandard(rs.getString(4));
                studDetails.setDivision(rs.getString(5));
                studDetails.setAcademicYear(rs.getString(6));
                studDetails.setStudentName(rs.getString(7));
                studDetails.setMobileNo(rs.getString(8));
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return studDetails;
    }
    
    public String getNameOfStudent1(int rollno) {
       // ArrayList<String> studDetails = new ArrayList<String>();
        String name = "";
        String RollNO=Integer.toString(rollno);
        conn = new DbConnection().getConnection();
        try {
            stmt = conn.createStatement();
            //rollno,name,pwd,std,div,year
             rs = stmt.executeQuery("select * from Student_Info where FULLNAME='" + RollNO+ "'");
            if (rs.next()) {
                  name= rs.getString(7);
                  return name;
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return name;
    }
    
     public ArrayList< StudentBean> getAllInfoStudent(int rollno) {
        ArrayList<StudentBean> studentBeanList = null;
        StudentBean studentBean=null;
        String RollNO=Integer.toString(rollno);
        conn = new DbConnection().getConnection();
        try {
            stmt = conn.createStatement();
            //rollno,name,pwd,std,div,year
             rs = stmt.executeQuery("select * from Student_Info where FULLNAME='" + RollNO+ "'");
            if (rs.next()) {
                 studentBeanList = new ArrayList<StudentBean>();
                 studentBean=new StudentBean();
                 studentBean.setRollno(rs.getInt(1));
                 studentBean.setFullname(rs.getString(2));
                 studentBean.setPass(rs.getString(3));
                 studentBean.setStandard(rs.getString(4));
                 studentBean.setDivision(rs.getString(5));
                 studentBean.setAcademicYear(rs.getString(6));
                 studentBean.setStudentName(rs.getString(7));
                 studentBean.setMobileNo(rs.getString(8));
                 studentBeanList.add(studentBean);
                 return studentBeanList;
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return studentBeanList;
    }
     
    public StudentBean getAllInfoStudent1(int rollno) {
        
        StudentBean studentBean=null;
        String RollNO=Integer.toString(rollno);
        conn = new DbConnection().getConnection();
        try {
            stmt = conn.createStatement();
            //rollno,name,pwd,std,div,year
             rs = stmt.executeQuery("select * from Student_Info where FULLNAME='" + RollNO+ "'");
            if (rs.next()) {
                 
                 studentBean=new StudentBean();
                 studentBean.setRollno(rs.getInt(1));
                 studentBean.setFullname(rs.getString(2));
                 studentBean.setPass(rs.getString(3));
                 studentBean.setStandard(rs.getString(4));
                 studentBean.setDivision(rs.getString(5));
                 studentBean.setAcademicYear(rs.getString(6));
                 studentBean.setStudentName(rs.getString(7));
                 studentBean.setMobileNo(rs.getString(8));
                 
                 return studentBean;
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return studentBean;
    } 
    
    public boolean deleteStudent(StudentBean studentBean1)  {
        
        boolean returnValue = false;
        int lastId = new NewIdOperation().getNewId1("STUDENT_INFO");
        lastId -= 1;
        try {
            int currentTestId = studentBean1.getRollno();
            conn = new DbConnection().getConnection();
            String query;
            
            
            query = "DELETE FROM SET_ROLLNO_DISTRIBUTION WHERE ROLLNO = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, currentTestId);
            ps.executeUpdate();
            
            if(lastId != currentTestId) {
                for(int i=currentTestId; i<lastId; i++) {
                    query = "UPDATE SET_ROLLNO_DISTRIBUTION SET ROLLNO = ? WHERE ROLLNO = ?";
                    ps = conn.prepareStatement(query);
                    ps.setInt(1, i); 
                    ps.setInt(2, i + 1);
                    ps.executeUpdate();
                }
            }
            
            String UserId=getUserIDStudent1(currentTestId);
            
            System.out.println("***UserId" +UserId);
            int userid=Integer.parseInt(UserId);
            System.out.println("234567*UserId" +UserId);
            
//            int TestId= getTestIdStudent1(userid);
//            System.out.println("*TestId*="+TestId);
//            conn = new DbConnection().getConnection();
            
//            query = "DELETE FROM CLASS_TEST_INFO WHERE TEST_ID = ?";
//            ps = conn.prepareStatement(query);
//            ps.setInt(1, TestId);
//            ps.executeUpdate();
            
//            conn = new DbConnection().getConnection();
//            query = "DELETE FROM CLASS_TEST_QUESTION_INFO WHERE TESTID = ?";
//            ps = conn.prepareStatement(query);
//            ps.setInt(1, TestId);
//            ps.executeUpdate();
//            
//            query = "DELETE FROM TEMP_CLASS_TEST_INFO WHERE TEST_ID = ?";
//            ps = conn.prepareStatement(query);
//            ps.setInt(1, TestId);
//            ps.executeUpdate();
//            
//            query = "DELETE FROM TEMP_CLASS_TEST_QUESTION_INFO WHERE TESTID = ?";
//            ps = conn.prepareStatement(query);
//            ps.setInt(1, TestId);
//            ps.executeUpdate();
            
            conn = new DbConnection().getConnection();
            query = "DELETE FROM CLASS_TEST_QUESTION_STATUS_INFO WHERE ROLLNO = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.executeUpdate();
            
            query = "DELETE FROM CLASS_TEST_RESULT_INFO WHERE ROLLNO = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.executeUpdate();
            
            conn = new DbConnection().getConnection();
            query = "DELETE FROM TOTALMARK WHERE ROLLNO = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.executeUpdate();
           
            query = "DELETE FROM STUDENTTESTBEAN WHERE ROLLNO = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.executeUpdate();
            
            
            
            query = "DELETE FROM STUDENT_INFO WHERE ROLLNO = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, currentTestId);
            ps.executeUpdate();
            
            if(lastId != currentTestId) {
                for(int i=currentTestId; i<lastId; i++) {
                    query = "UPDATE STUDENT_INFO SET ROLLNO = ? WHERE ROLLNO = ?";
                    ps = conn.prepareStatement(query);
                    ps.setInt(1, i);  
                    ps.setInt(2, i + 1);
                    ps.executeUpdate();
                }
            }
            
            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
           sqlClose();
        }
        return returnValue;
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
     public boolean deleteStudentList(ArrayList<StudentBean> studentbeanList) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
             String query;
             
             query = "DELETE FROM CLASS_TEST_QUESTION_STATUS_INFO WHERE ROLLNO = ?";
                       
            for(StudentBean studentBean1 : studentbeanList) {
                int RollNo = studentBean1.getRollno();
                System.out.println("CLASS_TEST_QUESTION_STATUS_INFO RollNo" +RollNo);
                String UserId=getUserIDStudent1(RollNo);
                int userid=Integer.parseInt(UserId);
                System.out.println("CLASS_TEST_QUESTION_STATUS_INFO UserId" +userid);
                ps = conn.prepareStatement(query);
                ps.setInt(1,userid );
                ps.executeUpdate();
            }
            
            query = "DELETE FROM CLASS_TEST_RESULT_INFO WHERE ROLLNO = ?";
                       
            for(StudentBean studentBean1 : studentbeanList) {
                int RollNo = studentBean1.getRollno();
                System.out.println("CLASS_TEST_RESULT_INFO RollNo" +RollNo);
                String UserId=getUserIDStudent1(RollNo);
                int userid=Integer.parseInt(UserId);
                System.out.println("CLASS_TEST_RESULT_INFO UserId" +userid);
                ps = conn.prepareStatement(query);
                ps.setInt(1,userid );
                ps.executeUpdate();
            }
            
             query = "DELETE FROM TOTALMARK WHERE ROLLNO = ?";
                       
            for(StudentBean studentBean1 : studentbeanList) {
                int RollNo = studentBean1.getRollno();
                System.out.println("TOTALMARK RollNo" +RollNo);
                String UserId=getUserIDStudent1(RollNo);
                int userid=Integer.parseInt(UserId);
                System.out.println("TOTALMARK UserId" +UserId);
                ps = conn.prepareStatement(query);
                ps.setInt(1,userid );
                ps.executeUpdate();
            }
            
             query = "DELETE FROM STUDENTTESTBEAN WHERE ROLLNO = ?";
                       
            for(StudentBean studentBean1 : studentbeanList) {
                int RollNo = studentBean1.getRollno();
                String UserId=getUserIDStudent1(RollNo);
                int userid=Integer.parseInt(UserId);
                System.out.println(" STUDENTTESTBEAN UserId" +userid);
                ps = conn.prepareStatement(query);
                ps.setInt(1,userid );
                ps.executeUpdate();
            }
            
            query = "DELETE FROM SET_ROLLNO_DISTRIBUTION WHERE ROLLNO = ?";
            for(StudentBean studentBean1 : studentbeanList) {
                ps = conn.prepareStatement(query);
                ps.setInt(1, studentBean1.getRollno());
                ps.executeUpdate();
            }
            
            query = "UPDATE SET_ROLLNO_DISTRIBUTION SET ROLLNO = ? WHERE ROLLNO = ?";
            int totalDeleted1 = studentbeanList.size();
            for(int i=1;i<=50;i++) {
                ps = conn.prepareStatement(query);
                ps.setInt(1, i);
                ps.setInt(2, i + totalDeleted1);
                ps.executeUpdate();
            }
            
            query = "DELETE FROM STUDENT_INFO WHERE ROLLNO = ?";
                       
            for(StudentBean studentBean1 : studentbeanList) {
                ps = conn.prepareStatement(query);
                ps.setInt(1, studentBean1.getRollno());
                ps.executeUpdate();
            }
            
            query = "UPDATE STUDENT_INFO SET ROLLNO = ? WHERE ROLLNO = ?";
            int totalDeleted = studentbeanList.size();
            for(int i=1;i<=50;i++) {
                ps = conn.prepareStatement(query);
                ps.setInt(1, i);  
                ps.setInt(2, i + totalDeleted);
                ps.executeUpdate();
            }
            
            
            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
     
      public String getUserIDStudent1(int rollno) 
      {
            System.out.println("my"+rollno);
          String  Rollno=null;
          conn = new DbConnection().getConnection();
          try {
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("select * from Student_Info where rollno=" +rollno);
             System.out.println("Rollno"+rs);
            while (rs.next()) {
                
                 Rollno = rs.getString(2);
                 System.out.println("Rollno");
                 return Rollno;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
          return Rollno;
      }
      
      public int getTestIdStudent1(int rollno) 
      {
          System.out.println("myrollno"+rollno);
          int  TestId = 0;
          conn = new DbConnection().getConnection();
          try {
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("select * from CLASS_TEST_QUESTION_STATUS_INFO where rollno=" +rollno);
             
            while (rs.next()) {
                
                 TestId = rs.getInt(1);
                 System.out.println("TestId="+TestId);
                 return TestId;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
          return TestId;
      }
      
      public boolean updateStudentBean(StudentBean studentBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE STUDENT_INFO SET FULLNAME = ?,PASS = ?,STANDARD = ?,DIVISION = ?,ACADEMICYEAR = ?,STUDENTNAME = ?,MOBILE_NO = ? WHERE ROLLNO = ?";
            ps = conn.prepareStatement(query);
            ps.setString(1, studentBean.getFullname());
            ps.setString(2,studentBean.getPass());
            ps.setString(3, studentBean.getStandard());
            ps.setString(4, studentBean.getDivision());
            ps.setString(5, studentBean.getAcademicYear());
            ps.setString(6,studentBean.getStudentName());
            ps.setString(7,studentBean.getMobileNo());
            ps.setInt(8,studentBean.getRollno());
            
            ps.executeUpdate();
            
            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnValue;
    }
      
      
      public ArrayList< StudentBean> getAllInfoStudentagain(ArrayList<RankBean> rankList) {
        ArrayList<StudentBean> studentBeanList = null;
        StudentBean studentBean=null;
        
        
        
        try {
            for(int i=0;i<rankList.size();i++) {
            conn = new DbConnection().getConnection();
            stmt = conn.createStatement();
            //rollno,name,pwd,std,div,year
            
                System.out.println(rankList.get(i).getRollNo());   
             rs = stmt.executeQuery("select * from Student_Info where FULLNAME='" + rankList.get(i).getRollNo()+ "'");
            if (rs.next()) {
                
                 studentBeanList = new ArrayList<StudentBean>();
                 studentBean=new StudentBean();
                 studentBean.setRollno(rs.getInt(1));
                 studentBean.setFullname(rs.getString(2));
                 studentBean.setPass(rs.getString(3));
                 studentBean.setStandard(rs.getString(4));
                 studentBean.setDivision(rs.getString(5));
                 studentBean.setAcademicYear(rs.getString(6));
                 studentBean.setStudentName(rs.getString(7));
                 studentBean.setMobileNo(rs.getString(8));
                 studentBeanList.add(studentBean);
                 
            }
            }
            
           
        } catch (SQLException ex) {
            Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return studentBeanList;
    }
      
   public ArrayList<StudentBean> getStudentInfoByStandard(String Standard) {
        ArrayList<StudentBean> studentList = new ArrayList<StudentBean>();
        conn = new DbConnection().getConnection();
        try {
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("select * from Student_Info where STANDARD = '" + Standard + "' order by rollno");
            while (rs.next()) {
            //VARCHAR(30) NOT NULL,academicYear                
                StudentBean studentBean1 = new StudentBean();
                studentBean1.setRollno(rs.getInt(1));
                studentBean1.setFullname(rs.getString(2));
                studentBean1.setPass(rs.getString(3));
                studentBean1.setStandard(rs.getString(4));
                studentBean1.setDivision(rs.getString(5));
                studentBean1.setAcademicYear(rs.getString(6));
                studentBean1.setStudentName(rs.getString(7));
                studentBean1.setMobileNo(rs.getString(8));
                studentList.add(studentBean1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return studentList;
    }   
   
   public ArrayList<StudentBean> getStudentInfoByStandardDivision(String Standard,String Division) {
        ArrayList<StudentBean> studentList = new ArrayList<StudentBean>();
        conn = new DbConnection().getConnection();
        try {
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("select * from Student_Info where STANDARD = '" + Standard + "' And  DIVISION ='" + Division + "' order by rollno");
            while (rs.next()) {
            //VARCHAR(30) NOT NULL,academicYear                
                StudentBean studentBean1 = new StudentBean();
                studentBean1.setRollno(rs.getInt(1));
                studentBean1.setFullname(rs.getString(2));
                studentBean1.setPass(rs.getString(3));
                studentBean1.setStandard(rs.getString(4));
                studentBean1.setDivision(rs.getString(5));
                studentBean1.setAcademicYear(rs.getString(6));
                studentBean1.setStudentName(rs.getString(7));
                studentBean1.setMobileNo(rs.getString(8));
                studentList.add(studentBean1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return studentList;
    }   
   
   public ArrayList<StudentBean> getStudentInfoByStandardDivisionAcdemicYear(String Standard,String Division,String AcdemicYear) {
        ArrayList<StudentBean> studentList = new ArrayList<StudentBean>();
        conn = new DbConnection().getConnection();
        try {
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("select * from Student_Info where STANDARD = '" + Standard + "' And  DIVISION ='" + Division + "' And ACADEMICYEAR = '" + AcdemicYear + "'  order by rollno");
            while (rs.next()) {
            //VARCHAR(30) NOT NULL,academicYear                
                StudentBean studentBean1 = new StudentBean();
                studentBean1.setRollno(rs.getInt(1));
                studentBean1.setFullname(rs.getString(2));
                studentBean1.setPass(rs.getString(3));
                studentBean1.setStandard(rs.getString(4));
                studentBean1.setDivision(rs.getString(5));
                studentBean1.setAcademicYear(rs.getString(6));
                studentBean1.setStudentName(rs.getString(7));
                studentBean1.setMobileNo(rs.getString(8));
                studentList.add(studentBean1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return studentList;
    }   
   
  public boolean updateStudentSameUserIdBean(StudentBean studentBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE STUDENT_INFO SET PASS = ?,STANDARD = ?,DIVISION = ?,ACADEMICYEAR = ?,STUDENTNAME = ?,MOBILE_NO = ? WHERE ROLLNO = ?";
            ps = conn.prepareStatement(query);
//            ps.setString(1, studentBean.getFullname());
            ps.setString(1,studentBean.getPass());
            ps.setString(2, studentBean.getStandard());
            ps.setString(3, studentBean.getDivision());
            ps.setString(4, studentBean.getAcademicYear());
            ps.setString(5,studentBean.getStudentName());
            ps.setString(6,studentBean.getMobileNo());
            ps.setInt(7,studentBean.getRollno());
            
            ps.executeUpdate();
            
            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnValue;
    }
}

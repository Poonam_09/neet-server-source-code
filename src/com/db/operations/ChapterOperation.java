/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.ChapterBean;
import com.bean.ChapterViewBean;
import com.bean.MasterChapterCountBean;
import com.bean.QuestionBean;
import com.bean.SubMasterChapterBean;
import com.bean.SubjectBean;
import com.bean.ViewChapterBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author Aniket
 */
public class ChapterOperation {
    
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private Statement stmt = null;
    private ArrayList<SubMasterChapterBean>SubMasterchapterList;
    private ArrayList<SubMasterChapterBean> updatedChapterList;
    public ChapterBean getChapterBean(int chapterId) {
        ChapterBean returnBean = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM CHAPTER_INFO WHERE CHAPTER_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, chapterId);
            rs = ps.executeQuery();
            while(rs.next()) {
                returnBean = new ChapterBean();
                returnBean.setChapterId(rs.getInt(1));
                returnBean.setSubjectId(rs.getInt(2));
                returnBean.setChapterName(rs.getString(3));   
                returnBean.setWeightage(rs.getInt(4));
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnBean;
    }
    
    public void updateChapterWeightage(Vector rows, ArrayList<SubMasterChapterBean>SubMasterchapterList,int currentPatternIndex) {
          updatedChapterList=null;
        boolean returnValue = false;
         if (rows != null) {
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE CHAPTER_INFO SET WEIGHTAGE =? WHERE CHAPTER_NAME =?";
                ps = conn.prepareStatement(query);
                     
                  int size=rows.size();
           for (int i = 0; i < size; i++) 
            {  
              Vector row = (Vector) rows.elementAt(i);
                   SubMasterChapterBean scb=null;
                    scb = new SubMasterChapterBean();
                    for(SubMasterChapterBean chapterBean : SubMasterchapterList) 
                    { 
                       if(chapterBean.getChapterName().equals(row.elementAt(1).toString())) 
                         {
                            if(chapterBean.getWeightage() != (Integer) row.elementAt(2)) 
                               {
                                   scb.setChapterId(chapterBean.getChapterId());
                                   scb.setChapterName(chapterBean.getChapterName().trim());
                                   scb.setWeightage((Integer) row.elementAt(2));
                                   ps.setInt(1,(Integer) row.elementAt(2));
                                   ps.setString(2,row.elementAt(1).toString());
                                   ps.executeUpdate();
                                   if(updatedChapterList == null)
                                       updatedChapterList = new ArrayList<SubMasterChapterBean>();
                                       updatedChapterList.add(scb);
                                       returnValue = true;
                                       //System.out.println("returnValue="+returnValue);
                               }
                         }
                    }
            }    
            
           
        } catch(Exception ex) {
       
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
      
      }
        
    }
   
    public ArrayList<ChapterBean> getChapterList(int subjectId) {
        ArrayList<ChapterBean> returnList = new ArrayList<ChapterBean>();
        
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM CHAPTER_INFO WHERE SUBJECT_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, subjectId);
            rs = ps.executeQuery();
            ChapterBean chapterBean = null;
            while(rs.next()) {
                chapterBean = new ChapterBean();
                chapterBean.setChapterId(rs.getInt(1));
                chapterBean.setSubjectId(rs.getInt(2));
                chapterBean.setChapterName(rs.getString(3));   
                chapterBean.setWeightage(rs.getInt(4));
                returnList.add(chapterBean);
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnList;
    }
    
    public ArrayList<ChapterBean> getChapterSummaryList(int subjectId) {
        ArrayList<ChapterBean> returnList = new ArrayList<ChapterBean>();
        conn = new DbConnection().getConnection();
        int chapterId = 1;
        try {
            String query = "SELECT * FROM SUB_MASTER_CHAPTER_INFO WHERE GROUP_ID = ? AND SUBJECT_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, 1);
            ps.setInt(2, subjectId);
            rs = ps.executeQuery();
            
            ChapterBean chapterBean = null;
            while(rs.next()) {
                chapterBean = new ChapterBean();
                chapterBean.setChapterId(chapterId++);
                chapterBean.setChapterName(rs.getString(2)); 
                returnList.add(chapterBean);
            }
        } catch(Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnList;
    }
    
    public ArrayList<ChapterBean> getChaptersList(ArrayList<Integer> chaptersIdList) {
        ArrayList<ChapterBean> returnList = new ArrayList<ChapterBean>();
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM CHAPTER_INFO WHERE CHAPTER_ID = ?";
            ps = conn.prepareStatement(query);
            for(int chapterId : chaptersIdList) {
                ps.setInt(1, chapterId);
                rs = ps.executeQuery();
                ChapterBean chapterBean = null;
                while(rs.next()) {
                    chapterBean = new ChapterBean();
                    chapterBean.setChapterId(rs.getInt(1));
                    chapterBean.setSubjectId(rs.getInt(2));
                    chapterBean.setChapterName(rs.getString(3));
                    chapterBean.setWeightage(rs.getInt(4));
                    returnList.add(chapterBean);
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        finally{
            sqlClose();
        }
        return returnList;
    }
    
    public ArrayList<ViewChapterBean> getChaptersViewList(int subjectId) {

        ArrayList<QuestionBean> questionsList = new QuestionOperation().getQuestuionsSubjectWise(subjectId);
        ArrayList<ViewChapterBean> returnList = new ArrayList<ViewChapterBean>();
        ArrayList<ChapterBean> chapterList = getChapterList(subjectId);
        ViewChapterBean viewChapterBean = null;                                      
        if(chapterList != null) {
            int easyCount,medCount,hrdCount;
            int hint,noHint;
            int used,noUsed;
            int pre,notPre;
            int the,num;   
            int total;
            for(ChapterBean chapterBean : chapterList) {
                viewChapterBean = new ViewChapterBean();
                viewChapterBean.setChapterBean(chapterBean);
                easyCount=0;medCount=0;hrdCount=0;
                hint=0;noHint=0;
                used=0;noUsed=0;
                pre=0;notPre=0;
                the=0;num=0;   
                total=0;
                for(QuestionBean questionsBean:questionsList) {
                    if(questionsBean.getChapterId() == chapterBean.getChapterId()) {
                        
                        total+=1;
                        if(questionsBean.getLevel()==0)
                            easyCount+=1;
                        else if(questionsBean.getLevel()==1)
                            medCount+=1;
                        else if(questionsBean.getLevel()==2)
                            hrdCount+=1;
                        
                        if(questionsBean.getHint().equals("") || questionsBean.getHint().equals(" ") || questionsBean.getHint().equals(null) || questionsBean.getHint().equals("\\mbox{}") || questionsBean.getHint().equals("\\mbox{ }"))
                            noHint+=1;
                        else
                            hint+=1;
                        
                        if(questionsBean.getAttempt()==1)
                            used+=1;
                        else
                            noUsed+=1;
                        
                        if(questionsBean.getYear().equals("") || questionsBean.getYear().equals(" ") || questionsBean.getYear().equals(null))
                            notPre+=1;
                        else
                            pre+=1;
                        
                        if(questionsBean.getType()==0)
                            the+=1;
                        else
                            num+=1;
                    }
                }
                viewChapterBean.setEasy(easyCount);
                viewChapterBean.setMedium(medCount);
                viewChapterBean.setHard(hrdCount);
    
                viewChapterBean.setHint(hint);
                viewChapterBean.setNoHint(noHint);
                
                viewChapterBean.setUsed(used);
                viewChapterBean.setNotUsed(noUsed);
                
                viewChapterBean.setAsked(pre);
                viewChapterBean.setNotAsked(notPre);
                
                viewChapterBean.setNumerical(num);
                viewChapterBean.setTheory(the);
                
                viewChapterBean.setTotalQue(total);
                
                returnList.add(viewChapterBean);
            }
        }
        return returnList;
    }
   
    public ArrayList<ChapterBean> getChapterList(ArrayList<SubjectBean> subjectList) {
        ArrayList<ChapterBean> returnList = new ArrayList<ChapterBean>();
        
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM CHAPTER_INFO WHERE SUBJECT_ID = ?";
            ps = conn.prepareStatement(query);
            for(SubjectBean subjectBean : subjectList) {
                ps.setInt(1, subjectBean.getSubjectId());
                rs=ps.executeQuery();
                ChapterBean chapterBean = null;
                while(rs.next()) {
                    chapterBean = new ChapterBean();
                    chapterBean.setChapterId(rs.getInt(1));
                    chapterBean.setSubjectId(rs.getInt(2));
                    chapterBean.setChapterName(rs.getString(3));
                    chapterBean.setWeightage(rs.getInt(4));
                    returnList.add(chapterBean);
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnList;
    } 
   
    public boolean updateChapterWeightage(ArrayList<ChapterBean> updatedChapterList) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE CHAPTER_INFO SET WEIGHTAGE = ? WHERE CHAPTER_ID = ?";
            ps = conn.prepareStatement(query);
            for(ChapterBean chapterBean : updatedChapterList) {
                ps.setInt(1, chapterBean.getWeightage());
                ps.setInt(2, chapterBean.getChapterId());
                ps.executeUpdate();
            }
            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    //Pattern Process
    public boolean insertChaptertList(ArrayList<ChapterBean> chapterList) {
        boolean returnValue = false;
        conn = new DbConnection().getConnection();
        try {
            String query = "INSERT INTO CHAPTER_INFO VALUES(?,?,?,?)";
            ps = conn.prepareStatement(query);
            for(ChapterBean chapterBean : chapterList) {
                ps.setInt(1, chapterBean.getChapterId());
                ps.setInt(2, chapterBean.getSubjectBean().getSubjectId());
                ps.setString(3, chapterBean.getChapterName());
                ps.setInt(4, chapterBean.getWeightage());
                ps.executeUpdate();
            }
            
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            sqlClose();
        }    
        return returnValue;
    }
  
    //Refine Process
    public ArrayList<ChapterBean> getChapterList() {
        ArrayList<ChapterBean> returnList = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM CHAPTER_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            ChapterBean bean = null;
            while(rs.next()) {
                bean = new ChapterBean();
                bean.setChapterId(rs.getInt(1));
                bean.setSubjectId(rs.getInt(2));
                bean.setChapterName(rs.getString(3));
                bean.setWeightage(rs.getInt(4));
                
                if(returnList == null)
                    returnList = new ArrayList<ChapterBean>();
                    
                returnList.add(bean);
            }
        } catch(Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnList;
    }
    
    public ArrayList<ChapterViewBean> getViewChapterList() {
        ArrayList<ChapterViewBean> returnList = null;
        ChapterViewBean chapterViewBean = null;
        ArrayList<MasterChapterCountBean> masterChapterCountList = new MasterChapterOperation().getMasterQuesCountList();
        ArrayList<SubMasterChapterBean> subMasterChapterList = new SubMasterChapterOperation().getSubMasterChapterList();
        if(subMasterChapterList != null) {
            for(SubMasterChapterBean subMasterChapterBean : subMasterChapterList) {
                chapterViewBean = new ChapterViewBean();
                chapterViewBean.setChapterId(subMasterChapterBean.getChapterId());
                chapterViewBean.setChapterName(subMasterChapterBean.getChapterName());
                chapterViewBean.setGroupBean(subMasterChapterBean.getGroupBean());
                chapterViewBean.setSubjectBean(subMasterChapterBean.getSubjectBean());
                chapterViewBean.setTotalQuesCount(getQuesCount(masterChapterCountList, subMasterChapterBean.getChapterIds().trim()));
                
                if(returnList == null)
                    returnList = new ArrayList<ChapterViewBean>();
                returnList.add(chapterViewBean);
            }
        }
        
        return returnList;
    }
    
    private int getQuesCount(ArrayList<MasterChapterCountBean> masterChapterCountList,String masterChapterIds) {
        int returnValue = 0;
        String [] chapterIds = masterChapterIds.split(",");
        for(String chapterId : chapterIds) {
            if(!chapterId.equalsIgnoreCase("")) {
                for(MasterChapterCountBean bean : masterChapterCountList) {
                    if(bean.getChapterId() == Integer.parseInt(chapterId)) {
                        returnValue += bean.getTotalQuetions();
                        break;
                    }
                }
            }
        }
        return returnValue;
    }
    
    public int getMaxChaptertId() {
       int newId = 0;
        try {
            conn = new com.db.DbConnection().getConnection();
            stmt = conn.createStatement();
            //String query = "SELECT COUNT(*) FROM CHAPTER_INFO WHERE SUBJECT_ID="+SubjectId;
            String query = "SELECT COUNT(*) FROM CHAPTER_INFO";
            rs=stmt.executeQuery(query);
            while (rs.next()) {
                newId = rs.getInt(1);
            }
            newId ++;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(stmt != null)
                    stmt.close();
                if(conn != null)
                    conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return newId;
    }
    public boolean insertChaptertList1(ChapterBean chapterBean) {
        boolean returnValue = false;
        conn = new DbConnection().getConnection();
        try {
            String query = "INSERT INTO CHAPTER_INFO VALUES(?,?,?,?)";
            ps = conn.prepareStatement(query);
            
                ps.setInt(1, chapterBean.getChapterId());
                ps.setInt(2, chapterBean.getSubjectBean().getSubjectId());
                ps.setString(3, chapterBean.getChapterName());
                ps.setInt(4, chapterBean.getWeightage());
                ps.executeUpdate();
                
            int MasterChapterId= new ChapterOperation().getMaxMasterChaptertId();
            query = "INSERT INTO MASTER_CHAPTER_INFO VALUES(?,?,?,?)";
            ps = conn.prepareStatement(query);
            
                ps.setInt(1, MasterChapterId);
                ps.setInt(2, chapterBean.getSubjectBean().getSubjectId());
                ps.setString(3, chapterBean.getChapterName());
                ps.setInt(4,1);
                ps.executeUpdate();     
            
            int SubChapterId= new ChapterOperation().getMaxSubMasterChaptertId();
            query = "INSERT INTO SUB_MASTER_CHAPTER_INFO VALUES(?,?,?,?,?,?)";
            ps = conn.prepareStatement(query);
            
                ps.setInt(1, SubChapterId);
                ps.setString(2, chapterBean.getChapterName());
                ps.setInt(3, chapterBean.getSubjectBean().getSubjectId());               
                ps.setInt(4, chapterBean.getWeightage());
                ps.setInt(5, MasterChapterId);
                ps.setInt(6, 1);
                ps.executeUpdate();        
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            sqlClose();
        }    
        return returnValue;
    }
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    public int getMaxMasterChaptertId() {
       int newId = 0;
        try {
            conn = new com.db.DbConnection().getConnection();
            stmt = conn.createStatement();
            //String query = "SELECT COUNT(*) FROM CHAPTER_INFO WHERE SUBJECT_ID="+SubjectId;
            String query = "SELECT COUNT(*) FROM MASTER_CHAPTER_INFO";
            rs=stmt.executeQuery(query);
            while (rs.next()) {
                newId = rs.getInt(1);
            }
            newId ++;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(stmt != null)
                    stmt.close();
                if(conn != null)
                    conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return newId;
    }
    
    public int getMaxSubMasterChaptertId() {
       int newId = 0;
        try {
            conn = new com.db.DbConnection().getConnection();
            stmt = conn.createStatement();
            //String query = "SELECT COUNT(*) FROM CHAPTER_INFO WHERE SUBJECT_ID="+SubjectId;
            String query = "SELECT COUNT(*) FROM SUB_MASTER_CHAPTER_INFO";
            rs=stmt.executeQuery(query);
            while (rs.next()) {
                newId = rs.getInt(1);
            }
            newId ++;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(stmt != null)
                    stmt.close();
                if(conn != null)
                    conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return newId;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.SMSBean;
import com.bean.SMSHistoryBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class SMSHistoryOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private Statement stmt = null;
    
    public  ArrayList<SMSHistoryBean > getSMSHistoryList() throws SQLException {
          ArrayList<SMSHistoryBean > smsHistoryBeanList = new ArrayList<SMSHistoryBean>();
          SMSHistoryBean smsHistoryBean=null;
        try {
        
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM SMSHISTORY ORDER By SID";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();            
            while(rs.next()) {                 
                 smsHistoryBean =new SMSHistoryBean();
                 smsHistoryBean.setSid(rs.getInt(1));
                 smsHistoryBean.setRollNo(rs.getInt(2));
                 smsHistoryBean.setStudentName(rs.getString(3));
                 smsHistoryBean.setMobileNo(rs.getString(4));
                 smsHistoryBean.setMessageText(rs.getString(5));
                 smsHistoryBean.setTestId(rs.getInt(6));
                 smsHistoryBean.setTestName(rs.getString(7));
                 smsHistoryBean.setDeliveryReport(rs.getBoolean(8));
                 smsHistoryBean.setSend_Sms_Date(rs.getString(9));
                 smsHistoryBeanList.add(smsHistoryBean);
            }
                      
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.close();
        }
       return smsHistoryBeanList;
    }
    public  ArrayList<SMSHistoryBean > getSMSHistoryByTestId(int testid) throws SQLException {
          ArrayList<SMSHistoryBean > smsHistoryBeanList = new ArrayList<SMSHistoryBean>();
          SMSHistoryBean smsHistoryBean=null;
        try {
        
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM SMSHISTORY where TEST_ID= " +testid +" ORDER By SID";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();            
            while(rs.next()) {                 
                 smsHistoryBean =new SMSHistoryBean();
                 smsHistoryBean.setSid(rs.getInt(1));
                 smsHistoryBean.setRollNo(rs.getInt(2));
                 smsHistoryBean.setStudentName(rs.getString(3));
                 smsHistoryBean.setMobileNo(rs.getString(4));
                 smsHistoryBean.setMessageText(rs.getString(5));
                 smsHistoryBean.setTestId(rs.getInt(6));
                 smsHistoryBean.setTestName(rs.getString(7));
                 smsHistoryBean.setDeliveryReport(rs.getBoolean(8));
                 smsHistoryBean.setSend_Sms_Date(rs.getString(9));
                 smsHistoryBeanList.add(smsHistoryBean);
            }
                      
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.close();
        }
       return smsHistoryBeanList;
    }
    public  ArrayList<SMSHistoryBean > getSMSHistoryBySTATUS(boolean DReport) throws SQLException {
          ArrayList<SMSHistoryBean > smsHistoryBeanList = new ArrayList<SMSHistoryBean>();
          SMSHistoryBean smsHistoryBean=null;
        try {
        
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM SMSHISTORY where DELIVERY_STATUS= " +DReport+" ORDER By SID";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();            
            while(rs.next()) {                 
                 smsHistoryBean =new SMSHistoryBean();
                 smsHistoryBean.setSid(rs.getInt(1));
                 smsHistoryBean.setRollNo(rs.getInt(2));
                 smsHistoryBean.setStudentName(rs.getString(3));
                 smsHistoryBean.setMobileNo(rs.getString(4));
                 smsHistoryBean.setMessageText(rs.getString(5));
                 smsHistoryBean.setTestId(rs.getInt(6));
                 smsHistoryBean.setTestName(rs.getString(7));
                 smsHistoryBean.setDeliveryReport(rs.getBoolean(8));
                 smsHistoryBean.setSend_Sms_Date(rs.getString(9));
                 smsHistoryBeanList.add(smsHistoryBean);
            }
                      
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.close();
        }
       return smsHistoryBeanList;
    }
    
    public  ArrayList<SMSHistoryBean > getSMSHistoryDeliveredList(boolean DReport ,int testid) throws SQLException {
          ArrayList<SMSHistoryBean > smsHistoryBeanList = new ArrayList<SMSHistoryBean>();
          SMSHistoryBean smsHistoryBean=null;
        try {
        
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM SMSHISTORY where DELIVERY_STATUS= " +DReport+" AND TEST_ID=" +testid+" ORDER By SID";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();            
            while(rs.next()) {                 
                 smsHistoryBean =new SMSHistoryBean();
                 smsHistoryBean.setSid(rs.getInt(1));
                 smsHistoryBean.setRollNo(rs.getInt(2));
                 smsHistoryBean.setStudentName(rs.getString(3));
                 smsHistoryBean.setMobileNo(rs.getString(4));
                 smsHistoryBean.setMessageText(rs.getString(5));
                 smsHistoryBean.setTestId(rs.getInt(6));
                 smsHistoryBean.setTestName(rs.getString(7));
                 smsHistoryBean.setDeliveryReport(rs.getBoolean(8));
                 smsHistoryBean.setSend_Sms_Date(rs.getString(9));
                 smsHistoryBeanList.add(smsHistoryBean);
            }
                      
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.close();
        }
       return smsHistoryBeanList;
    }
    public int getNewSId(){
        int newSId = 0;
        try {
            conn = new DbConnection().getConnection();
            stmt = conn.createStatement();
            String query = "SELECT COUNT(*) FROM SMSHISTORY";
            rs=stmt.executeQuery(query);
            while (rs.next()) {
                newSId = rs.getInt(1);
            }
            newSId ++;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(stmt != null)
                    stmt.close();
                if(conn != null)
                    conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return newSId;
    }
    
     public  boolean InsertSMSHistory(SMSHistoryBean smsHistoryBean) throws SQLException {
         boolean return1=false ;
        try {
            
            conn = new DbConnection().getConnection();
            String query = "INSERT INTO SMSHISTORY"
                    + "(SID,ROLL_NO,STUDENT_NAME,MOBILE_NO,MESSAGETEXT,TEST_ID,TEST_NAME,DELIVERY_STATUS,SEND_SMS_DATE)"
                    + "Values(?,?,?,?,?,?,?,?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, smsHistoryBean.getSid());
            ps.setInt(2, smsHistoryBean.getRollNo());
            ps.setString(3, smsHistoryBean.getStudentName());
            ps.setString(4, smsHistoryBean.getMobileNo());
            ps.setString(5, smsHistoryBean.getMessageText());
            ps.setInt(6, smsHistoryBean.getTestId());
            ps.setString(7, smsHistoryBean.getTestName());
            ps.setBoolean(8, smsHistoryBean.isDeliveryReport());
            ps.setString(9, smsHistoryBean.getSend_Sms_Date());
            ps.executeUpdate();
            return1=true;
            
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.close();
        }
       return return1;
    }
    
    public boolean updateSmsHistory (SMSHistoryBean smsHistoryBean) throws SQLException {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE SMSHISTORY SET ROLL_NO = ? ,STUDENT_NAME = ? , MOBILE_NO = ?,MESSAGETEXT=?,TEST_ID=?,TEST_NAME=?,DELIVERY_STATUS=?,SEND_SMS_DATE=? WHERE SID = ?";

            ps = conn.prepareStatement(query);            
            ps.setInt(1, smsHistoryBean.getSid());
            ps.setInt(2, smsHistoryBean.getRollNo());
            ps.setString(3, smsHistoryBean.getStudentName());
            ps.setString(4, smsHistoryBean.getMobileNo());
            ps.setString(5, smsHistoryBean.getMessageText());
            ps.setInt(6, smsHistoryBean.getTestId());
            ps.setString(7, smsHistoryBean.getTestName());
            ps.setBoolean(8, smsHistoryBean.isDeliveryReport());
            ps.setString(9, smsHistoryBean.getSend_Sms_Date());
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            conn.close();
        }
        return returnValue;
    }
    
    public  boolean deleteSMSHistoryList() throws SQLException {
         boolean returnValue = false;
        try {
        
            conn = new DbConnection().getConnection();
            String query = "DELETE FROM SMSHISTORY";
            ps = conn.prepareStatement(query);
            ps.executeUpdate();            
            returnValue = true;
                      
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.close();
        }
       return returnValue;
    }
}

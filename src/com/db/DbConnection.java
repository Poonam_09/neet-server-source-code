/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 *
 * @author admin
 */
public class DbConnection {
    
    public Connection getConnection(){
                // Load the JDBC Driver
                String DERBY_EMBEDDED_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
                String DERBY_EMBEDDED_URL="jdbc:derby:MERGEDB;";
		try{
                    Class.forName(DERBY_EMBEDDED_DRIVER).newInstance();
		} 
                catch (Exception e) {
                    System.out.println("[NsSample] Unable to load the JDBC driver. Following exception was thrown");
                    e.printStackTrace();
                    System.exit(1);  //critical error, so exit
		}
		Properties properties = new java.util.Properties();		
		properties.setProperty("user","root");
		properties.setProperty("password","root");

		// Get database connection via DriverManager api
		try{			
                    Connection conn = (Connection) DriverManager.getConnection(DERBY_EMBEDDED_URL, "root", "root");
//                    System.out.println("Embedded Connection request Successful ");
                    return conn;
		} 
                catch(Exception e){
			System.out.println("[NsSample] Connection request unsuccessful, exception thrown was: ");
			System.out.println("[NsSample] Please check if all the jar files are in the classpath and the dbUrl is set correctly.");
			e.printStackTrace();
			System.exit(1);  //critical error, so exit
		  }

		return null;
        }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.tables;

import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Aniket
 */
public class CreateTables {
    private Connection conn = null;
    private PreparedStatement ps = null;
    
    public void createTables() {
        conn = new DbConnection().getConnection();
        try {
            ps = conn.prepareStatement("CREATE TABLE CHAPTER_INFO (\n" +
                                    " CHAPTER_ID INTEGER NOT NULL,\n" +
                                    " SUBJECT_ID INTEGER NOT NULL,\n" +
                                    " CHAPTER_NAME VARCHAR(255) NOT NULL,\n" +
                                    " WEIGHTAGE INTEGER NOT NULL,\n" +
                                    " PRIMARY KEY (CHAPTER_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE IMAGERATIO (\n" +
                                    " IMAGENAME VARCHAR(255),\n" +
                                    " VIEWDIME DOUBLE NOT NULL\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE OPTION_IMAGE_DIMENSIONS (\n" +
                                    " QUEST_ID INTEGER NOT NULL,\n" +
                                    " OPTA VARCHAR(30) NOT NULL,\n" +
                                    " OPTB VARCHAR(30) NOT NULL,\n" +
                                    " OPTC VARCHAR(30) NOT NULL,\n" +
                                    " OPTD VARCHAR(30) NOT NULL,\n" +
                                    " PRIMARY KEY (QUEST_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE OPTION_IMAGE_DIMENSIONSFORPAPER (\n" +
                                    " QUEST_ID INTEGER NOT NULL,\n" +
                                    " OPTA VARCHAR(30) NOT NULL,\n" +
                                    " OPTB VARCHAR(30) NOT NULL,\n" +
                                    " OPTC VARCHAR(30) NOT NULL,\n" +
                                    " OPTD VARCHAR(30) NOT NULL,\n" +
                                    " PRIMARY KEY (QUEST_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        
        try {
            ps = conn.prepareStatement("CREATE TABLE PAGE_SETUP_INFO (\n" +
                                    " PAGE_FORMAT INTEGER NOT NULL,\n" +
                                    " IS_TWO_COLUMN BOOLEAN NOT NULL,\n" +
                                    " IS_PRINT_YEAR BOOLEAN NOT NULL,\n" +
                                    " PAPER_TYPE VARCHAR(20) NOT NULL,\n" +
                                    " IS_SET_LOGO BOOLEAN NOT NULL,\n" +
                                    " LOGO_PATH VARCHAR(250) NOT NULL,\n" +
                                    " IS_SET_IMAGE_ACTUAL_SIZE BOOLEAN NOT NULL,\n" +
                                    " LOGO_POSITION INTEGER NOT NULL,\n" +
                                    " WATERMARK_INFO INTEGER NOT NULL,\n" +
                                    " WATERMARK_TEXT_GRAYSCALE INTEGER NOT NULL,\n" +
                                    " WATERMARK_SCALE INTEGER NOT NULL,\n" +
                                    " WATERMARK_ANGLE INTEGER NOT NULL,\n" +
                                    " WATERMARK_LOGOPATH VARCHAR(250) NOT NULL,\n" +
                                    " HEADER_LEFT_CHECKBOX BOOLEAN NOT NULL,\n" +
                                    " HEADER_LEFT VARCHAR(100) NOT NULL,\n" +
                                    " HEADER_CENTER_CHECKBOX BOOLEAN NOT NULL,\n" +
                                    " HEADER_CENTER VARCHAR(100) NOT NULL,\n" +
                                    " HEADER_RIGHT_CHECKBOX BOOLEAN NOT NULL,\n" +
                                    " HEADER_RIGHT VARCHAR(100) NOT NULL,\n" +
                                    " FOOTER_LEFT_CHECKBOX BOOLEAN NOT NULL,\n" +
                                    " FOOTER_LEFT VARCHAR(100) NOT NULL,\n" +
                                    " FOOTER_CENTER_CHECKBOX BOOLEAN NOT NULL,\n" +
                                    " FOOTER_CENTER VARCHAR(100) NOT NULL,\n" +
                                    " FOOTER_RIGHT_CHECKBOX BOOLEAN NOT NULL,\n" +
                                    " FOOTER_RIGHT VARCHAR(100) NOT NULL,\n" +
                                    " IS_HEADER_FOOTER_ON_FIRST_PAGE BOOLEAN NOT NULL,\n" +
                                    " QUESTION_START_NO INTEGER NOT NULL,\n" +
                                    " IS_QUESTION_BOLD BOOLEAN NOT NULL,\n" +
                                    " IS_OPTION_BOLD BOOLEAN NOT NULL,\n" +
                                    " IS_SOLUTION_BOLD BOOLEAN NOT NULL,\n" +
                                    " IS_CENTERLINE BOOLEAN NOT NULL,\n" +
                                    " IS_PAGE_BORDER BOOLEAN NOT NULL,\n" +
                                    " BORDER_WIDTH INTEGER NOT NULL\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE PRINTED_TEST_INFO (\n" +
                                    " PRINTED_TEST_ID INTEGER NOT NULL,\n" +
                                    " TEST_NAME VARCHAR(200) NOT NULL,\n" +
                                    " QUES_IDS VARCHAR(10000) NOT NULL,\n" +
                                    " TOTAL_QUES INTEGER NOT NULL,\n" +
                                    " QUES_TYPE VARCHAR(200) NOT NULL,\n" +
                                    " SUBJECT_IDS VARCHAR(20) NOT NULL,\n" +
                                    " SAVE_DATE_TIME TIMESTAMP NOT NULL,\n" +
                                    " QUESTION_LIST_OBJECT BLOB(2147483647) NOT NULL,\n" +
                                    " PRIMARY KEY (PRINTED_TEST_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE QUESTION_INFO (\n" +
                                    " QUESTION_ID INTEGER NOT NULL,\n" +
                                    " QUESTION VARCHAR(3000) NOT NULL,\n" +
                                    " OPTIONA VARCHAR(1000) NOT NULL,\n" +
                                    " OPTIONB VARCHAR(1000) NOT NULL,\n" +
                                    " OPTIONC VARCHAR(1000) NOT NULL,\n" +
                                    " OPTIOND VARCHAR(1000) NOT NULL,\n" +
                                    " ANSWER VARCHAR(1000),\n" +
                                    " HINT VARCHAR(4000),\n" +
                                    " QUESTION_LEVEL INTEGER NOT NULL,\n" +
                                    " SUBJECT_ID INTEGER NOT NULL,\n" +
                                    " CHAPTER_ID INTEGER NOT NULL,\n" +
                                    " TOPIC_ID INTEGER NOT NULL,\n" +
                                    " ISQUESTIONASIMAGE BOOLEAN NOT NULL,\n" +
                                    " QUESTIONIMAGEPATH VARCHAR(255),\n" +
                                    " ISOPTIONASIMAGE BOOLEAN NOT NULL,\n" +
                                    " OPTIONIMAGEPATH VARCHAR(255),\n" +
                                    " ISHINTASIMAGE BOOLEAN NOT NULL,\n" +
                                    " HINTIMAGEPATH VARCHAR(255),\n" +
                                    " ATTEMPTEDVALUE INTEGER NOT NULL,\n" +
                                    " QUESTION_TYPE INTEGER NOT NULL,\n" +
                                    " QUESTION_YEAR VARCHAR(255),\n" +
                                    " PRIMARY KEY (QUESTION_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE QUESTION_PAPER_INFO (\n" +
                                    " QUESTION_ID INTEGER NOT NULL,\n" +
                                    " QUESTION VARCHAR(3000) NOT NULL,\n" +
                                    " OPTIONA VARCHAR(1000) NOT NULL,\n" +
                                    " OPTIONB VARCHAR(1000) NOT NULL,\n" +
                                    " OPTIONC VARCHAR(1000) NOT NULL,\n" +
                                    " OPTIOND VARCHAR(1000) NOT NULL,\n" +
                                    " ANSWER VARCHAR(1000),\n" +
                                    " HINT VARCHAR(4000),\n" +
                                    " QUESTION_LEVEL INTEGER NOT NULL,\n" +
                                    " SUBJECT_ID INTEGER NOT NULL,\n" +
                                    " CHAPTER_ID INTEGER NOT NULL,\n" +
                                    " TOPIC_ID INTEGER NOT NULL,\n" +
                                    " ISQUESTIONASIMAGE BOOLEAN NOT NULL,\n" +
                                    " QUESTIONIMAGEPATH VARCHAR(255),\n" +
                                    " ISOPTIONASIMAGE BOOLEAN NOT NULL,\n" +
                                    " OPTIONIMAGEPATH VARCHAR(255),\n" +
                                    " ISHINTASIMAGE BOOLEAN NOT NULL,\n" +
                                    " HINTIMAGEPATH VARCHAR(255),\n" +
                                    " ATTEMPTEDVALUE INTEGER NOT NULL,\n" +
                                    " QUESTION_TYPE INTEGER NOT NULL,\n" +
                                    " QUESTION_YEAR VARCHAR(255),\n" +
                                    " PRIMARY KEY (QUESTION_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE SUBJECT_INFO (\n" +
                                    " SUBJECT_ID INTEGER NOT NULL,\n" +
                                    " SUBJECT_NAME VARCHAR(255) NOT NULL,\n" +
                                    " MARKS_PER_QUESTION INTEGER NOT NULL,\n" +
                                    " MARKS_PER_WRONG_QUESTION INTEGER NOT NULL,\n" +
                                    " ACTIVE_STATUS BOOLEAN,\n" +
                                    " PRIMARY KEY (SUBJECT_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        //Pattern Process
        try {
            ps = conn.prepareStatement("CREATE TABLE MASTER_BOOK_INFO (\n" +
                                    " BOOK_ID INTEGER NOT NULL,\n" +
                                    " BOOK_NAME VARCHAR(500) NOT NULL,\n" +
                                    " BOOK_AUTHOR VARCHAR(500) NOT NULL,\n" +
                                    " PRIMARY KEY (BOOK_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE MASTER_CHAPTER_INFO (\n" +
                                    " CHAPTER_ID INTEGER NOT NULL,\n" +
                                    " SUBJECT_ID INTEGER NOT NULL,\n" +
                                    " CHAPTER_NAME VARCHAR(255) NOT NULL,\n" +
                                    " BOOK_ID INTEGER,\n" +
                                    " PRIMARY KEY (CHAPTER_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE MASTER_GROUP_INFO (\n" +
                                    " GROUP_ID INTEGER NOT NULL,\n" +
                                    " GROUP_NAME VARCHAR(100) NOT NULL,\n" +
                                    " SUBJECTIDS VARCHAR(25) NOT NULL,\n" +
                                    " PRIMARY KEY (GROUP_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE MASTER_IMAGERATIO (\n" +
                                    " IMAGENAME VARCHAR(255),\n" +
                                    " VIEWDIME DOUBLE NOT NULL\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE MASTER_OPTION_IMAGE_DIMENSIONS (\n" +
                                    " QUEST_ID INTEGER NOT NULL,\n" +
                                    " OPTA VARCHAR(30) NOT NULL,\n" +
                                    " OPTB VARCHAR(30) NOT NULL,\n" +
                                    " OPTC VARCHAR(30) NOT NULL,\n" +
                                    " OPTD VARCHAR(30) NOT NULL,\n" +
                                    " PRIMARY KEY (QUEST_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE MASTER_QUESTION_INFO (\n" +
                                    " QUESTION_ID INTEGER NOT NULL,\n" +
                                    " QUESTION VARCHAR(3000) NOT NULL,\n" +
                                    " OPTIONA VARCHAR(1000) NOT NULL,\n" +
                                    " OPTIONB VARCHAR(1000) NOT NULL,\n" +
                                    " OPTIONC VARCHAR(1000) NOT NULL,\n" +
                                    " OPTIOND VARCHAR(1000) NOT NULL,\n" +
                                    " ANSWER VARCHAR(1000),\n" +
                                    " HINT VARCHAR(4000),\n" +
                                    " QUESTION_LEVEL INTEGER NOT NULL,\n" +
                                    " SUBJECT_ID INTEGER NOT NULL,\n" +
                                    " CHAPTER_ID INTEGER NOT NULL,\n" +
                                    " TOPIC_ID INTEGER NOT NULL,\n" +
                                    " ISQUESTIONASIMAGE BOOLEAN NOT NULL,\n" +
                                    " QUESTIONIMAGEPATH VARCHAR(255),\n" +
                                    " ISOPTIONASIMAGE BOOLEAN NOT NULL,\n" +
                                    " OPTIONIMAGEPATH VARCHAR(255),\n" +
                                    " ISHINTASIMAGE BOOLEAN NOT NULL,\n" +
                                    " HINTIMAGEPATH VARCHAR(255),\n" +
                                    " ATTEMPTEDVALUE INTEGER NOT NULL,\n" +
                                    " QUESTION_TYPE INTEGER NOT NULL,\n" +
                                    " QUESTION_YEAR VARCHAR(255),\n" +
                                    " PRIMARY KEY (QUESTION_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE MASTER_SUBJECT_INFO (\n" +
                                    " SUBJECT_ID INTEGER NOT NULL,\n" +
                                    " SUBJECT_NAME VARCHAR(200) NOT NULL,\n" +
                                    " PRIMARY KEY (SUBJECT_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE SUB_MASTER_CHAPTER_INFO (\n" +
                                    " CHAPTER_ID INTEGER NOT NULL,\n" +
                                    " CHAPTER_NAME VARCHAR(200) NOT NULL,\n" +
                                    " SUBJECT_ID INTEGER NOT NULL,\n" +
                                    " WEIGTHAGE INTEGER NOT NULL,\n" +
                                    " MASTER_CHAPTER_IDS VARCHAR(150) NOT NULL,\n" +
                                    " GROUP_ID INTEGER NOT NULL,\n" +
                                    " PRIMARY KEY (CHAPTER_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE STATE_INFO (\n" +
                                    " STATE_ID INTEGER NOT NULL,\n" +
                                    " STATE_NAME VARCHAR(100) NOT NULL,\n" +
                                    " PRIMARY KEY (STATE_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        try {
            ps = conn.prepareStatement("CREATE TABLE CITY_INFO (\n" +
                                    " CITY_ID INTEGER NOT NULL,\n" +
                                    " CITY_NAME VARCHAR(150) NOT NULL,\n" +
                                    " STATE_ID INTEGER NOT NULL,\n" +
                                    " PRIMARY KEY (CITY_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        
        try {
            ps = conn.prepareStatement("CREATE TABLE REGISTRATION_INFO (\n" +
                                    " REG_ID INTEGER NOT NULL,\n" +
                                    " CUST_NAME VARCHAR(100) NOT NULL,\n" +
                                    " MBL_NUM VARCHAR(15) NOT NULL,\n" +
                                    " MAIL_ID VARCHAR(50) NOT NULL,\n" +
                                    " ADDRESS VARCHAR(2000) NOT NULL,\n" +
                                    " CITY_ID INTEGER NOT NULL,\n" +
                                    " PIN_CODE VARCHAR(15) NOT NULL,\n" +
                                    " SELLER_NAME VARCHAR(250) NOT NULL,\n" +
                                    " SELLER_MBL VARCHAR(15) NOT NULL,\n" +
                                    " INST_NAME VARCHAR(200) NOT NULL,\n" +
                                    " PRD_ID INTEGER NOT NULL,\n" +
                                    " CPU_ID VARCHAR(200) NOT NULL,\n" +
                                    " CD_KEY VARCHAR(50),\n" +
                                    " PIN_KEY VARCHAR(50),\n" +
                                    " ACT_DATE VARCHAR(100) NOT NULL,\n" +
                                    " EXP_DATE VARCHAR(100),\n" +
                                    " ACT_STATUS BOOLEAN NOT NULL,\n" +
                                    " PRIMARY KEY (REG_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        
        try {
            ps = conn.prepareStatement("CREATE TABLE PRODUCT_INFO (\n" +
                                    " PRD_ID INTEGER NOT NULL,\n" +
                                    " PRD_NAME VARCHAR(120) NOT NULL,\n" +
                                    " PRD_TYPE VARCHAR(100) NOT NULL,\n" +
                                    " PRIMARY KEY (PRD_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
        
        try {
            ps = conn.prepareStatement("CREATE TABLE PRODUCT_TYPE_INFO (\n" +
                                    " PRD_TYPE_ID INTEGER NOT NULL,\n" +
                                    " PRD_TYPE VARCHAR(100) NOT NULL,\n" +
                                    " PRIMARY KEY (PRD_TYPE_ID)\n" +
                                    " )");
            ps.execute();
        } catch (SQLException ex) {
        }
    }
    
    public static void main(String[] args) {
        new CreateTables().createTables();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pages;

import com.LatexProcessing.LatexConversion;
import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import com.bean.ChapterBean;
import com.bean.CountChapterBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.bean.ViewChapterBean;
import com.db.operations.ChapterOperation;
import com.db.operations.QuestionOperation;
import com.db.operations.SubjectOperation;
import javax.swing.JLabel;
import com.Model.TitleInfo;

/**
 *
 * @author admin
 */
public class MultipleChapterSelection extends javax.swing.JFrame {

    private int subjectFirstId = 0, subjectSecondId = 0, subjectThirdId = 0;
    private CardLayout cardLayout;
    private ArrayList<ViewChapterBean> firstPanelChaptersList;
    private ArrayList<ViewChapterBean> secondPanelChaptersList;
    private ArrayList<ViewChapterBean> thirdPanelChaptersList;
    private ArrayList<QuestionBean> questionsList;
    private javax.swing.JCheckBox[] chkboxArrayFirstSub, chkboxArraySecondSub, chkboxArrayThirdSub;
    private ArrayList<String> selectedChapters;
    private ArrayList<ChapterBean> selectedChapterList;
    private ArrayList<SubjectBean> selectedSubjectList;
    private SubjectBean subjectFirstBean;
    private SubjectBean subjectSecondBean;
    private SubjectBean subjectThirdBean;
    private String printingPaperType;

    public MultipleChapterSelection(int subjectFirstId, int subjectSecondId, int subjectThirdId) {
        initComponents();
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        setLocationRelativeTo(null);
        cardLayout = (CardLayout) PanelBody.getLayout();
        this.subjectFirstId = subjectFirstId;
        this.subjectSecondId = subjectSecondId;
        this.subjectThirdId = subjectThirdId;
        cardLayout.show(PanelBody, "cardFirst");
        BtnAll.setVisible(true);
        BtnSecondSubject.setContentAreaFilled(true);
        BtnThirdSubject.setContentAreaFilled(true);
        SubjectBean subjectBean1 = new SubjectOperation().getSubjectBean(this.subjectFirstId);
        BtnFirstSubject.setText(subjectBean1.getSubjectName());
        SubjectBean subjectBean2 = new SubjectOperation().getSubjectBean(this.subjectSecondId);
        BtnSecondSubject.setText(subjectBean2.getSubjectName());
        SubjectBean subjectBean3 = new SubjectOperation().getSubjectBean(this.subjectThirdId);
        BtnThirdSubject.setText(subjectBean3.getSubjectName());
        setPanelSubjectFirst();
        setPanelSubjectSecond();
        setPanelSubjectThird();
        setExtendedState(MAXIMIZED_BOTH);
    }
    
    //Select Manual Units
    public MultipleChapterSelection(SubjectBean subjectFirstBean, SubjectBean subjectSecondBean, SubjectBean subjectThirdBean,String printingPaperType) {
        initComponents();
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        setLocationRelativeTo(null);
        cardLayout = (CardLayout) PanelBody.getLayout();
        selectedSubjectList = new ArrayList<SubjectBean>();
        this.subjectFirstBean = subjectFirstBean;
        this.subjectSecondBean = subjectSecondBean;
        this.subjectThirdBean = subjectThirdBean;
        this.printingPaperType = printingPaperType;
        
        selectedSubjectList.add(this.subjectFirstBean);
        selectedSubjectList.add(this.subjectSecondBean);
        selectedSubjectList.add(this.subjectThirdBean);
        
        this.subjectFirstId = subjectFirstBean.getSubjectId();
        this.subjectSecondId = subjectSecondBean.getSubjectId();
        this.subjectThirdId = subjectThirdBean.getSubjectId();

        cardLayout.show(PanelBody, "cardFirst");
        BtnAll.setVisible(true);
        BtnSecondSubject.setContentAreaFilled(true);
        BtnThirdSubject.setContentAreaFilled(true);
        BtnFirstSubject.setText(this.subjectFirstBean.getSubjectName());
        BtnSecondSubject.setText(this.subjectSecondBean.getSubjectName());
        BtnThirdSubject.setText(this.subjectThirdBean.getSubjectName());
        setPanelSubjectFirst();
        setPanelSubjectSecond();
        setPanelSubjectThird();
        setExtendedState(MAXIMIZED_BOTH);
    }
    
    //Select UnitsWise
    public MultipleChapterSelection(SubjectBean subjectFirstBean,String printingPaperType) {
        initComponents();
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        setLocationRelativeTo(null);
        cardLayout = (CardLayout) PanelBody.getLayout();
        this.subjectFirstBean = subjectFirstBean;
        this.printingPaperType = printingPaperType;
        selectedSubjectList = new ArrayList<SubjectBean>();
        selectedSubjectList.add(this.subjectFirstBean);
        this.subjectFirstId = subjectFirstBean.getSubjectId();
//        SubPanelHeader2.remove(BtnAll);
        BtnAll.setVisible(true);
//        BtnAll.setVisible(false);
        SubPanelHeader1.remove(BtnFirstSubject);
        SubPanelHeader1.remove(BtnSecondSubject);
        SubPanelHeader1.remove(BtnThirdSubject);
        SubPanelHeader1.add(BtnFirstSubject);
        BtnSecondSubject.setOpaque(true);
        BtnSecondSubject.setVisible(false);
        BtnThirdSubject.setVisible(false);
        cardLayout.show(PanelBody, "cardFirst");
        BtnFirstSubject.setText(subjectFirstBean.getSubjectName());
        setPanelSubjectFirst();
        setExtendedState(MAXIMIZED_BOTH);
    }

    private void setPanelSubjectFirst() {
        firstPanelChaptersList = new ChapterOperation().getChaptersViewList(subjectFirstBean.getSubjectId());
        SubFirstPanel.removeAll();
        chkboxArrayFirstSub = new JCheckBox[firstPanelChaptersList.size()];
        for (int x = 0; x < firstPanelChaptersList.size(); x++) {
            chkboxArrayFirstSub[x] = new JCheckBox();
            chkboxArrayFirstSub[x].setVerticalTextPosition(SwingConstants.TOP);
            chkboxArrayFirstSub[x].setText("<html>"
                    + "<b><font color=#EF9433>" + firstPanelChaptersList.get(x).getChapterBean().getChapterName()
                    + "</font></b><hr>Total Questions: " + firstPanelChaptersList.get(x).getTotalQue()
                    + "<br />Easy: " + firstPanelChaptersList.get(x).getEasy()
                    + "<br />Medium: " + firstPanelChaptersList.get(x).getMedium()
                    + "<br />Hard: " + firstPanelChaptersList.get(x).getHard()
                    + "<br />Numerical: " + firstPanelChaptersList.get(x).getNumerical()
                    + "<br />Theoretical: " + firstPanelChaptersList.get(x).getTheory()
                    + "<br />Used: " + firstPanelChaptersList.get(x).getUsed()
                    + "<br />Previously Asked: " + firstPanelChaptersList.get(x).getAsked()
                    + "<br />Available Hints: " + firstPanelChaptersList.get(x).getHint()
                    + "</html>");
            chkboxArrayFirstSub[x].setMaximumSize(new java.awt.Dimension(200, 220));
            chkboxArrayFirstSub[x].setMinimumSize(new java.awt.Dimension(200, 220));
            chkboxArrayFirstSub[x].setPreferredSize(new java.awt.Dimension(200, 220));
            chkboxArrayFirstSub[x].setBorderPainted(false);
            chkboxArrayFirstSub[x].setBackground(new java.awt.Color(11, 45, 55));
            chkboxArrayFirstSub[x].setFont(new java.awt.Font("Microsoft YaHei UI", 0, 12)); // NOI18N
            chkboxArrayFirstSub[x].setForeground(new java.awt.Color(255, 255, 255));
            chkboxArrayFirstSub[x].setVerticalAlignment(1);
        }
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        cons.insets = new java.awt.Insets(10, 10,10, 10);
        GridBagLayout layout = new GridBagLayout();
        for (int x = 0; x < firstPanelChaptersList.size(); x++) {
            if (x % 6 == 0) {
                cons.gridy++;
                cons.gridx = 0;
            }
            
            SubFirstPanel.setLayout(layout);
            SubFirstPanel.add(chkboxArrayFirstSub[x], cons);
            cons.gridx++;
        }
        SubFirstPanel.validate();
        SubFirstPanel.repaint();
    }
    
    private void setPanelSubjectSecond() {
        secondPanelChaptersList = new ChapterOperation().getChaptersViewList(subjectSecondBean.getSubjectId());
        SubSecondPanel.removeAll();
        chkboxArraySecondSub = new JCheckBox[secondPanelChaptersList.size()];
        for (int x = 0; x < secondPanelChaptersList.size(); x++) {
            chkboxArraySecondSub[x] = new JCheckBox();
            chkboxArraySecondSub[x].setVerticalTextPosition(SwingConstants.TOP);
            chkboxArraySecondSub[x].setText("<html>"
                    + "<b><font color=#EF9433>" + secondPanelChaptersList.get(x).getChapterBean().getChapterName()
                    + "</font></b><hr>Total Questions: " + secondPanelChaptersList.get(x).getTotalQue()
                    + "<br />Easy: " + secondPanelChaptersList.get(x).getEasy()
                    + "<br />Medium: " + secondPanelChaptersList.get(x).getMedium()
                    + "<br />Hard: " + secondPanelChaptersList.get(x).getHard()
                    + "<br />Numerical: " + secondPanelChaptersList.get(x).getNumerical()
                    + "<br />Theoretical: " + secondPanelChaptersList.get(x).getTheory()
                    + "<br />Used: " + secondPanelChaptersList.get(x).getUsed()
                    + "<br />Previously Asked: " + secondPanelChaptersList.get(x).getAsked()
                    + "<br />Available Hints: " + secondPanelChaptersList.get(x).getHint()
                    + "</html>");
            chkboxArraySecondSub[x].setMaximumSize(new java.awt.Dimension(200, 220));
            chkboxArraySecondSub[x].setMinimumSize(new java.awt.Dimension(200, 220));
            chkboxArraySecondSub[x].setPreferredSize(new java.awt.Dimension(200, 220));
            chkboxArraySecondSub[x].setBorderPainted(false);
            chkboxArraySecondSub[x].setBackground(new java.awt.Color(11, 45, 55));
            chkboxArraySecondSub[x].setFont(new java.awt.Font("Microsoft YaHei UI", 0, 12)); // NOI18N
            chkboxArraySecondSub[x].setForeground(new java.awt.Color(255, 255, 255));
            chkboxArraySecondSub[x].setVerticalAlignment(1);
        }
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        cons.insets = new java.awt.Insets(10, 10,10, 10);
        GridBagLayout layout = new GridBagLayout();
        for (int x = 0; x < secondPanelChaptersList.size(); x++) {
            if (x % 6 == 0) {
                cons.gridy++;
                cons.gridx = 0;
            }
            SubSecondPanel.setLayout(layout);
            SubSecondPanel.add(chkboxArraySecondSub[x], cons);
            cons.gridx++;
        }
        SubSecondPanel.validate();
        SubSecondPanel.repaint();
    }

    private void setPanelSubjectThird() {
        thirdPanelChaptersList = new ChapterOperation().getChaptersViewList(subjectThirdBean.getSubjectId());
        SubThirdPanel.removeAll();
        chkboxArrayThirdSub = new JCheckBox[thirdPanelChaptersList.size()];
        for (int x = 0; x < thirdPanelChaptersList.size(); x++) {
            chkboxArrayThirdSub[x] = new JCheckBox();
            chkboxArrayThirdSub[x].setVerticalTextPosition(SwingConstants.TOP);
            chkboxArrayThirdSub[x].setText("<html>"
                    + "<b><font color=#EF9433>" + thirdPanelChaptersList.get(x).getChapterBean().getChapterName()
                    + "</font></b><hr>Total Questions: " + thirdPanelChaptersList.get(x).getTotalQue()
                    + "<br />Easy: " + thirdPanelChaptersList.get(x).getEasy()
                    + "<br />Medium: " + thirdPanelChaptersList.get(x).getMedium()
                    + "<br />Hard: " + thirdPanelChaptersList.get(x).getHard()
                    + "<br />Numerical: " + thirdPanelChaptersList.get(x).getNumerical()
                    + "<br />Theoretical: " + thirdPanelChaptersList.get(x).getTheory()
                    + "<br />Used: " + thirdPanelChaptersList.get(x).getUsed()
                    + "<br />Previously Asked: " + thirdPanelChaptersList.get(x).getAsked()
                    + "<br />Available Hints: " + thirdPanelChaptersList.get(x).getHint()
                    + "</html>");
            chkboxArrayThirdSub[x].setMaximumSize(new java.awt.Dimension(200, 220));
            chkboxArrayThirdSub[x].setMinimumSize(new java.awt.Dimension(200, 220));
            chkboxArrayThirdSub[x].setPreferredSize(new java.awt.Dimension(200, 220));
            chkboxArrayThirdSub[x].setBorderPainted(false);
            chkboxArrayThirdSub[x].setBackground(new java.awt.Color(11, 45, 55));
            chkboxArrayThirdSub[x].setFont(new java.awt.Font("Microsoft YaHei UI", 0, 12)); // NOI18N
            chkboxArrayThirdSub[x].setForeground(new java.awt.Color(255, 255, 255));
            chkboxArrayThirdSub[x].setVerticalAlignment(1);
        }
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        cons.insets = new java.awt.Insets(10, 10,10, 10);
        GridBagLayout layout = new GridBagLayout();
        for (int x = 0; x < thirdPanelChaptersList.size(); x++) {
            if (x % 6 == 0) {
                cons.gridy++;
                cons.gridx = 0;
            }
            SubThirdPanel.setLayout(layout);
            SubThirdPanel.add(chkboxArrayThirdSub[x], cons);
            cons.gridx++;
        }
        SubThirdPanel.validate();
        SubThirdPanel.repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BodyPanelScrollPane = new javax.swing.JScrollPane();
        PanelBody = new javax.swing.JPanel();
        PanelFirstSubjectChapters = new javax.swing.JPanel();
        SubFirstPanel = new javax.swing.JPanel();
        PanelSecondSubjectChapters = new javax.swing.JPanel();
        SubSecondPanel = new javax.swing.JPanel();
        PanelThirdSubjectChapters = new javax.swing.JPanel();
        SubThirdPanel = new javax.swing.JPanel();
        PanelHeader = new javax.swing.JPanel();
        SubPanelHeader1 = new javax.swing.JPanel();
        BtnFirstSubject = new javax.swing.JButton();
        BtnSecondSubject = new javax.swing.JButton();
        BtnThirdSubject = new javax.swing.JButton();
        SubPanelHeader2 = new javax.swing.JPanel();
        BtnBack = new javax.swing.JButton();
        BtnStart = new javax.swing.JButton();
        BtnAll = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        BodyPanelScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        BodyPanelScrollPane.setName("BodyPanelScrollPane"); // NOI18N

        PanelBody.setName("PanelBody"); // NOI18N
        PanelBody.setLayout(new java.awt.CardLayout());

        PanelFirstSubjectChapters.setBackground(new java.awt.Color(0, 102, 102));
        PanelFirstSubjectChapters.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        PanelFirstSubjectChapters.setName("PanelFirstSubjectChapters"); // NOI18N

        SubFirstPanel.setBackground(new java.awt.Color(0, 102, 102));
        SubFirstPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        SubFirstPanel.setName("SubFirstPanel"); // NOI18N

        javax.swing.GroupLayout SubFirstPanelLayout = new javax.swing.GroupLayout(SubFirstPanel);
        SubFirstPanel.setLayout(SubFirstPanelLayout);
        SubFirstPanelLayout.setHorizontalGroup(
            SubFirstPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        SubFirstPanelLayout.setVerticalGroup(
            SubFirstPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout PanelFirstSubjectChaptersLayout = new javax.swing.GroupLayout(PanelFirstSubjectChapters);
        PanelFirstSubjectChapters.setLayout(PanelFirstSubjectChaptersLayout);
        PanelFirstSubjectChaptersLayout.setHorizontalGroup(
            PanelFirstSubjectChaptersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1138, Short.MAX_VALUE)
            .addGroup(PanelFirstSubjectChaptersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(PanelFirstSubjectChaptersLayout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(SubFirstPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(1138, Short.MAX_VALUE)))
        );
        PanelFirstSubjectChaptersLayout.setVerticalGroup(
            PanelFirstSubjectChaptersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 220, Short.MAX_VALUE)
            .addGroup(PanelFirstSubjectChaptersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(PanelFirstSubjectChaptersLayout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(SubFirstPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(214, 214, 214)))
        );

        PanelBody.add(PanelFirstSubjectChapters, "cardFirst");

        PanelSecondSubjectChapters.setBackground(new java.awt.Color(0, 102, 102));
        PanelSecondSubjectChapters.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        PanelSecondSubjectChapters.setName("PanelSecondSubjectChapters"); // NOI18N

        SubSecondPanel.setBackground(new java.awt.Color(0, 102, 102));
        SubSecondPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        SubSecondPanel.setName("SubSecondPanel"); // NOI18N

        javax.swing.GroupLayout SubSecondPanelLayout = new javax.swing.GroupLayout(SubSecondPanel);
        SubSecondPanel.setLayout(SubSecondPanelLayout);
        SubSecondPanelLayout.setHorizontalGroup(
            SubSecondPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        SubSecondPanelLayout.setVerticalGroup(
            SubSecondPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout PanelSecondSubjectChaptersLayout = new javax.swing.GroupLayout(PanelSecondSubjectChapters);
        PanelSecondSubjectChapters.setLayout(PanelSecondSubjectChaptersLayout);
        PanelSecondSubjectChaptersLayout.setHorizontalGroup(
            PanelSecondSubjectChaptersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(PanelSecondSubjectChaptersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(PanelSecondSubjectChaptersLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(SubSecondPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );
        PanelSecondSubjectChaptersLayout.setVerticalGroup(
            PanelSecondSubjectChaptersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(PanelSecondSubjectChaptersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(PanelSecondSubjectChaptersLayout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(SubSecondPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        PanelBody.add(PanelSecondSubjectChapters, "cardSecond");

        PanelThirdSubjectChapters.setBackground(new java.awt.Color(0, 102, 102));
        PanelThirdSubjectChapters.setName("PanelThirdSubjectChapters"); // NOI18N

        SubThirdPanel.setBackground(new java.awt.Color(0, 102, 102));
        SubThirdPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        SubThirdPanel.setName("SubThirdPanel"); // NOI18N

        javax.swing.GroupLayout SubThirdPanelLayout = new javax.swing.GroupLayout(SubThirdPanel);
        SubThirdPanel.setLayout(SubThirdPanelLayout);
        SubThirdPanelLayout.setHorizontalGroup(
            SubThirdPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        SubThirdPanelLayout.setVerticalGroup(
            SubThirdPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout PanelThirdSubjectChaptersLayout = new javax.swing.GroupLayout(PanelThirdSubjectChapters);
        PanelThirdSubjectChapters.setLayout(PanelThirdSubjectChaptersLayout);
        PanelThirdSubjectChaptersLayout.setHorizontalGroup(
            PanelThirdSubjectChaptersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1138, Short.MAX_VALUE)
            .addGroup(PanelThirdSubjectChaptersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(PanelThirdSubjectChaptersLayout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(SubThirdPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(1138, Short.MAX_VALUE)))
        );
        PanelThirdSubjectChaptersLayout.setVerticalGroup(
            PanelThirdSubjectChaptersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 220, Short.MAX_VALUE)
            .addGroup(PanelThirdSubjectChaptersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(PanelThirdSubjectChaptersLayout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(SubThirdPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(220, Short.MAX_VALUE)))
        );

        PanelBody.add(PanelThirdSubjectChapters, "cardThird");

        BodyPanelScrollPane.setViewportView(PanelBody);

        PanelHeader.setName("PanelHeader"); // NOI18N

        SubPanelHeader1.setBackground(new java.awt.Color(0, 102, 102));
        SubPanelHeader1.setName("SubPanelHeader1"); // NOI18N
        SubPanelHeader1.setLayout(new java.awt.GridLayout(1, 0));

        BtnFirstSubject.setBackground(new java.awt.Color(0, 102, 102));
        BtnFirstSubject.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        BtnFirstSubject.setForeground(new java.awt.Color(255, 255, 255));
        BtnFirstSubject.setText("Physics");
        BtnFirstSubject.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255), new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255)));
        BtnFirstSubject.setContentAreaFilled(false);
        BtnFirstSubject.setName("BtnFirstSubject"); // NOI18N
        BtnFirstSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnFirstSubjectActionPerformed(evt);
            }
        });
        SubPanelHeader1.add(BtnFirstSubject);

        BtnSecondSubject.setBackground(new java.awt.Color(0, 102, 102));
        BtnSecondSubject.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        BtnSecondSubject.setForeground(new java.awt.Color(255, 255, 255));
        BtnSecondSubject.setText("Chemistry");
        BtnSecondSubject.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255), new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255)));
        BtnSecondSubject.setContentAreaFilled(false);
        BtnSecondSubject.setName("BtnSecondSubject"); // NOI18N
        BtnSecondSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSecondSubjectActionPerformed(evt);
            }
        });
        SubPanelHeader1.add(BtnSecondSubject);

        BtnThirdSubject.setBackground(new java.awt.Color(0, 102, 102));
        BtnThirdSubject.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        BtnThirdSubject.setForeground(new java.awt.Color(255, 255, 255));
        BtnThirdSubject.setText("Mathematics");
        BtnThirdSubject.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255), new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255)));
        BtnThirdSubject.setContentAreaFilled(false);
        BtnThirdSubject.setName("BtnThirdSubject"); // NOI18N
        BtnThirdSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnThirdSubjectActionPerformed(evt);
            }
        });
        SubPanelHeader1.add(BtnThirdSubject);

        SubPanelHeader2.setBackground(new java.awt.Color(0, 102, 102));
        SubPanelHeader2.setName("SubPanelHeader2"); // NOI18N
        SubPanelHeader2.setLayout(new java.awt.GridLayout(1, 0));

        BtnBack.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        BtnBack.setForeground(new java.awt.Color(255, 255, 255));
        BtnBack.setText("Back");
        BtnBack.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255), new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255)));
        BtnBack.setContentAreaFilled(false);
        BtnBack.setName("BtnBack"); // NOI18N
        BtnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackActionPerformed(evt);
            }
        });
        SubPanelHeader2.add(BtnBack);

        BtnStart.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        BtnStart.setForeground(new java.awt.Color(255, 255, 255));
        BtnStart.setText("Start");
        BtnStart.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255), new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255)));
        BtnStart.setContentAreaFilled(false);
        BtnStart.setName("BtnStart"); // NOI18N
        BtnStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnStartActionPerformed(evt);
            }
        });
        SubPanelHeader2.add(BtnStart);

        BtnAll.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        BtnAll.setForeground(new java.awt.Color(255, 255, 255));
        BtnAll.setText("All");
        BtnAll.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255), new java.awt.Color(204, 255, 204), java.awt.Color.white));
        BtnAll.setContentAreaFilled(false);
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        SubPanelHeader2.add(BtnAll);

        javax.swing.GroupLayout PanelHeaderLayout = new javax.swing.GroupLayout(PanelHeader);
        PanelHeader.setLayout(PanelHeaderLayout);
        PanelHeaderLayout.setHorizontalGroup(
            PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 413, Short.MAX_VALUE)
            .addGroup(PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(SubPanelHeader1, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE))
            .addGroup(PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(SubPanelHeader2, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE))
        );
        PanelHeaderLayout.setVerticalGroup(
            PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 104, Short.MAX_VALUE)
            .addGroup(PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(PanelHeaderLayout.createSequentialGroup()
                    .addComponent(SubPanelHeader1, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(43, Short.MAX_VALUE)))
            .addGroup(PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelHeaderLayout.createSequentialGroup()
                    .addContainerGap(61, Short.MAX_VALUE)
                    .addComponent(SubPanelHeader2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PanelHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(BodyPanelScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(PanelHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(224, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGap(107, 107, 107)
                    .addComponent(BodyPanelScrollPane)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void BtnFirstSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnFirstSubjectActionPerformed
    cardLayout.show(PanelBody, "cardFirst");
    BtnSecondSubject.setContentAreaFilled(true);
    BtnFirstSubject.setContentAreaFilled(false);
    BtnThirdSubject.setContentAreaFilled(true);
}//GEN-LAST:event_BtnFirstSubjectActionPerformed

private void BtnSecondSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSecondSubjectActionPerformed
    cardLayout.show(PanelBody, "cardSecond");
    BtnFirstSubject.setContentAreaFilled(true);
    BtnSecondSubject.setContentAreaFilled(false);
    BtnThirdSubject.setContentAreaFilled(true);
}//GEN-LAST:event_BtnSecondSubjectActionPerformed

private void BtnThirdSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnThirdSubjectActionPerformed
    cardLayout.show(PanelBody, "cardThird");
    BtnSecondSubject.setContentAreaFilled(true);
    BtnThirdSubject.setContentAreaFilled(false);
    BtnFirstSubject.setContentAreaFilled(true);
}//GEN-LAST:event_BtnThirdSubjectActionPerformed

private void BtnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackActionPerformed
//    if (subjectSecondId == 0) 
//        new HomePage(2).setVisible(true);
//    else 
//        new HomePage(10).setVisible(true);
    new HomePage(printingPaperType).setVisible(true);
    this.dispose();
}//GEN-LAST:event_BtnBackActionPerformed

private void BtnStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnStartActionPerformed
    int cnt1 = 0, cnt2 = 0, cnt3 = 0;
    if (subjectSecondId == 0) {
        for (int i = 0; i < chkboxArrayFirstSub.length; i++) {
            if (chkboxArrayFirstSub[i].isSelected()) {
                cnt1 = 1;
                cnt2 = 1;
                cnt3 = 1;
                break;
            }
        }
    } else {
        for (int i = 0; i < chkboxArrayFirstSub.length; i++) {
            if (chkboxArrayFirstSub[i].isSelected()) {
                cnt1 = 1;
                break;
            }
        }
        for (int i = 0; i < chkboxArraySecondSub.length; i++) {
            if (chkboxArraySecondSub[i].isSelected()) {
                cnt2 = 1;
                break;
            }
        }
        for (int i = 0; i < chkboxArrayThirdSub.length; i++) {
            if (chkboxArrayThirdSub[i].isSelected()) {
                cnt3 = 1;
                break;
            }
        }
    }
    
    selectedChapterList = new ArrayList<ChapterBean>();
    if (cnt1 == 1 && cnt2 == 1 && cnt3 == 1) {
        try {
            if (subjectFirstId != 0) {
                selectedChapters = new ArrayList<String>();
                
                for (int i = 0; i < chkboxArrayFirstSub.length; i++) {
                    if (chkboxArrayFirstSub[i].isSelected()) {
                        String getChapter = chkboxArrayFirstSub[i].getActionCommand();
                        String strSplt[] = getChapter.split("EF9433>");
                        String tempSplt = strSplt[1];
                        strSplt = tempSplt.split("<");
                        tempSplt = strSplt[0];
                        
                        selectedChapters.add(tempSplt);
                    }
                }
                
                for (String chapterName : selectedChapters) {
                    for (int i = 0; i < firstPanelChaptersList.size(); i++) {
                        if (firstPanelChaptersList.get(i).getChapterBean().getChapterName().equals(chapterName)) {
                            selectedChapterList.add(firstPanelChaptersList.get(i).getChapterBean());
                        }
                    }
                }
            }
            
            if (subjectSecondId != 0) {
                selectedChapters = new ArrayList<String>();
                for (int i = 0; i < chkboxArraySecondSub.length; i++) {
                    if (chkboxArraySecondSub[i].isSelected()) {
                        String getChapter = chkboxArraySecondSub[i].getActionCommand();
                        String strSplt[] = getChapter.split("EF9433>");
                        String tempSplt = strSplt[1];
                        strSplt = tempSplt.split("<");
                        tempSplt = strSplt[0];
                        selectedChapters.add(tempSplt);
                    }
                }
                for (String chapterName : selectedChapters) {
                    for (int i = 0; i < secondPanelChaptersList.size(); i++) {
                        if (secondPanelChaptersList.get(i).getChapterBean().getChapterName().equals(chapterName)) {
                            selectedChapterList.add(secondPanelChaptersList.get(i).getChapterBean());
                        }
                    }
                }
            }
            
            if (subjectThirdId != 0) {
                selectedChapters = new ArrayList<String>();
                for (int i = 0; i < chkboxArrayThirdSub.length; i++) {
                    if (chkboxArrayThirdSub[i].isSelected()) {
                        String getChapter = chkboxArrayThirdSub[i].getActionCommand();
                        String strSplt[] = getChapter.split("EF9433>");
                        String tempSplt = strSplt[1];
                        strSplt = tempSplt.split("<");
                        tempSplt = strSplt[0];
                        selectedChapters.add(tempSplt);
                    }
                }
                for (String chapterName : selectedChapters) {
                    for (int i = 0; i < thirdPanelChaptersList.size(); i++) {
                        if (thirdPanelChaptersList.get(i).getChapterBean().getChapterName().equals(chapterName)) {
                            selectedChapterList.add(thirdPanelChaptersList.get(i).getChapterBean());
                        }
                    }
                }
            }
    
            questionsList = new QuestionOperation().getQuestuionsUnitWise(selectedChapterList);
            ArrayList<QuestionBean> sortQuestionsList = new ArrayList<QuestionBean>();
            String strOption = "All";
            int resultOption = 0;
            Object[] possibilities = {"All", "With Hints", "Without Hints"};
            strOption = (String) JOptionPane.showInputDialog(null, "Select Question Type:\n", "", JOptionPane.PLAIN_MESSAGE, null, possibilities, "ham");
//            new LatexConversion().setLableText(new JLabel(), "a");
            if(strOption != null) {
                if (strOption.endsWith("All")) {
                    resultOption=0;
                } else if (strOption.endsWith("With Hints")) {
                    resultOption=1;
                } else if (strOption.endsWith("Without Hints")) {
                    resultOption=2;
                }

                if(resultOption==0) {
                    sortQuestionsList = questionsList;
                } else {
                    for(QuestionBean questionsBean : questionsList) {
                        if(resultOption==1) {
                            if(!questionsBean.getHint().equals("") && !questionsBean.getHint().equals(" ") && !questionsBean.getHint().equals(null) && !questionsBean.getHint().equals("\\mbox{}") && !questionsBean.getHint().equals("\\mbox{ }"))
                                sortQuestionsList.add(questionsBean);   
                        } else if(resultOption==2) {
                            if(questionsBean.getHint().equals("") || questionsBean.getHint().equals(" ") || questionsBean.getHint().equals(null) || questionsBean.getHint().equals("\\mbox{}") || questionsBean.getHint().equals("\\mbox{ }"))
                                sortQuestionsList.add(questionsBean);
                        }       
                    }
                }

                ArrayList <CountChapterBean> countedChapterList = new ArrayList<CountChapterBean>();
                CountChapterBean ccBean = null;
                for(ChapterBean bean : selectedChapterList) {
                    int totalQuestions =0;
                    for(QuestionBean questionsBean : sortQuestionsList) {
                        if(bean.getChapterId()==questionsBean.getChapterId()) {
                            totalQuestions++;
                        }    
                    }
                    ccBean = new CountChapterBean();
                    ccBean.setChapterBean(bean);
                    ccBean.setTotalQuestions(totalQuestions);
                    countedChapterList.add(ccBean);
                }

                new MultipleChapterQuestionsSelection(sortQuestionsList, selectedChapterList, selectedSubjectList,countedChapterList,this,printingPaperType).setVisible(true);
                this.setVisible(false);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    } else {
        if (!selectedChapterList.isEmpty()) {
            selectedChapterList.clear();
        }
        if (subjectSecondId != 0 || subjectThirdId != 0) {
            JOptionPane.showMessageDialog(null, "Please Select At Least One Chapter of Each Subject.");
        } else {
            JOptionPane.showMessageDialog(null, "Please Select At Least One Chapter.");
        }
    }
}//GEN-LAST:event_BtnStartActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        new HomePage().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_formWindowClosing

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        Object[] options = {"YES", "NO"};
        int oo = JOptionPane.showOptionDialog(null, "Are You Sure to Select all chapters?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if(oo == 0){
            try{
                selectedChapterList = new ArrayList<ChapterBean>();
                for(ViewChapterBean chapterBean : firstPanelChaptersList){
                    selectedChapterList.add(chapterBean.getChapterBean());
                }
                
                if(subjectSecondId!=0 && subjectThirdId!=0){
                    for(ViewChapterBean chapterBean : secondPanelChaptersList){
                        selectedChapterList.add(chapterBean.getChapterBean());
                    }
                    for(ViewChapterBean chapterBean : thirdPanelChaptersList){
                        selectedChapterList.add(chapterBean.getChapterBean());
                    }
                }
                
                questionsList = new QuestionOperation().getQuestuionsUnitWise(selectedChapterList);

                ArrayList<QuestionBean> sortQuestionsList=new ArrayList<QuestionBean>();
                String strOption = "All";
                int resultOption=0;
                Object[] possibilities = {"All", "With Hints", "Without Hints"};
                strOption = (String) JOptionPane.showInputDialog(null, "Select Question Type:\n", "", JOptionPane.PLAIN_MESSAGE, null, possibilities, "ham");
//                new LatexConversion().setLableText(new JLabel(), "a");
                if(strOption != null) {
                    if (strOption.endsWith("All")) {
                        resultOption=0;
                    } else if (strOption.endsWith("With Hints")) {
                        resultOption=1;
                    } else if (strOption.endsWith("Without Hints")) {
                        resultOption=2;
                    }
                    if(resultOption==0){
                        sortQuestionsList = questionsList;
                    } else {
                        for(QuestionBean questionsBean : questionsList) {
                            if(resultOption==1) {
                                if(!questionsBean.getHint().equals("") && !questionsBean.getHint().equals(" ") && !questionsBean.getHint().equals(null) && !questionsBean.getHint().equals("\\mbox{}") && !questionsBean.getHint().equals("\\mbox{ }"))
                                    sortQuestionsList.add(questionsBean);   
                            } else if(resultOption==2) {
                                if(questionsBean.getHint().equals("") || questionsBean.getHint().equals(" ") || questionsBean.getHint().equals(null) || questionsBean.getHint().equals("\\mbox{}") || questionsBean.getHint().equals("\\mbox{ }"))
                                    sortQuestionsList.add(questionsBean);
                            }    
                        }
                    }

                    ArrayList <CountChapterBean> countedChapterList = new ArrayList<CountChapterBean>();
                    CountChapterBean ccBean;
                    for(ChapterBean bean : selectedChapterList) {
                        int totalQuestions =0;
                        for(QuestionBean questionsBean : sortQuestionsList) {
                            if(bean.getChapterId() == questionsBean.getChapterId()) {
                                totalQuestions ++;   
                            }    
                        }
                        
                        ccBean = new CountChapterBean();
                        ccBean.setChapterBean(bean);
                        ccBean.setTotalQuestions(totalQuestions);
                        countedChapterList.add(ccBean);
                    }
                    new MultipleChapterQuestionsSelection(sortQuestionsList, selectedChapterList, selectedSubjectList,countedChapterList,this,printingPaperType).setVisible(true);
                    this.setVisible(false);
                }
            } catch(Exception ex){ 
                ex.printStackTrace();
            }
        }
    }//GEN-LAST:event_BtnAllActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(PracticeChapterSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(PracticeChapterSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(PracticeChapterSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(PracticeChapterSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>
            
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane BodyPanelScrollPane;
    private javax.swing.JButton BtnAll;
    private javax.swing.JButton BtnBack;
    private javax.swing.JButton BtnFirstSubject;
    private javax.swing.JButton BtnSecondSubject;
    private javax.swing.JButton BtnStart;
    private javax.swing.JButton BtnThirdSubject;
    private javax.swing.JPanel PanelBody;
    private javax.swing.JPanel PanelFirstSubjectChapters;
    private javax.swing.JPanel PanelHeader;
    private javax.swing.JPanel PanelSecondSubjectChapters;
    private javax.swing.JPanel PanelThirdSubjectChapters;
    private javax.swing.JPanel SubFirstPanel;
    private javax.swing.JPanel SubPanelHeader1;
    private javax.swing.JPanel SubPanelHeader2;
    private javax.swing.JPanel SubSecondPanel;
    private javax.swing.JPanel SubThirdPanel;
    // End of variables declaration//GEN-END:variables
}


package com.pages;

import com.Model.ProcessManager;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;
import com.Pdf.Generation.PdfPageSetup;
import com.Word.Generation.WordPageSetup;
import com.bean.CountYearBean;
import com.bean.ImageRatioBean;
import java.util.Collections;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.bean.PrintedTestBean;
import com.db.operations.ImageRatioOperation;
import com.db.operations.QuestionPaperOperation;
import com.db.operations.SubjectOperation;
import java.awt.event.KeyEvent;
import com.ui.support.pages.QuestionPanel;
import com.Model.TitleInfo;

public class MultipleYearQuestionsSelection extends javax.swing.JFrame {

    private int currentIndex, totalSelectedQueCount = 0;
    private ArrayList<CountYearBean> selectedYearList;
    private ArrayList<QuestionBean> questionsList=new ArrayList<QuestionBean>();
    private ArrayList<QuestionBean> sortedQuestionsList=new ArrayList<QuestionBean>(); 
    private ArrayList<QuestionBean> selectedQuestionsList=new ArrayList<QuestionBean>();
    private ArrayList<Integer> addedSequence = new ArrayList<Integer>();
    private QuestionPanel currentPanel = null;
    private javax.swing.JButton[] nonSelectedButtonsArray,selectedButtonsArray;
    private int shuffleValue;
    private ArrayList<QuestionBean> yearWiseQuestuionsList;
    private ArrayList<SubjectBean> subjectList;
    private SubjectBean subFirstBean;
    private SubjectBean subSecondBean;
    private SubjectBean subThirdBean;
    private boolean isQuestion;
    private String printingPaperType;
    private ArrayList<ImageRatioBean> imageRatioList;
    private PrintedTestBean printedTestBean;
    private String groupName;
    private ArrayList<Integer> saveTestSelectedQuesIdList;
    private boolean testSaveStatus;

    public MultipleYearQuestionsSelection() {
        initComponents();
    }
    
    //Saved test Resume
    public MultipleYearQuestionsSelection(ArrayList<CountYearBean> selectedYearList,ArrayList<QuestionBean> questionsList,ArrayList<QuestionBean> selectedQuestionsList,String printingPaperType,PrintedTestBean printedTestBean) {
        initComponents();
        this.getContentPane().setBackground(Color.white);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        currentPanel = PanelQues1;
        this.questionsList = questionsList;
        this.selectedQuestionsList = selectedQuestionsList;
        this.printingPaperType = printingPaperType;
        this.selectedYearList = selectedYearList;
        this.printedTestBean = printedTestBean;
        totalSelectedQueCount = selectedQuestionsList.size();
        currentPanel = PanelQues1;
        TxtFldQuestionNumber.setColumns(3);
        TxtFldQuestionNumber.setHorizontalAlignment(JTextField.CENTER);
        shuffleValue = 0;
        yearWiseQuestuionsList = new ArrayList<QuestionBean>();
        setYearCombo();
        imageRatioList = new ImageRatioOperation().getImageRatioList();
        CmbYear.removeAllItems();
        if(selectedYearList.size() != 1) {
            LblYear.setText("Year :");
            CmbYear.setVisible(true);
            for(CountYearBean countYearBean : selectedYearList) 
                CmbYear.addItem(countYearBean.getYear()+" ( Total : "+countYearBean.getTotalQuestions()+")");
        } else {
            LblYear.setText("Year :"+selectedYearList.get(0).getYear()+" ( Total : "+selectedYearList.get(0).getTotalQuestions()+")");
            CmbYear.setVisible(false);
        }
        subjectList = new SubjectOperation().getSubjectsList();
        int index = 0;
        CmbSubject.removeAllItems();
        for(SubjectBean subjectBean : subjectList) {
            if(index == 0)
                subFirstBean = subjectBean;
            else if(index == 1)
                subSecondBean = subjectBean;
            else if(index == 2)
                subThirdBean = subjectBean;
            CmbSubject.addItem(subjectBean.getSubjectName());
            index++;
        }
        saveTestSelectedQuesIdList = new ArrayList<Integer>();
        for(QuestionBean bean : selectedQuestionsList) 
            saveTestSelectedQuesIdList.add(bean.getQuestionId());
        testSaveStatus = true; 
        LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
        groupName = new ProcessManager().getGroupName().trim();
        LblHeading.setText(groupName);
//        setQuesType();
//        BtnModify.setVisible(false);
    }
    
    //Select Questions YearWise
    public MultipleYearQuestionsSelection(ArrayList<CountYearBean> selectedYearList,ArrayList<QuestionBean> questionsList,String printingPaperType) {
        initComponents();
        this.getContentPane().setBackground(Color.white);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        currentPanel = PanelQues1;
        this.questionsList = questionsList;
        this.printingPaperType = printingPaperType;
        this.selectedYearList = selectedYearList;
        currentPanel = PanelQues1;
        imageRatioList = new ImageRatioOperation().getImageRatioList();
        TxtFldQuestionNumber.setColumns(3);
        TxtFldQuestionNumber.setHorizontalAlignment(JTextField.CENTER);
        shuffleValue = 0;
        yearWiseQuestuionsList = new ArrayList<QuestionBean>();
        setYearCombo();
        CmbYear.removeAllItems();
        if(selectedYearList.size() != 1) {
            LblYear.setText("Year :");
            CmbYear.setVisible(true);
            for(CountYearBean countYearBean : selectedYearList) 
                CmbYear.addItem(countYearBean.getYear()+" ( Total : "+countYearBean.getTotalQuestions()+")");
        } else {
            LblYear.setText("Year :"+selectedYearList.get(0).getYear()+" ( Total : "+selectedYearList.get(0).getTotalQuestions()+")");
            CmbYear.setVisible(false);
        }
        subjectList = new SubjectOperation().getSubjectsList();
        int index = 0;
        CmbSubject.removeAllItems();
        for(SubjectBean subjectBean : subjectList) {
            if(index == 0)
                subFirstBean = subjectBean;
            else if(index == 1)
                subSecondBean = subjectBean;
            else if(index == 2)
                subThirdBean = subjectBean;
            CmbSubject.addItem(subjectBean.getSubjectName());
            index++;
        }
        printedTestBean = null;
        testSaveStatus = false;
        saveTestSelectedQuesIdList = null;
        groupName = new ProcessManager().getGroupName().trim();
        LblHeading.setText(groupName);
//        BtnModify.setVisible(false);
    }
        
    private void setQuesType() {
        ArrayList<QuestionBean> tempSortedQue = new ArrayList<QuestionBean>();
        ArrayList<QuestionBean> tempQueSub = new ArrayList<QuestionBean>();
        ArrayList<QuestionBean> tempQueUsed = new ArrayList<QuestionBean>();
        ArrayList<QuestionBean> tempSelection = new ArrayList<QuestionBean>();
        tempSortedQue = yearWiseQuestuionsList;
        for(QuestionBean questionsBean:yearWiseQuestuionsList) {
            if (ChkSubject.isSelected()) {
                if (CmbSubject.getSelectedIndex() == 0 && questionsBean.getSubjectId() == subFirstBean.getSubjectId()) {
                    tempQueSub.add(questionsBean);
                } else if (CmbSubject.getSelectedIndex() == 1 && questionsBean.getSubjectId() == subSecondBean.getSubjectId()) {
                    tempQueSub.add(questionsBean);
                } else if (CmbSubject.getSelectedIndex() == 2 && questionsBean.getSubjectId() == subThirdBean.getSubjectId()){
                    tempQueSub.add(questionsBean);
                }
            }
            
            if (ChkUsed.isSelected()) {
                if (CmbUsed.getSelectedIndex() == 0 && questionsBean.getAttempt() == 1) {
                    tempQueUsed.add(questionsBean);
                } else if (CmbUsed.getSelectedIndex() == 1 && questionsBean.getAttempt() == 0) {
                    tempQueUsed.add(questionsBean);
                }
            }
        }
        
        if(tempSortedQue.size()>0 && ChkSubject.isSelected()) {
            for(QuestionBean bean : tempSortedQue) {
                if(tempQueSub.contains(bean)) {
                    tempSelection.add(bean);
                }
            }
            
            tempSortedQue = tempSelection;
            tempSelection = new ArrayList<QuestionBean>();
        }
        
        if(tempSortedQue.size()>0 && ChkUsed.isSelected()) {
            for(QuestionBean bean : tempSortedQue) {
                if(tempQueUsed.contains(bean)) {
                    tempSelection.add(bean);
                }
            }
            tempSortedQue = tempSelection;
            tempSelection = new ArrayList<QuestionBean>();
        }
        
        if(tempSortedQue.size() > 0) 
            isQuestion = true;
        else
            isQuestion = false;
        
        LblCount.setText("COUNT: " + tempSortedQue.size());
            
        addedSequence = new ArrayList<Integer>();
        int i=0;
        sortedQuestionsList =new ArrayList<QuestionBean>();
        for(QuestionBean bean:tempSortedQue) {
            if(selectedQuestionsList.contains(bean)) {
                bean.setSelected(true);
                addedSequence.add(i);
            }
            sortedQuestionsList.add(bean);
            i++;
        }
        if(shuffleValue == 1 && addedSequence.size() != 0){
            Collections.shuffle(addedSequence);
        }
//        currentIndex = 0;
        setPanel(true);
    }
    
    private void setPanel(boolean flag) {
        if(isQuestion) {
            QuestionBean questionBean=sortedQuestionsList.get(currentIndex);
            currentPanel = (currentPanel == PanelQues1) ? PanelQues2 : PanelQues1;
            //set Question       
            currentPanel.setQuestionsOnPanel(questionBean, (currentIndex + 1),imageRatioList);
            TxtFldQuestionNumber.setText((currentIndex + 1) + "");
            //slide panel    
            MiddleBodyPanel.nextSlidPanel(1, currentPanel, flag);
            MiddleBodyPanel.refresh();
            if (questionBean.isSelected()) {
                BtnAddRemove.setText("Remove");
                BtnAddRemove.setForeground(Color.red);
            } else {
                BtnAddRemove.setText("Add");
                BtnAddRemove.setForeground(Color.black);
            }
            setButtonOnPanel();
        } else {
            MiddleBodyPanel.nextSlidPanel(1, noQuestionPanel1, flag);
            MiddleBodyPanel.refresh();
            setButtonOnPanel();
        }
    }

    public void setQuestion(String actionCommand) {
        currentIndex = Integer.parseInt(actionCommand.split(" ")[1]);
        setPanel(false);
    }

    public void setButtonOnPanel() {
        LeftBodyPanel.removeAll();
        RightBodyPanel.removeAll();
        nonSelectedButtonsArray = new JButton[sortedQuestionsList.size()];
        selectedButtonsArray = new JButton[sortedQuestionsList.size()];
        int i, j;
        i = 0; j = 0;
        for (int x = 0; x < sortedQuestionsList.size(); x++) {
            if (sortedQuestionsList.get(x).isSelected()) {
                selectedButtonsArray[i] = new javax.swing.JButton();
                selectedButtonsArray[i].setActionCommand(" " + x);
                selectedButtonsArray[i].setText("" + (x + 1));
                selectedButtonsArray[i].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setQuestion(e.getActionCommand());
                    }
                });
                i++;
            } else {
                nonSelectedButtonsArray[j] = new javax.swing.JButton();
                nonSelectedButtonsArray[j].setActionCommand(" " + x);
                nonSelectedButtonsArray[j].setText("" + (x + 1));
                nonSelectedButtonsArray[j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setQuestion(e.getActionCommand());
                    }
                });
                j++;
            }
        }
        
        GridBagConstraints cons = null;
        GridBagLayout layout = null;

        cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        cons.insets = new java.awt.Insets(1, 3, 1, 3);
        layout = new GridBagLayout();
        for (int x = 0; x < i; x++) {
            int uu = addedSequence.get(x);
            uu++;
            String uus = "" + uu;
            if (x % 20 == 0) {
                cons.gridx++;
                cons.gridy = 0;
            }
            for (int y = 0; y < addedSequence.size(); y++) {
                if (selectedButtonsArray[y].getText().equalsIgnoreCase(uus)) {
                    layout.setConstraints(selectedButtonsArray[y], cons);
                    LeftBodyPanel.setLayout(layout);
                    LeftBodyPanel.add(selectedButtonsArray[y], cons);
                    cons.gridy++;
                } else {
                }
            }
        }
        cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        cons.insets = new java.awt.Insets(1, 3, 1, 3);
        layout = new GridBagLayout();
        for (int x = 0; x < j; x++) {
            if (x % 20 == 0) {
                cons.gridx++;
                cons.gridy = 0;
            }
            layout.setConstraints(nonSelectedButtonsArray[x], cons);
            
            RightBodyPanel.setLayout(layout);
            RightBodyPanel.add(nonSelectedButtonsArray[x], cons);
            cons.gridy++;
        }
        
        LeftBodyPanel.validate();
        RightBodyPanel.validate();
        LeftBodyPanel.repaint();
        RightBodyPanel.repaint();
        LeftBodyScrollPane.validate();
        RightBodyScrollPane.validate();
        
    }
       
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupAnimation = new javax.swing.ButtonGroup();
        HeaderPanel = new javax.swing.JPanel();
        LblYear = new javax.swing.JLabel();
        CmbYear = new javax.swing.JComboBox();
        LblCount = new javax.swing.JLabel();
        SubHeaderPanel = new javax.swing.JPanel();
        SortPanel = new javax.swing.JPanel();
        CmbSubject = new javax.swing.JComboBox();
        ChkSubject = new javax.swing.JCheckBox();
        ChkUsed = new javax.swing.JCheckBox();
        CmbUsed = new javax.swing.JComboBox();
        LblHeading = new javax.swing.JLabel();
        LblNonSelectedQue = new javax.swing.JLabel();
        LblSelectedCount = new javax.swing.JLabel();
        LeftBodyScrollPane = new javax.swing.JScrollPane();
        LeftBodyPanel = new javax.swing.JPanel();
        MiddleBodyScrollPane = new javax.swing.JScrollPane();
        MiddleBodyPanel = new com.ui.support.pages.JPanelsSliding();
        PanelQues1 = new com.ui.support.pages.QuestionPanel();
        PanelQues2 = new com.ui.support.pages.QuestionPanel();
        noQuestionPanel1 = new com.ui.support.pages.NoQuestionPanel();
        RightBodyScrollPane = new javax.swing.JScrollPane();
        RightBodyPanel = new javax.swing.JPanel();
        BodyBottomPanel = new javax.swing.JPanel();
        BtnAddRemove = new javax.swing.JButton();
        BtnHome = new javax.swing.JButton();
        BtnShuffle = new javax.swing.JButton();
        BtnPdf = new javax.swing.JButton();
        BtnWord = new javax.swing.JButton();
        BtnChangeChapter = new javax.swing.JButton();
        FooterPanel = new javax.swing.JPanel();
        SubFooterPanel = new javax.swing.JPanel();
        TxtFldStart = new javax.swing.JTextField();
        TxtFldEnd = new javax.swing.JTextField();
        BtnRandomAdd = new javax.swing.JButton();
        ChbkRandom = new javax.swing.JCheckBox();
        BtnRemoveAll = new javax.swing.JButton();
        BtnModify = new javax.swing.JButton();
        BtnFirst = new javax.swing.JButton();
        BtnPrevious = new javax.swing.JButton();
        BtnNext = new javax.swing.JButton();
        TxtFldQuestionNumber = new javax.swing.JTextField();
        btnLast = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("CruncherSoft's Medical CET+JEE Software 2014");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        HeaderPanel.setBackground(new java.awt.Color(240, 255, 255));
        HeaderPanel.setName("HeaderPanel"); // NOI18N

        LblYear.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        LblYear.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LblYear.setText("Year :");
        LblYear.setName("LblYear"); // NOI18N

        CmbYear.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        CmbYear.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Easy", "Medium", "Hard" }));
        CmbYear.setName("CmbYear"); // NOI18N
        CmbYear.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbYearItemStateChanged(evt);
            }
        });

        LblCount.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        LblCount.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblCount.setText("Count");
        LblCount.setToolTipText("");
        LblCount.setName("LblCount"); // NOI18N

        SubHeaderPanel.setName("SubHeaderPanel"); // NOI18N

        SortPanel.setName("SortPanel"); // NOI18N

        CmbSubject.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        CmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Physics", "Chemistry", "Mathematics" }));
        CmbSubject.setEnabled(false);
        CmbSubject.setName("CmbSubject"); // NOI18N
        CmbSubject.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbSubjectItemStateChanged(evt);
            }
        });

        ChkSubject.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        ChkSubject.setForeground(new java.awt.Color(255, 0, 0));
        ChkSubject.setText("Subject :");
        ChkSubject.setName("ChkSubject"); // NOI18N
        ChkSubject.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkSubjectItemStateChanged(evt);
            }
        });

        ChkUsed.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        ChkUsed.setForeground(new java.awt.Color(255, 0, 0));
        ChkUsed.setText("Used");
        ChkUsed.setName("ChkUsed"); // NOI18N
        ChkUsed.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkUsedItemStateChanged(evt);
            }
        });

        CmbUsed.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        CmbUsed.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Used", "Not Used" }));
        CmbUsed.setEnabled(false);
        CmbUsed.setName("CmbUsed"); // NOI18N
        CmbUsed.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbUsedItemStateChanged(evt);
            }
        });

        LblHeading.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        LblHeading.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblHeading.setText("MH-CET");
        LblHeading.setName("LblHeading"); // NOI18N

        javax.swing.GroupLayout SortPanelLayout = new javax.swing.GroupLayout(SortPanel);
        SortPanel.setLayout(SortPanelLayout);
        SortPanelLayout.setHorizontalGroup(
            SortPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SortPanelLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(ChkSubject)
                .addGap(18, 18, 18)
                .addComponent(CmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(211, 211, 211)
                .addComponent(LblHeading)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 237, Short.MAX_VALUE)
                .addComponent(ChkUsed, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(CmbUsed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
        );
        SortPanelLayout.setVerticalGroup(
            SortPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SortPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(SortPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(SortPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(ChkUsed)
                        .addComponent(CmbUsed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(SortPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(CmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ChkSubject)
                        .addComponent(LblHeading, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        SubHeaderPanel.add(SortPanel);

        LblNonSelectedQue.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 12)); // NOI18N
        LblNonSelectedQue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblNonSelectedQue.setText("Non Selected Questions");
        LblNonSelectedQue.setName("LblNonSelectedQue"); // NOI18N

        LblSelectedCount.setFont(new java.awt.Font("Microsoft YaHei UI", 1, 13)); // NOI18N
        LblSelectedCount.setText("Selected Questions : 0");
        LblSelectedCount.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        LblSelectedCount.setName("LblSelectedCount"); // NOI18N
        LblSelectedCount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblSelectedCountMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblSelectedCountMouseExited(evt);
            }
        });

        javax.swing.GroupLayout HeaderPanelLayout = new javax.swing.GroupLayout(HeaderPanel);
        HeaderPanel.setLayout(HeaderPanelLayout);
        HeaderPanelLayout.setHorizontalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderPanelLayout.createSequentialGroup()
                .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(HeaderPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(SubHeaderPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 1000, Short.MAX_VALUE)
                            .addGroup(HeaderPanelLayout.createSequentialGroup()
                                .addComponent(LblYear)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(CmbYear, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(LblCount, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(HeaderPanelLayout.createSequentialGroup()
                        .addComponent(LblSelectedCount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(LblNonSelectedQue, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        HeaderPanelLayout.setVerticalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblYear)
                    .addComponent(CmbYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblCount))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(SubHeaderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblNonSelectedQue)
                    .addComponent(LblSelectedCount))
                .addContainerGap())
        );

        LeftBodyScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        LeftBodyScrollPane.setName("LeftBodyScrollPane"); // NOI18N

        LeftBodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        LeftBodyPanel.setName("LeftBodyPanel"); // NOI18N

        javax.swing.GroupLayout LeftBodyPanelLayout = new javax.swing.GroupLayout(LeftBodyPanel);
        LeftBodyPanel.setLayout(LeftBodyPanelLayout);
        LeftBodyPanelLayout.setHorizontalGroup(
            LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 150, Short.MAX_VALUE)
        );
        LeftBodyPanelLayout.setVerticalGroup(
            LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 514, Short.MAX_VALUE)
        );

        LeftBodyScrollPane.setViewportView(LeftBodyPanel);

        MiddleBodyScrollPane.setName("MiddleBodyScrollPane"); // NOI18N

        MiddleBodyPanel.setBackground(new java.awt.Color(233, 238, 255));
        MiddleBodyPanel.setName("MiddleBodyPanel"); // NOI18N
        MiddleBodyPanel.setLayout(new java.awt.CardLayout());

        PanelQues1.setBackground(new java.awt.Color(185, 225, 254));
        PanelQues1.setFont(new java.awt.Font("Centaur", 0, 11)); // NOI18N
        PanelQues1.setName("PanelQues1"); // NOI18N
        MiddleBodyPanel.add(PanelQues1, "panelQues");

        PanelQues2.setBackground(new java.awt.Color(185, 225, 254));
        PanelQues2.setName("PanelQues2"); // NOI18N
        MiddleBodyPanel.add(PanelQues2, "card3");

        noQuestionPanel1.setName("noQuestionPanel1"); // NOI18N
        MiddleBodyPanel.add(noQuestionPanel1, "noQues");

        MiddleBodyScrollPane.setViewportView(MiddleBodyPanel);

        RightBodyScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        RightBodyScrollPane.setName("RightBodyScrollPane"); // NOI18N

        RightBodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        RightBodyPanel.setName("RightBodyPanel"); // NOI18N

        javax.swing.GroupLayout RightBodyPanelLayout = new javax.swing.GroupLayout(RightBodyPanel);
        RightBodyPanel.setLayout(RightBodyPanelLayout);
        RightBodyPanelLayout.setHorizontalGroup(
            RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 227, Short.MAX_VALUE)
        );
        RightBodyPanelLayout.setVerticalGroup(
            RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 514, Short.MAX_VALUE)
        );

        RightBodyScrollPane.setViewportView(RightBodyPanel);

        BodyBottomPanel.setBackground(new java.awt.Color(240, 255, 255));
        BodyBottomPanel.setName("BodyBottomPanel"); // NOI18N

        BtnAddRemove.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnAddRemove.setText("Add");
        BtnAddRemove.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnAddRemove.setName("BtnAddRemove"); // NOI18N
        BtnAddRemove.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnAddRemoveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnAddRemoveMouseExited(evt);
            }
        });
        BtnAddRemove.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                BtnAddRemoveItemStateChanged(evt);
            }
        });
        BtnAddRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAddRemoveActionPerformed(evt);
            }
        });
        BtnAddRemove.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAddRemoveKeyPressed(evt);
            }
        });
        BodyBottomPanel.add(BtnAddRemove);

        BtnHome.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnHome.setText("Home");
        BtnHome.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnHome.setName("BtnHome"); // NOI18N
        BtnHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnHomeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnHomeMouseExited(evt);
            }
        });
        BtnHome.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                BtnHomeItemStateChanged(evt);
            }
        });
        BtnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHomeActionPerformed(evt);
            }
        });
        BodyBottomPanel.add(BtnHome);

        BtnShuffle.setFont(new java.awt.Font("Microsoft JhengHei", 1, 13)); // NOI18N
        BtnShuffle.setText("Shuffle");
        BtnShuffle.setName("BtnShuffle"); // NOI18N
        BtnShuffle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnShuffleActionPerformed(evt);
            }
        });
        BodyBottomPanel.add(BtnShuffle);

        BtnPdf.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnPdf.setText("Export Pdf");
        BtnPdf.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnPdf.setName("BtnPdf"); // NOI18N
        BtnPdf.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnPdfMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnPdfMouseExited(evt);
            }
        });
        BtnPdf.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                BtnPdfItemStateChanged(evt);
            }
        });
        BtnPdf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPdfActionPerformed(evt);
            }
        });
        BtnPdf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPdfKeyPressed(evt);
            }
        });
        BodyBottomPanel.add(BtnPdf);

        BtnWord.setFont(new java.awt.Font("Microsoft JhengHei", 1, 13)); // NOI18N
        BtnWord.setText("Export Word");
        BtnWord.setName("BtnWord"); // NOI18N
        BtnWord.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnWordActionPerformed(evt);
            }
        });
        BodyBottomPanel.add(BtnWord);

        BtnChangeChapter.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnChangeChapter.setText("Change Paper Selections");
        BtnChangeChapter.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnChangeChapter.setName("BtnChangeChapter"); // NOI18N
        BtnChangeChapter.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnChangeChapterMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnChangeChapterMouseExited(evt);
            }
        });
        BtnChangeChapter.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                BtnChangeChapterItemStateChanged(evt);
            }
        });
        BtnChangeChapter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnChangeChapterActionPerformed(evt);
            }
        });
        BtnChangeChapter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnChangeChapterKeyPressed(evt);
            }
        });
        BodyBottomPanel.add(BtnChangeChapter);

        FooterPanel.setBackground(new java.awt.Color(240, 248, 255));
        FooterPanel.setName("FooterPanel"); // NOI18N

        SubFooterPanel.setName("SubFooterPanel"); // NOI18N

        TxtFldStart.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 12)); // NOI18N
        TxtFldStart.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtFldStart.setName("TxtFldStart"); // NOI18N
        TxtFldStart.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtFldStartKeyPressed(evt);
            }
        });

        TxtFldEnd.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 12)); // NOI18N
        TxtFldEnd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtFldEnd.setName("TxtFldEnd"); // NOI18N
        TxtFldEnd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtFldEndKeyPressed(evt);
            }
        });

        BtnRandomAdd.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnRandomAdd.setText("Add");
        BtnRandomAdd.setName("BtnRandomAdd"); // NOI18N
        BtnRandomAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRandomAddActionPerformed(evt);
            }
        });

        ChbkRandom.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        ChbkRandom.setText("Random");
        ChbkRandom.setName("ChbkRandom"); // NOI18N
        ChbkRandom.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChbkRandomItemStateChanged(evt);
            }
        });

        BtnRemoveAll.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnRemoveAll.setText("Remove All");
        BtnRemoveAll.setName("BtnRemoveAll"); // NOI18N
        BtnRemoveAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRemoveAllActionPerformed(evt);
            }
        });

        BtnModify.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnModify.setText("Modify");
        BtnModify.setName("BtnModify"); // NOI18N
        BtnModify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnModifyActionPerformed(evt);
            }
        });

        BtnFirst.setBackground(new java.awt.Color(255, 255, 255));
        BtnFirst.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first.png"))); // NOI18N
        BtnFirst.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnFirst.setMargin(new java.awt.Insets(0, 0, 0, 0));
        BtnFirst.setName("BtnFirst"); // NOI18N
        BtnFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnFirstMouseExited(evt);
            }
        });
        BtnFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnFirstActionPerformed(evt);
            }
        });

        BtnPrevious.setBackground(new java.awt.Color(255, 255, 255));
        BtnPrevious.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/prev.png"))); // NOI18N
        BtnPrevious.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnPrevious.setIconTextGap(0);
        BtnPrevious.setMargin(new java.awt.Insets(0, 0, 0, 0));
        BtnPrevious.setName("BtnPrevious"); // NOI18N
        BtnPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnPreviousMouseExited(evt);
            }
        });
        BtnPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPreviousActionPerformed(evt);
            }
        });

        BtnNext.setBackground(new java.awt.Color(255, 255, 255));
        BtnNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/next.png"))); // NOI18N
        BtnNext.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnNext.setMargin(new java.awt.Insets(0, 0, 0, 0));
        BtnNext.setName("BtnNext"); // NOI18N
        BtnNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnNextMouseExited(evt);
            }
        });
        BtnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnNextActionPerformed(evt);
            }
        });

        TxtFldQuestionNumber.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 12)); // NOI18N
        TxtFldQuestionNumber.setText("0000");
        TxtFldQuestionNumber.setName("TxtFldQuestionNumber"); // NOI18N
        TxtFldQuestionNumber.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                TxtFldQuestionNumberMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                TxtFldQuestionNumberMouseExited(evt);
            }
        });
        TxtFldQuestionNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtFldQuestionNumberKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtFldQuestionNumberKeyReleased(evt);
            }
        });

        btnLast.setBackground(new java.awt.Color(255, 255, 255));
        btnLast.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last.png"))); // NOI18N
        btnLast.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLast.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnLast.setName("btnLast"); // NOI18N
        btnLast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnLastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnLastMouseExited(evt);
            }
        });
        btnLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLastActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout SubFooterPanelLayout = new javax.swing.GroupLayout(SubFooterPanel);
        SubFooterPanel.setLayout(SubFooterPanelLayout);
        SubFooterPanelLayout.setHorizontalGroup(
            SubFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SubFooterPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BtnFirst)
                .addGap(5, 5, 5)
                .addComponent(BtnPrevious)
                .addGap(5, 5, 5)
                .addComponent(TxtFldQuestionNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(BtnNext)
                .addGap(5, 5, 5)
                .addComponent(btnLast)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ChbkRandom)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TxtFldStart, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(TxtFldEnd, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnRandomAdd)
                .addGap(10, 10, 10)
                .addComponent(BtnRemoveAll)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnModify)
                .addContainerGap())
        );
        SubFooterPanelLayout.setVerticalGroup(
            SubFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SubFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(BtnFirst)
                .addComponent(BtnPrevious)
                .addGroup(SubFooterPanelLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(TxtFldQuestionNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(BtnNext)
                .addComponent(btnLast))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SubFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(TxtFldStart, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(BtnRandomAdd)
                .addComponent(TxtFldEnd, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(ChbkRandom)
                .addComponent(BtnRemoveAll)
                .addComponent(BtnModify))
        );

        FooterPanel.add(SubFooterPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(FooterPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(HeaderPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(LeftBodyScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(MiddleBodyScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(RightBodyScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(BodyBottomPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 1020, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(HeaderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(LeftBodyScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                    .addComponent(RightBodyScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                    .addComponent(MiddleBodyScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BodyBottomPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FooterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
private void BtnFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnFirstActionPerformed
    if(isQuestion) {
        if (currentIndex != 0) {
            currentIndex = 0;
            setPanel(true);
        } else {
            JOptionPane.showMessageDialog(null, "This is First Question.");
        }
    }
}//GEN-LAST:event_BtnFirstActionPerformed

private void BtnPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPreviousActionPerformed
    if(isQuestion) {
        if (currentIndex != 0) {
            currentIndex--;
            setPanel(true);
        } else {
            JOptionPane.showMessageDialog(null, "This is First Question.");
        }
    }
}//GEN-LAST:event_BtnPreviousActionPerformed

private void TxtFldQuestionNumberKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtFldQuestionNumberKeyReleased
    if (evt.getKeyCode() == 10) {
        if(!TxtFldQuestionNumber.getText().isEmpty()) {
            int i = Integer.parseInt(TxtFldQuestionNumber.getText());
            int size = sortedQuestionsList.size();
            if ((i < 1) || (i > size))
                JOptionPane.showMessageDialog(rootPane, "Invalid Question Number");
            else
                currentIndex = i - 1;
        } else {
            JOptionPane.showMessageDialog(rootPane, "Invalid Question Number");
        }
        setPanel(false);
    }
}//GEN-LAST:event_TxtFldQuestionNumberKeyReleased

    private void BtnNextActionPerformed(java.awt.event.ActionEvent evt, boolean b) {
        int last=sortedQuestionsList.size()-1;
        if (currentIndex == last) {
            currentIndex = 0;
            setPanel(false);
        } else {
            JOptionPane.showMessageDialog(null, "This is Last Question.");
        }
    }

private void BtnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnNextActionPerformed
    if(isQuestion) {
        int last=sortedQuestionsList.size()-1;
        if (currentIndex < last) {
            currentIndex++;
            setPanel(false);
        } else {
            JOptionPane.showMessageDialog(null, "This is Last Question.");
        }
    }
}//GEN-LAST:event_BtnNextActionPerformed
private void btnLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLastActionPerformed
    if(isQuestion) {
        int last=sortedQuestionsList.size()-1;
        if (currentIndex < last) {
            currentIndex = last;
            setPanel(false);
        } else {
            JOptionPane.showMessageDialog(null, "This is Last Question.");
        }
    }
}//GEN-LAST:event_btnLastActionPerformed
private void BtnAddRemoveItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_BtnAddRemoveItemStateChanged
}//GEN-LAST:event_BtnAddRemoveItemStateChanged
private void BtnAddRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAddRemoveActionPerformed
    if(isQuestion){
        int last=sortedQuestionsList.size()-1;
        int i = currentIndex;
        if ((currentIndex <= last) && (currentIndex >= 0)) {
            if (BtnAddRemove.getText().equals("Add")) {
                sortedQuestionsList.get(currentIndex).setSelected(true);
                totalSelectedQueCount++;
                addedSequence.add(currentIndex);
                selectedQuestionsList.add(sortedQuestionsList.get(currentIndex));
            } else {
                int index = selectedQuestionsList.indexOf(sortedQuestionsList.get(currentIndex));
                sortedQuestionsList.get(currentIndex).setSelected(false);
                selectedQuestionsList.remove(index);
                int iii = addedSequence.indexOf(currentIndex);
                addedSequence.remove(iii);
                totalSelectedQueCount--;
                if(selectedQuestionsList.isEmpty())
                    shuffleValue = 0;
            }
            LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
            setButtonOnPanel();
//            LeftBodyPanel.validate();
//            LeftBodyPanel.repaint();
//            RightBodyPanel.validate();
//            RightBodyPanel.repaint();
        }

        if (i < last) {
            BtnNextActionPerformed(evt);
        } else if (i == last) {
            boolean b = true;
            BtnNextActionPerformed(evt, b);
        }
    }
}//GEN-LAST:event_BtnAddRemoveActionPerformed
private void BtnHomeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_BtnHomeItemStateChanged
// TODO add your handling code here:
}//GEN-LAST:event_BtnHomeItemStateChanged
private void BtnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHomeActionPerformed
    new HomePage().setVisible(true);
    this.dispose();
}//GEN-LAST:event_BtnHomeActionPerformed
private void BtnAddRemoveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnAddRemoveMouseEntered
    BtnAddRemove.setForeground(Color.red);
}//GEN-LAST:event_BtnAddRemoveMouseEntered
private void BtnAddRemoveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnAddRemoveMouseExited
    BtnAddRemove.setForeground(Color.black);
}//GEN-LAST:event_BtnAddRemoveMouseExited
private void BtnHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnHomeMouseEntered
    BtnHome.setForeground(Color.red);
}//GEN-LAST:event_BtnHomeMouseEntered
private void BtnHomeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnHomeMouseExited
    BtnHome.setForeground(Color.black);
}//GEN-LAST:event_BtnHomeMouseExited
private void BtnAddRemoveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAddRemoveKeyPressed
}//GEN-LAST:event_BtnAddRemoveKeyPressed

private void BtnFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnFirstMouseEntered
}//GEN-LAST:event_BtnFirstMouseEntered

private void BtnFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnFirstMouseExited
}//GEN-LAST:event_BtnFirstMouseExited

private void BtnPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPreviousMouseEntered
}//GEN-LAST:event_BtnPreviousMouseEntered

private void BtnPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPreviousMouseExited
}//GEN-LAST:event_BtnPreviousMouseExited

private void BtnNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnNextMouseEntered
}//GEN-LAST:event_BtnNextMouseEntered

private void btnLastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLastMouseEntered
}//GEN-LAST:event_btnLastMouseEntered

private void BtnNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnNextMouseExited
}//GEN-LAST:event_BtnNextMouseExited

private void btnLastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLastMouseExited
}//GEN-LAST:event_btnLastMouseExited

private void TxtFldQuestionNumberMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TxtFldQuestionNumberMouseEntered
}//GEN-LAST:event_TxtFldQuestionNumberMouseEntered

private void TxtFldQuestionNumberMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TxtFldQuestionNumberMouseExited
}//GEN-LAST:event_TxtFldQuestionNumberMouseExited

private void LblSelectedCountMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblSelectedCountMouseEntered
}//GEN-LAST:event_LblSelectedCountMouseEntered

private void LblSelectedCountMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblSelectedCountMouseExited
}//GEN-LAST:event_LblSelectedCountMouseExited

private void BtnPdfMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPdfMouseEntered
// TODO add your handling code here:
}//GEN-LAST:event_BtnPdfMouseEntered

private void BtnPdfMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPdfMouseExited
// TODO add your handling code here:
}//GEN-LAST:event_BtnPdfMouseExited

private void BtnPdfItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_BtnPdfItemStateChanged
// TODO add your handling code here:
}//GEN-LAST:event_BtnPdfItemStateChanged

private void BtnPdfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPdfActionPerformed
    if(!selectedQuestionsList.isEmpty()) {
        new PdfPageSetup(selectedQuestionsList,subjectList,null,(Object)this,questionsList,printingPaperType).setVisible(true);
//        new PdfPageSetup(selectedQuestionsList,subjectList,null,this,questionsList,printingPaperType).setVisible(true);
//        setAttemptedValue();
        this.setEnabled(false);
    } else {    
        JOptionPane.showMessageDialog(rootPane, "Please Add Questions for Printing.");
    }
}//GEN-LAST:event_BtnPdfActionPerformed

private void BtnPdfKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPdfKeyPressed
// TODO add your handling code here:
}//GEN-LAST:event_BtnPdfKeyPressed

private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
    Object[] options = {"YES", "CANCEL"};
    int i = JOptionPane.showOptionDialog(null, "Are You Sure to Cancel Test?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
    if (i == 0) {
        new HomePage().setVisible(true);
        this.dispose();
    }
}//GEN-LAST:event_formWindowClosing

private void BtnRemoveAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRemoveAllActionPerformed
    if(isQuestion) {
        if(selectedQuestionsList.size()!=0) {
            for (int i = 0; i < questionsList.size(); i++) {
                questionsList.get(i).setSelected(false);
            }
            for(int i=0; i< sortedQuestionsList.size(); i++) {
                sortedQuestionsList.get(i).setSelected(false);
            }
            shuffleValue = 0;
            totalSelectedQueCount = 0;
            selectedQuestionsList = new ArrayList<QuestionBean>();
            BtnAddRemove.setText("Add");
            BtnAddRemove.setForeground(Color.black);
            TxtFldStart.setText("");
            TxtFldEnd.setText("");
            addedSequence = new ArrayList<Integer>();
            LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
            setButtonOnPanel();
//            LeftBodyPanel.validate();
//            LeftBodyPanel.repaint();
//            RightBodyPanel.validate();
//            RightBodyPanel.repaint();
            BodyBottomPanel.validate();
            BodyBottomPanel.revalidate();
            BodyBottomPanel.repaint();
        } else {
            JOptionPane.showMessageDialog(null, "You Are Not Selected Any Questions.");
        }
    }
}//GEN-LAST:event_BtnRemoveAllActionPerformed

private void BtnRandomAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRandomAddActionPerformed
    if(isQuestion) {
        int strt = 0, end = 0;
        String str1 = TxtFldStart.getText();
        String str2 = TxtFldEnd.getText();
        TxtFldStart.setText("");
        TxtFldEnd.setText("");
        if (ChbkRandom.isSelected()) {
            try {
                end = Integer.parseInt(str2);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Please enter valid number");
            }

            ArrayList<QuestionBean> tempRemainingList = new ArrayList<QuestionBean>();
            for(QuestionBean bean : sortedQuestionsList) {
                if(!bean.isSelected())
                    tempRemainingList.add(bean);
            }
            
            Collections.shuffle(tempRemainingList);

            if(end > 0 && selectedQuestionsList.size() == 0 && end < sortedQuestionsList.size()+1) {
                for(int i=0;i<end;i++) {
                    tempRemainingList.get(i).setSelected(true);
                    selectedQuestionsList.add(tempRemainingList.get(i));
                    int index1 = sortedQuestionsList.indexOf(tempRemainingList.get(i));
                    sortedQuestionsList.get(index1).setSelected(true);
                    addedSequence.add(index1);
    //                int index2 = questionsList.indexOf(tempRemainingList.get(i));
    //                questionsList.get(index2).setSelected(true);
                    totalSelectedQueCount ++;
                }            
            } else {
                if(end < tempRemainingList.size()+1 ) {
                    for(int i=0;i<end;i++) {
                        selectedQuestionsList.add(tempRemainingList.get(i));
                        int index1 = sortedQuestionsList.indexOf(tempRemainingList.get(i));
                        sortedQuestionsList.get(index1).setSelected(true);
                        addedSequence.add(index1);
    //                    int index2 = questionsList.indexOf(tempRemainingList.get(i));
    //                    questionsList.get(index2).setSelected(true); 
                        totalSelectedQueCount++;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Invalid range.\nYou Can Add ("+tempRemainingList.size()+") Questions.");
    //                JOptionPane.showMessageDialog(null, "Entered Count is out of range.");
                    TxtFldEnd.setText("");
                } 
            }

            LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
//            setButtonOnPanel();
//            LeftBodyPanel.validate();
//            LeftBodyPanel.repaint();
//            RightBodyPanel.validate();
//            RightBodyPanel.repaint();
            setPanel(false);
            BodyBottomPanel.validate();
            BodyBottomPanel.revalidate();
            BodyBottomPanel.repaint();
        } else {
            try {
                strt = Integer.parseInt(str1);
                end = Integer.parseInt(str2);
            } catch (Exception e) {
            }

            if (strt > 0 && end > 0) {
                if (strt < end) {
                    if(end < sortedQuestionsList.size()+1) {    
                        for (int i = strt - 1; i < end; i++) {
                            if(!(selectedQuestionsList.contains(sortedQuestionsList.get(i)))){
                                sortedQuestionsList.get(i).setSelected(true);
                                totalSelectedQueCount++;
                                selectedQuestionsList.add(sortedQuestionsList.get(i));
                                int index = questionsList.indexOf(sortedQuestionsList.get(i));
                                questionsList.get(index).setSelected(true);
                                addedSequence.add(i);
                            }
                        }
                        LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
                        setPanel(false);
//                        setButtonOnPanel();
//                        LeftBodyPanel.validate();
//                        LeftBodyPanel.repaint();
//                        RightBodyPanel.validate();
//                        RightBodyPanel.repaint();

                        BodyBottomPanel.validate();
                        BodyBottomPanel.revalidate();
                        BodyBottomPanel.repaint();
                    } else {
                        JOptionPane.showMessageDialog(null, "Entered Count is out of range.");
                    }
                } else {
                    if (strt < sortedQuestionsList.size()+1) {
                        for (int i = end - 1; i < strt; i++) {
                            if(!(selectedQuestionsList.contains(sortedQuestionsList.get(i)))) {
                                sortedQuestionsList.get(i).setSelected(true);
                                totalSelectedQueCount++;
                                selectedQuestionsList.add(sortedQuestionsList.get(i));
                                int index = questionsList.indexOf(sortedQuestionsList.get(i));
                                questionsList.get(index).setSelected(true);
                                addedSequence.add(i);
                            }
                        }
                        LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
                        setPanel(false);
//                        setButtonOnPanel();
//                        LeftBodyPanel.validate();
//                        LeftBodyPanel.repaint();
//                        RightBodyPanel.validate();
//                        RightBodyPanel.repaint();

                        BodyBottomPanel.validate();
                        BodyBottomPanel.revalidate();
                        BodyBottomPanel.repaint();
                    } else {
                        JOptionPane.showMessageDialog(null, "Entered Count is out of range.");
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please enter valid range");
            }
        }
    }
}//GEN-LAST:event_BtnRandomAddActionPerformed

private void ChkSubjectItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkSubjectItemStateChanged
    
//    if(!(selectedSequenceId.contains(1))) {
//        selectedSequenceId.add(1);
//    } else {
//        int index = selectedSequenceId.indexOf(1);
//        selectedSequenceId.remove(index);
//    }
    if (ChkSubject.isSelected()) {
        CmbSubject.setEnabled(true);
    } else {
        CmbSubject.setEnabled(false);
    }
//    setLevelQues();
    currentIndex = 0;
    setQuesType();
    
}//GEN-LAST:event_ChkSubjectItemStateChanged

private void ChkUsedItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkUsedItemStateChanged
//    if(!(selectedSequenceId.contains(2))) {
//        selectedSequenceId.add(2);
//    } else {
//        int index = selectedSequenceId.indexOf(2);
//        selectedSequenceId.remove(index);
//    }
//    System.out.println("Hello:"+selectedSequenceId);
    if (ChkUsed.isSelected()) {
        CmbUsed.setEnabled(true);
    } else {
        CmbUsed.setEnabled(false);
    }
//    setTypeQues();
    currentIndex = 0;
    setQuesType();
}//GEN-LAST:event_ChkUsedItemStateChanged

private void CmbUsedItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbUsedItemStateChanged
     if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) {
         if (ChkUsed.isSelected()) {
            currentIndex = 0;
            setQuesType();
//        setTypeQues();
        }
     }
}//GEN-LAST:event_CmbUsedItemStateChanged

private void CmbSubjectItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbSubjectItemStateChanged
    if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) {
        if (ChkSubject.isSelected()) {
    //        setLevelQues();
            currentIndex = 0;
            setQuesType();
        }
    }
}//GEN-LAST:event_CmbSubjectItemStateChanged

    private void BtnModifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnModifyActionPerformed
        if(isQuestion) {
            QuestionBean questionsBean = sortedQuestionsList.get(currentIndex);
            new ModifyPaperQuestion(questionsBean , this , currentIndex).setVisible(true);
            this.setVisible(false);
        }
    }//GEN-LAST:event_BtnModifyActionPerformed

    private void BtnChangeChapterMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnChangeChapterMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnChangeChapterMouseEntered

    private void BtnChangeChapterMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnChangeChapterMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnChangeChapterMouseExited

    private void BtnChangeChapterItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_BtnChangeChapterItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnChangeChapterItemStateChanged

    private void BtnChangeChapterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnChangeChapterActionPerformed
//        new SelectionChapterWise(subjectId, selectiontype).setVisible(true);
        new MultipleYearSelection(printingPaperType).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnChangeChapterActionPerformed

    private void BtnChangeChapterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnChangeChapterKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnChangeChapterKeyPressed

    private void ChbkRandomItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChbkRandomItemStateChanged
        // TODO add your handling code here:
        if(isQuestion) {
            if (ChbkRandom.isSelected()) {
                TxtFldStart.setEnabled(false);
            } else {
                TxtFldStart.setEnabled(true);
            }
        }
    }//GEN-LAST:event_ChbkRandomItemStateChanged

    private void BtnShuffleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnShuffleActionPerformed
        if(isQuestion) {
            if(addedSequence.size() > 1){
                Object[] options = {"YES", "NO"};
                int i = JOptionPane.showOptionDialog(null, "Sequence of question will be changed? Are you sure to continue?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                if (i == 0) {
    //                ShuffleQuestions();
                    shuffleValue = 1;
                    Collections.shuffle(addedSequence);
                    Collections.shuffle(selectedQuestionsList);
                    setButtonOnPanel();
                    JOptionPane.showMessageDialog(null, "Questions are shuffled.");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Unable to Shuffle, please add more questions.");
            }
        }
    }//GEN-LAST:event_BtnShuffleActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowClosed

    private void BtnWordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnWordActionPerformed
        // TODO add your handling code here:
        if(!selectedQuestionsList.isEmpty()) {
            if(shuffleValue == 1) 
                Collections.shuffle(selectedQuestionsList);
            
            new WordPageSetup(selectedQuestionsList, subjectList, null,(Object)this, questionsList, printingPaperType).setVisible(true);
            this.setEnabled(false);
        } else {    
            JOptionPane.showMessageDialog(this, "Please Add Questions for Printing.");
        }
    }//GEN-LAST:event_BtnWordActionPerformed

    private void CmbYearItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbYearItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) {
            if(CmbYear.getSelectedIndex() != -1){
                setYearCombo();
            }
        }
    }//GEN-LAST:event_CmbYearItemStateChanged

    private void TxtFldQuestionNumberKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtFldQuestionNumberKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() != 10) {
            keyPressed(evt, TxtFldQuestionNumber);
        }
    }                                               
    
    private void keyPressed(KeyEvent evt, JTextField text) {
        if ((evt.getKeyChar() >= '0' && evt.getKeyChar() <= '9') || evt.getKeyCode() == 8 || evt.getKeyCode() == 9 || evt.getKeyCode() == 13 || evt.getKeyCode() == 16 || evt.getKeyCode() == 17 || evt.getKeyCode() == 20 || evt.getKeyCode() == 27 || evt.getKeyCode() == 37 || evt.getKeyCode() == 38 || evt.getKeyCode() == 39 || evt.getKeyCode() == 40 || evt.getKeyCode() == 46) {
            text.setEditable(true);
        } else {
            text.setEditable(false);
            JOptionPane.showMessageDialog(null, "Please Enter Numbers Only.");
            text.setEditable(true);
        }
    }//GEN-LAST:event_TxtFldQuestionNumberKeyPressed

    private void TxtFldStartKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtFldStartKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() != 10) {
            keyPressed(evt, TxtFldStart);
        }
    }//GEN-LAST:event_TxtFldStartKeyPressed

    private void TxtFldEndKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtFldEndKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() != 10) {
            keyPressed(evt, TxtFldEnd);
        }
    }//GEN-LAST:event_TxtFldEndKeyPressed
    
    public static void main(String args[]) {
      
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BodyBottomPanel;
    private javax.swing.JButton BtnAddRemove;
    private javax.swing.JButton BtnChangeChapter;
    private javax.swing.JButton BtnFirst;
    private javax.swing.JButton BtnHome;
    private javax.swing.JButton BtnModify;
    private javax.swing.JButton BtnNext;
    private javax.swing.JButton BtnPdf;
    private javax.swing.JButton BtnPrevious;
    private javax.swing.JButton BtnRandomAdd;
    private javax.swing.JButton BtnRemoveAll;
    private javax.swing.JButton BtnShuffle;
    private javax.swing.JButton BtnWord;
    private javax.swing.JCheckBox ChbkRandom;
    private javax.swing.JCheckBox ChkSubject;
    private javax.swing.JCheckBox ChkUsed;
    private javax.swing.JComboBox CmbSubject;
    private javax.swing.JComboBox CmbUsed;
    private javax.swing.JComboBox CmbYear;
    private javax.swing.JPanel FooterPanel;
    private javax.swing.JPanel HeaderPanel;
    private javax.swing.JLabel LblCount;
    private javax.swing.JLabel LblHeading;
    private javax.swing.JLabel LblNonSelectedQue;
    private javax.swing.JLabel LblSelectedCount;
    private javax.swing.JLabel LblYear;
    private javax.swing.JPanel LeftBodyPanel;
    private javax.swing.JScrollPane LeftBodyScrollPane;
    private com.ui.support.pages.JPanelsSliding MiddleBodyPanel;
    private javax.swing.JScrollPane MiddleBodyScrollPane;
    private com.ui.support.pages.QuestionPanel PanelQues1;
    private com.ui.support.pages.QuestionPanel PanelQues2;
    private javax.swing.JPanel RightBodyPanel;
    private javax.swing.JScrollPane RightBodyScrollPane;
    private javax.swing.JPanel SortPanel;
    private javax.swing.JPanel SubFooterPanel;
    private javax.swing.JPanel SubHeaderPanel;
    private javax.swing.JTextField TxtFldEnd;
    private javax.swing.JTextField TxtFldQuestionNumber;
    private javax.swing.JTextField TxtFldStart;
    private javax.swing.ButtonGroup btnGroupAnimation;
    private javax.swing.JButton btnLast;
    private com.ui.support.pages.NoQuestionPanel noQuestionPanel1;
    // End of variables declaration//GEN-END:variables

    private void setYearCombo() {
        yearWiseQuestuionsList.clear();
        String year = "";
        if(CmbYear.isVisible())
            year = selectedYearList.get(CmbYear.getSelectedIndex()).getYear().trim();
        else
            year = selectedYearList.get(0).getYear().trim();
        for(QuestionBean questionsBean : questionsList) {
            if(questionsBean.getYear().trim().equals(year.trim()))
                yearWiseQuestuionsList.add(questionsBean);
        }
        currentIndex = 0;
        setQuesType();
    }
    
    public void loadAfterModification(QuestionBean questionsBean,boolean ratioUpdate) {
        if(questionsBean != null) {
            QuestionBean oldQuesBean = sortedQuestionsList.get(currentIndex); 
            questionsBean.setSelected(oldQuesBean.isSelected());
            int index1 = questionsList.indexOf(oldQuesBean);
            int index2 = selectedQuestionsList.indexOf(oldQuesBean);
            int index3 = yearWiseQuestuionsList.indexOf(oldQuesBean);
            
            sortedQuestionsList.set(currentIndex, questionsBean);
            questionsList.set(index1, questionsBean);
            if(index2 != -1)
                selectedQuestionsList.set(index2, questionsBean);
            if(index3 != -1)
                yearWiseQuestuionsList.set(index3, questionsBean);
            
            if(ratioUpdate)
                imageRatioList = new ImageRatioOperation().getImageRatioList();
            setQuesType();
        }
    }
    
//    private void setAttemptedValue() {
//        ArrayList<QuestionBean> updateQuesList = null;
//        for(QuestionBean questionBean : selectedQuestionsList) {
//            if(questionBean.getAttempt() == 0) {
//                if(updateQuesList == null)
//                    updateQuesList = new ArrayList<QuestionBean>();
//                questionBean.setAttempt(1);
//                updateQuesList.add(questionBean);
//            }
//        }
//
//        if(updateQuesList != null)
//            new QuestionPaperOperation().updateUsedValue(updateQuesList);
//    }
    
    //Getter And Setters
    public boolean isTestSaveStatus() {
        return testSaveStatus;
    }

    public void setTestSaveStatus(boolean testSaveStatus) {
        this.testSaveStatus = testSaveStatus;
    }

    public ArrayList<Integer> getSaveTestSelectedQuesIdList() {
        return saveTestSelectedQuesIdList;
    }

    public void setSaveTestSelectedQuesIdList(ArrayList<Integer> saveTestSelectedQuesIdList) {
        this.saveTestSelectedQuesIdList = saveTestSelectedQuesIdList;
    }

    public PrintedTestBean getPrintedTestBean() {
        return printedTestBean;
    }

    public void setPrintedTestBean(PrintedTestBean printedTestBean) {
        this.printedTestBean = printedTestBean;
    }
}
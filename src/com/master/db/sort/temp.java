/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.master.db.sort;

import com.bean.ImageRatioBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Aniket
 */
public class temp {
    private Connection conn1;
    ResultSet rs1 = null;
    PreparedStatement ps1 = null;
    
    public temp() {
        conn1 = new DbConnection().getConnection();
        imageRatioOperation();
    }
    
    private void imageRatioOperation() {
        ArrayList<ImageRatioBean> oldImgList = getImageRatioList();
        ArrayList<ImageRatioBean> newImgList = null;
        
        ArrayList<String> imageList = null;
        String imgName;
        for(ImageRatioBean imageRatioBean : oldImgList) {
            imgName = imageRatioBean.getImageName().trim();
            if(imgName.contains("JEE") || imgName.contains("NEET") ||
               imgName.contains("jee") || imgName.contains("neet") ||
               imgName.contains("Jee") || imgName.contains("Neet")) {
                imageRatioBean.setImageName("masterI"+imgName.substring(1, imgName.length()));
                if(newImgList == null)
                    newImgList = new ArrayList<ImageRatioBean>();
                newImgList.add(imageRatioBean);
            }
//            if(imgName.startsWith("images")) {
//                if(imageList == null)
//                    imageList = new ArrayList<String>();
//               imageList.add("masterI"+imgName.substring(1, imgName.length())+"##"+imgName);
//            }
        }
        
//        if(imageList != null) {
//            if(updateModifyQuesImageRatio(imageList))
//                JOptionPane.showMessageDialog(null, "Image Ratio Insert Successfully");
//        }
        System.out.println("Size:"+newImgList.size()); 
        if(newImgList != null) {
            if(updatePaperModifyQuesImageRatio(newImgList))
                JOptionPane.showMessageDialog(null, "Image Ratio Insert Successfully");
        }
    }
    
    public ArrayList<ImageRatioBean> getImageRatioList() {
        ArrayList<ImageRatioBean> ratioList = null;
        try {
            conn1 = new DbConnection().getConnection();
            String query = "SELECT * FROM IMAGERATIO";
            ps1 = conn1.prepareStatement(query);
            rs1 = ps1.executeQuery();
            
            ImageRatioBean imageRatioBean = null;
            
            while(rs1.next()) {
                imageRatioBean = new ImageRatioBean();
                imageRatioBean.setImageName(rs1.getString(1));
                imageRatioBean.setViewDimention(rs1.getDouble(2));
                if(ratioList == null)
                    ratioList = new ArrayList<ImageRatioBean>();
                ratioList.add(imageRatioBean);
            }
        } catch(Exception ex) {
            ratioList = null;
            ex.getMessage();
        } finally {
        }
        
        return ratioList;
    }
    
    public boolean updateModifyQuesImageRatio(ArrayList<String> imageRatioList) {
        boolean returnValue = false;
        try {
            conn1 = new DbConnection().getConnection();
            String query = "";
            for(String str : imageRatioList) {
                String[] strArray = str.split("##");
                    query = "UPDATE MASTER_IMAGERATIO SET IMAGENAME = ? WHERE IMAGENAME = ?";
                    ps1 = conn1.prepareStatement(query);
                    ps1.setString(1, strArray[0].trim());
                   ps1.setString(2, strArray[1].trim());
                    ps1.executeUpdate();
            }
            returnValue = true;
        } catch (SQLException ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
        }
        return returnValue;
    }
    
    public boolean updatePaperModifyQuesImageRatio(ArrayList<ImageRatioBean> imageRatioList) {
        boolean returnValue = false;
        try {
            conn1 = new DbConnection().getConnection();
            String query = "";
            query = "INSERT INTO MASTER_IMAGERATIO VALUES(?,?)";
            for(ImageRatioBean imageRatioBean : imageRatioList) {
                    ps1 = conn1.prepareStatement(query);
                    ps1.setString(1, imageRatioBean.getImageName().trim());
                    ps1.setDouble(2, imageRatioBean.getViewDimention());
                    ps1.executeUpdate();
            }
            returnValue = true;
        } catch (SQLException ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
        }
        return returnValue;
    }
    
    public static void main(String[] args) {
        new temp();
    }
    
}

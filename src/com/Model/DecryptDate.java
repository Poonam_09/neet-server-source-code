/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

/**
 *
 * @author Aniket
 */
public class DecryptDate {
    
    public String getDate(String date, String mobileNumber) {
        date = date.toLowerCase();
        String dd = "" + date.charAt(5) + date.charAt(1);
        String mm = "" + date.charAt(0) + date.charAt(4);
        String yy = "" + date.charAt(3) + date.charAt(2);
        int ddAlgoSequence;
        int mmAlgoSequence;
        int yyAlgoSequence;
        if (getOddEven(mobileNumber).equals("Even")) {
            ddAlgoSequence = getDigitTotal(""
                    + mobileNumber.charAt(8) + mobileNumber.charAt(4)
                    + mobileNumber.charAt(7) + mobileNumber.charAt(0));
            mmAlgoSequence = getDigitTotal(""
                    + mobileNumber.charAt(1) + mobileNumber.charAt(8)
                    + mobileNumber.charAt(2) + mobileNumber.charAt(6));
            yyAlgoSequence = getDigitTotal(""
                    + mobileNumber.charAt(9) + mobileNumber.charAt(5)
                    + mobileNumber.charAt(4) + mobileNumber.charAt(3));
        } else {
            ddAlgoSequence = getDigitTotal(""
                    + mobileNumber.charAt(6) + mobileNumber.charAt(3)
                    + mobileNumber.charAt(9) + mobileNumber.charAt(1));
            mmAlgoSequence = getDigitTotal(""
                    + mobileNumber.charAt(0) + mobileNumber.charAt(4)
                    + mobileNumber.charAt(7) + mobileNumber.charAt(5));
            yyAlgoSequence = getDigitTotal(""
                    + mobileNumber.charAt(2) + mobileNumber.charAt(8)
                    + mobileNumber.charAt(5) + mobileNumber.charAt(6));
        }
        String refineDate = getAlgorithmValue(dd, ddAlgoSequence) + "-" + getAlgorithmValue(mm, mmAlgoSequence) + "-" + getAlgorithmValue(yy, yyAlgoSequence);
//        System.out.println("Date:" + refineDate);
        return refineDate;
    }
    
    private String getOddEven(String str) {
        char[] chArray = str.toCharArray();
        int sum = 0;
        for (char ch : chArray) {
            sum += Integer.parseInt("" + ch);
        }
        if (sum % 2 == 0) {
            return "Even";
        } else {
            return "Odd";
        }
    }
    
    private int getDigitTotal(String str) {
        char[] chArray = str.toCharArray();
        int sum = 0;
        for (char ch : chArray) {
            sum += Integer.parseInt("" + ch);
        }
        while (true) {
            if (sum < 10) {
                break;
            } else {
                sum = getDigitCal(sum);
            }
        }
        return sum;
    }
    
    private int getDigitCal(int no) {
        int sum = 0;
        while (no != 0) {
            sum += no % 10;
            no /= 10;
        }
        return sum;
    }
    
    //Calling Algorithms
    private String getAlgorithmValue(String str, int algoNumber) {
        String returnString = "";
        if (algoNumber == 0) {
            returnString = getPosAlgoZero(str);
        } else if (algoNumber == 1) {
            returnString = getPosAlgoOne(str);
        } else if (algoNumber == 2) {
            returnString = getPosAlgoTwo(str);
        } else if (algoNumber == 3) {
            returnString = getPosAlgoThree(str);
        } else if (algoNumber == 4) {
            returnString = getPosAlgoFour(str);
        } else if (algoNumber == 5) {
            returnString = getPosAlgoFive(str);
        } else if (algoNumber == 6) {
            returnString = getPosAlgoSix(str);
        } else if (algoNumber == 7) {
            returnString = getPosAlgoSeven(str);
        } else if (algoNumber == 8) {
            returnString = getPosAlgoEight(str);
        } else if (algoNumber == 9) {
            returnString = getPosAlgoNine(str);
        }
        return returnString;
    }
    
    private int getPosition(char ch, char[] chArray) {
        int returnPos = 10;
        for (int i = 0; i < chArray.length; i++) {
            if (ch == chArray[i]) {
                returnPos = i;
                break;
            }
        }
        return returnPos;
    }
    
    
    //Algoritms getPositions
    private String getPosAlgoZero(String str) {
        String returnString = "";
        char[] chArrayOne = {'4', 'z', 'a', 'n', '3', '7', 't', 'v', 'x', '9'};
        char[] chArrayTwo = {'5', 'j', 'o', 's', 'r', 'f', 'g', 'm', '8', 'p'};
//        char[] chArrayOne = {'l', 'b', '7', 'd', 'u', 'f', 'x', 'h', '5', 'k'};
//        char[] chArrayTwo = {'s', 'a', '9', 'z', '2', 'g', '6', 'r', 'i', 'n'};
        int returnVal;
        returnVal = getPosition(str.charAt(0), chArrayOne);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }
        returnVal = getPosition(str.charAt(1), chArrayTwo);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }

        return returnString;
    }

    private String getPosAlgoOne(String str) {
        String returnString = "";
        char[] chArrayOne = {'t', 'a', 'w', '6', 'q', 'o', 'p', 'm', 'i', '4'};
        char[] chArrayTwo = {'y', 'd', '3', '5', 'e', 'n', '8', 'h', 'f', 'r'};
        int returnVal;
        returnVal = getPosition(str.charAt(0), chArrayOne);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }
        returnVal = getPosition(str.charAt(1), chArrayTwo);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }

        return returnString;
    }

    private String getPosAlgoTwo(String str) {
        String returnString = "";
        char[] chArrayOne = {'l', 'b', '7', 'd', 'u', 'f', 'x', 'h', '5', 'k'};
        char[] chArrayTwo = {'s', 'a', '9', 'z', '2', 'g', '6', 'r', 'i', 'n'};
//        char[] chArrayOne = {'v', 'h', '1', 'd', 'j', 'n', 'w', '7', 'l', 'k'};
//        char[] chArrayTwo = {'4', 'u', 'c', '8', 'g', '9', 'z', 't', 'i', 'p'};
        int returnVal;
        returnVal = getPosition(str.charAt(0), chArrayOne);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }
        returnVal = getPosition(str.charAt(1), chArrayTwo);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }

        return returnString;
    }

    private String getPosAlgoThree(String str) {
        String returnString = "";
        char[] chArrayOne = {'g', 'p', 'f', 'z', '3', 'b', 'q', 'd', 'm', 'l'};
        char[] chArrayTwo = {'5', 'a', '2', 'n', 'y', '9', 'h', 'r', 't', 'o'};
//        char[] chArrayOne = {'1', 'r', 'm', 'b', 'h', 'c', '3', 'k', 'o', '2'};
//        char[] chArrayTwo = {'9', 'n', 'x', 'w', '6', 'd', 'j', 'v', '4', 'a'};
        int returnVal;
        returnVal = getPosition(str.charAt(0), chArrayOne);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }
        returnVal = getPosition(str.charAt(1), chArrayTwo);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }

        return returnString;
    }

    private String getPosAlgoFour(String str) {
        String returnString = "";
        char[] chArrayOne = {'1', 'r', 'm', 'b', 'h', 'c', '3', 'k', 'o', '2'};
        char[] chArrayTwo = {'9', 'n', 'x', 'w', '6', 'd', 'j', 'v', '4', 'a'};
//        char[] chArrayOne = {'4', 'z', 'a', 'n', '3', '7', 't', 'v', 'x', '9'};
//        char[] chArrayTwo = {'5', 'j', 'o', 's', 'r', 'f', 'g', 'm', '8', 'p'};
        int returnVal;
        returnVal = getPosition(str.charAt(0), chArrayOne);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }
        returnVal = getPosition(str.charAt(1), chArrayTwo);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }

        return returnString;
    }

    private String getPosAlgoFive(String str) {
        String returnString = "";
        char[] chArrayOne = {'c', 'u', '9', '6', 's', '4', 'r', 'o', 'k', 'a'};
        char[] chArrayTwo = {'y', '1', 'q', '5', 'p', '7', 'b', '3', 'd', '8'};
//        char[] chArrayOne = {'m', '8', 'l', '7', 'k', '6', 'c', 'h', '5', 'a'};
//        char[] chArrayTwo = {'s', 'q', '9', 'n', '2', 'x', '3', 'y', '4', 'z'};
        int returnVal;
        returnVal = getPosition(str.charAt(0), chArrayOne);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }
        returnVal = getPosition(str.charAt(1), chArrayTwo);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }

        return returnString;
    }

    private String getPosAlgoSix(String str) {
        String returnString = "";
        char[] chArrayOne = {'h', '3', 'e', 'c', '5', '1', 't', 'g', '6', 'm'};
        char[] chArrayTwo = {'x', '4', '2', 'b', '9', 'p', 'y', 'j', 'n', '7'};
//        char[] chArrayOne = {'g', 'p', 'f', 'z', '3', 'b', 'q', 'd', 'm', 'l'};
//        char[] chArrayTwo = {'5', 'a', '2', 'n', 'y', '9', 'h', 'r', 't', 'o'};
        int returnVal;
        returnVal = getPosition(str.charAt(0), chArrayOne);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }
        returnVal = getPosition(str.charAt(1), chArrayTwo);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }

        return returnString;
    }

    private String getPosAlgoSeven(String str) {
        String returnString = "";
        char[] chArrayOne = {'m', '8', 'l', '7', 'k', '6', 'c', 'h', '5', 'a'};
        char[] chArrayTwo = {'s', 'q', '9', 'n', '2', 'x', '3', 'y', '4', 'z'};
//        char[] chArrayOne = {'z', '1', 'x', 'n', 'a', '5', 'd', 'v', 's', 'l'};
//        char[] chArrayTwo = {'3', 'u', 'p', 'g', '8', 't', 'j', 'w', 'm', '2'};
        int returnVal;
        returnVal = getPosition(str.charAt(0), chArrayOne);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }
        returnVal = getPosition(str.charAt(1), chArrayTwo);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }

        return returnString;
    }

    private String getPosAlgoEight(String str) {
        String returnString = "";
        char[] chArrayOne = {'z', '1', 'x', 'n', 'a', '5', 'd', 'v', 's', 'l'};
        char[] chArrayTwo = {'3', 'u', 'p', 'g', '8', 't', 'j', 'w', 'm', '2'};
//        char[] chArrayOne = {'h', '3', 'e', 'c', '5', '1', 't', 'g', '6', 'm'};
//        char[] chArrayTwo = {'x', '4', '2', 'b', '9', 'p', 'y', 'j', 'n', '7'};
        int returnVal;
        returnVal = getPosition(str.charAt(0), chArrayOne);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }
        returnVal = getPosition(str.charAt(1), chArrayTwo);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }

        return returnString;
    }

    private String getPosAlgoNine(String str) {
        String returnString = "";
        char[] chArrayOne = {'v', 'h', '1', 'd', 'j', 'n', 'w', '7', 'l', 'k'};
        char[] chArrayTwo = {'4', 'u', 'c', '8', 'g', '9', 'z', 't', 'i', 'p'};
//        char[] chArrayOne = {'c', 'u', '9', '6', 's', '4', 'r', 'o', 'k', 'a'};
//        char[] chArrayTwo = {'y', '1', 'q', '5', 'p', '7', 'b', '3', 'd', '8'};
        int returnVal;
        returnVal = getPosition(str.charAt(0), chArrayOne);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }
        returnVal = getPosition(str.charAt(1), chArrayTwo);
        if (returnVal != 10) {
            returnString += returnVal;
        } else {
            returnString += "*";
        }

        return returnString;
    }
    
    public static void main(String[] args) {
        new DecryptDate().getDate("CWN35Z", "9665746718");
    }
}

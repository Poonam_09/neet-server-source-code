/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.LatexProcessing.pdf.QuestionsProcessingChapterWise;
import com.LatexProcessing.pdf.QuestionsProcessingGroupWiseYearWise;
import com.LatexProcessing.pdf.QuestionsProcessingSubjectWise;
import com.LatexProcessing.pdf.QuestionsProcessingUnitWise;
import com.Pdf.Generation.PdfFinalPanel;
import com.bean.ChapterBean;
import com.bean.HeaderFooterTextBean;
import com.bean.PdfMainStringBean;
import com.bean.PdfPageSetupBean;
import com.bean.PdfSecondFormatBean;
import com.bean.PrintPdfFormatSecondBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class PrintFormatSecondPdf {
    private PdfPageSetupBean pdfPageSetupBean;
    
    public void setPrintPdf(PdfPageSetupBean pdfPageSetupBean,PdfSecondFormatBean pdfSecondFormatBean,ArrayList<SubjectBean> selectedSubjectList,ArrayList<ChapterBean> chapterList,ArrayList<QuestionBean> selectedQuestionList,String printingPaperType,int totalMarks,boolean correctAnswerSheet,boolean solutionSheet,boolean solutionSheetTwoColumn,boolean teachersCopy,String instituteName,PdfFinalPanel pdfFinalPanel,String fileLocation) {
        PdfMainStringBean pdfMainStringBean = null;
        this.pdfPageSetupBean = pdfPageSetupBean;
        PrintPdfFormatSecondBean printPdfFormatSecondBean = new PrintPdfFormatSecondBean();
        printPdfFormatSecondBean.setPageType(getPageType());
        printPdfFormatSecondBean.setWaterMark(getWaterMarkString(instituteName));
        printPdfFormatSecondBean.setPageBorder(getPageBorder());
        printPdfFormatSecondBean.setHeaderLeft(getHeaderFooterTextValue(pdfPageSetupBean.getHeaderLeftBean(), pdfSecondFormatBean, instituteName, selectedSubjectList, printingPaperType));
        printPdfFormatSecondBean.setHeaderCenter(getHeaderFooterTextValue(pdfPageSetupBean.getHeaderCenterBean(), pdfSecondFormatBean, instituteName, selectedSubjectList, printingPaperType));
        printPdfFormatSecondBean.setHeaderRight(getHeaderFooterTextValue(pdfPageSetupBean.getHeaderRightBean(), pdfSecondFormatBean, instituteName, selectedSubjectList, printingPaperType));
        printPdfFormatSecondBean.setFooterLeft(getHeaderFooterTextValue(pdfPageSetupBean.getFooterLeftBean(), pdfSecondFormatBean, instituteName, selectedSubjectList, printingPaperType));
        printPdfFormatSecondBean.setFooterCenter(getHeaderFooterTextValue(pdfPageSetupBean.getFooterCenterBean(), pdfSecondFormatBean, instituteName, selectedSubjectList, printingPaperType));
        printPdfFormatSecondBean.setFooterRight(getHeaderFooterTextValue(pdfPageSetupBean.getFooterRightBean(), pdfSecondFormatBean, instituteName, selectedSubjectList, printingPaperType));
        printPdfFormatSecondBean.setHeaderFooterOnFirstPage(getHeaderFooterOnFirstPage());
        printPdfFormatSecondBean.setInstituteName(getInstituteName(instituteName));
        printPdfFormatSecondBean.setHeaderText(pdfSecondFormatBean.getSubjectName());
        printPdfFormatSecondBean.setTimeDuration(pdfSecondFormatBean.getSelectedTime());
        printPdfFormatSecondBean.setPaperType(pdfSecondFormatBean.getDivisionName());
        printPdfFormatSecondBean.setTotalMarks(getMarks(totalMarks));
//        printPdfFormatSecondBean.setMainString(new QuestionsProcessingGroupWiseYearWise().getMainString(selectedQuestionList, selectedSubjectList, totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn));
       /* if(printingPaperType.equalsIgnoreCase("GroupWise") || printingPaperType.equalsIgnoreCase("YearWise")) {
            pdfMainStringBean = new QuestionsProcessingGroupWiseYearWise().getMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatSecondBean.setMainString(pdfMainStringBean.getMainString());
        } else if(printingPaperType.equalsIgnoreCase("ChapterWise")) {
            pdfMainStringBean = new QuestionsProcessingChapterWise().getMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatSecondBean.setMainString(pdfMainStringBean.getMainString());
        } else if(printingPaperType.equalsIgnoreCase("UnitWise")) {
            pdfMainStringBean = new QuestionsProcessingUnitWise().getMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatSecondBean.setMainString(pdfMainStringBean.getMainString());
        } else if(printingPaperType.equalsIgnoreCase("SubjectWise")) {
            pdfMainStringBean = new QuestionsProcessingSubjectWise().getMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatSecondBean.setMainString(pdfMainStringBean.getMainString());
        } */
       
       if(printingPaperType.equalsIgnoreCase("GroupWise") || printingPaperType.equalsIgnoreCase("YearWise")) {
            if(pdfPageSetupBean.isOptimizeLineSpace())
            {
                pdfMainStringBean = new QuestionsProcessingGroupWiseYearWise().getOptimizeMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
                printPdfFormatSecondBean.setMainString(pdfMainStringBean.getMainString());
            }
            else
            {
            pdfMainStringBean = new QuestionsProcessingGroupWiseYearWise().getMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatSecondBean.setMainString(pdfMainStringBean.getMainString());
            }
        } else if(printingPaperType.equalsIgnoreCase("ChapterWise")) {
            if(pdfPageSetupBean.isOptimizeLineSpace())
            {
                pdfMainStringBean = new QuestionsProcessingChapterWise().getOptimizeMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
                printPdfFormatSecondBean.setMainString(pdfMainStringBean.getMainString());
            }
            else
            {
            pdfMainStringBean = new QuestionsProcessingChapterWise().getMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatSecondBean.setMainString(pdfMainStringBean.getMainString());
            }
        } else if(printingPaperType.equalsIgnoreCase("UnitWise")) {
            if(pdfPageSetupBean.isOptimizeLineSpace())
            {
                pdfMainStringBean = new QuestionsProcessingUnitWise().getOptimizeMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
                printPdfFormatSecondBean.setMainString(pdfMainStringBean.getMainString());
            }
            else
            {
            pdfMainStringBean = new QuestionsProcessingUnitWise().getMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatSecondBean.setMainString(pdfMainStringBean.getMainString());
            }
        } else if(printingPaperType.equalsIgnoreCase("SubjectWise")) {
            if(pdfPageSetupBean.isOptimizeLineSpace())
            {
            pdfMainStringBean = new QuestionsProcessingSubjectWise().getOptimizeMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatSecondBean.setMainString(pdfMainStringBean.getMainString());
            }
            else
            {
            pdfMainStringBean = new QuestionsProcessingSubjectWise().getMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatSecondBean.setMainString(pdfMainStringBean.getMainString());
            }
        }
        new ExportProcess().createFormatSecondPdf(printPdfFormatSecondBean, fileLocation, pdfFinalPanel);
        if(teachersCopy)
            new PrintTeacherCopy().setPrintPdf(pdfMainStringBean.getTeacherMainString(), instituteName, pdfSecondFormatBean.getSelectedTime(), totalMarks, getTeacherHeaderText(printingPaperType,selectedSubjectList,chapterList), fileLocation);
    }
    
    //Proccessing Methods
    private String getPageType() {
        return "\\documentclass[14pt," + pdfPageSetupBean.getPaperType().trim() + "]{article}";
    }
    
    private String getPageBorder() {
        String returnValue = "";
        if(pdfPageSetupBean.isPageBorder()) {
            returnValue += "\\usepackage{pgf}\n" +
                            "\\usepackage{pgfpages}\n" +
                            "\n" +
                            "\\pgfpagesdeclarelayout{boxed}\n" +
                            "{\n" +
                            "	\\edef\\pgfpageoptionborder{0pt}\n" +
                            "}\n" +
                            "{\n" +
                            "	\\pgfpagesphysicalpageoptions\n" +
                            "	{%\n" +
                            "		logical pages=1,%\n" +
                            "	}\n" +
                            "	\\pgfpageslogicalpageoptions{1}\n" +
                            "	{\n" +
                            "		border code=\\pgfsetlinewidth{"+pdfPageSetupBean.getBorderWidth()+"pt}\\pgfstroke,%\n" +
                            "		border shrink=\\pgfpageoptionborder,%\n" +
                            "		resized width=.90\\pgfphysicalwidth,%\n" +
                            "		resized height=.95\\pgfphysicalheight,%\n" +
                            "		center=\\pgfpoint{.5\\pgfphysicalwidth}{.5\\pgfphysicalheight}%\n" +
                            "	}%\n" +
                            "}\n" +
                            "\n" +
                            "\\pgfpagesuselayout{boxed}\n" +
                            "";
        } else {
            returnValue += " ";
        }
        return returnValue;
    }
    
    private String getHeaderFooterOnFirstPage() {
        String returnValue = "";
        if(pdfPageSetupBean.isHeaderFooterOnFirstPage()) {
            returnValue += " ";
        } else {
            returnValue += "\\thispagestyle{empty}";
        }
        return returnValue;
    }
    
    private String getWaterMarkString(String instituteName) {
        String returnValue = "";
        int waterMarkPosition = pdfPageSetupBean.getWaterMarkInfo();
        int waterMarkScale = pdfPageSetupBean.getWaterMarkScale();
        int waterMarkAngle = pdfPageSetupBean.getWaterMarkAngle();
        int waterMarkTextGrayScale = pdfPageSetupBean.getWaterMarkTextGrayScale();
        String waterMarkLogoPath = pdfPageSetupBean.getWaterMarkLogoPath();
//        waterMarkScale = waterMarkScale + 1;
        waterMarkTextGrayScale = waterMarkTextGrayScale + 1;
        String waterMarkTextGrayScales = "0." + waterMarkTextGrayScale;
        
        if (waterMarkPosition == 1 || waterMarkPosition == 2) {
            waterMarkLogoPath = waterMarkLogoPath.replace("\\", "/");
            if (waterMarkPosition == 1) {
                returnValue = "\\SetWatermarkText{"+ instituteName +"}\\SetWatermarkScale{"+ (waterMarkScale+1) +"}\\SetWatermarkAngle{"+ waterMarkAngle +"}\\SetWatermarkFontSize{0.5cm}\\SetWatermarkColor[gray]{"+ waterMarkTextGrayScales +"}";
            } else if (waterMarkPosition == 2) {
                returnValue = "\\SetWatermarkText{\\includegraphics{"+ waterMarkLogoPath +"}}\\SetWatermarkScale{"+ waterMarkScale +"}\\SetWatermarkAngle{"+ waterMarkAngle +"}\\SetWatermarkFontSize{0.5cm}";
            }
        } else {
            returnValue = "\\SetWatermarkText{ }\\SetWatermarkScale{"+ waterMarkScale +"}\\SetWatermarkAngle{"+ waterMarkAngle +"}\\SetWatermarkFontSize{1cm}\\SetWatermarkColor[rgb]{160,160,160}";
        }
        return returnValue;
    }
    
    private String getHeaderFooterTextValue(HeaderFooterTextBean headerFooterTextBean,PdfSecondFormatBean pdfSecondFormatBean,String instituteName,ArrayList<SubjectBean> selectedSubjectList,String printingPaperType) {
        String returnValue = "";
        if(!headerFooterTextBean.isCheckBoxSelected()) {
            if(headerFooterTextBean.getTextValue().trim().equalsIgnoreCase("None")) {
                returnValue = " ";
            } else if(headerFooterTextBean.getTextValue().trim().equalsIgnoreCase("Subject/Group")) {
                if(printingPaperType.equalsIgnoreCase("GroupWise") || printingPaperType.equalsIgnoreCase("YearWise")) {
                    for(SubjectBean sb : selectedSubjectList) {
                        returnValue += sb.getSubjectName().substring(0, 1);
                    }
                } else {
                    returnValue = selectedSubjectList.get(0).getSubjectName();
                }
            } else if(headerFooterTextBean.getTextValue().trim().equalsIgnoreCase("Division")) {
                returnValue = pdfSecondFormatBean.getDivisionName();
            } else if(headerFooterTextBean.getTextValue().trim().equalsIgnoreCase("Date")) {
                returnValue = getSystemDate();
            } else if(headerFooterTextBean.getTextValue().trim().equalsIgnoreCase("Institute Name")) {
                returnValue = instituteName;
            } else if(headerFooterTextBean.getTextValue().trim().equalsIgnoreCase("Page Number")) {
                returnValue = "\\thepage";
            }
        } else {
            returnValue = headerFooterTextBean.getTextValue().trim();
        }
        return returnValue;
    }
    
    private String getMarks(int totalMarks) {
        return "Marks : "+totalMarks;
    }
    
//    private String getInstituteDepartmentHeading(PdfSecondFormatBean secondPageSetupBean,String instituteName) {
//        int logoPosition = pdfPageSetupBean.getLogoPosition(); 
//        String returnValue = "";
//        if (pdfPageSetupBean.isSetLogo()) {
//            String logoPath = pdfPageSetupBean.getLogoPath();
//            String logoScaleCode = "[width=2.1cm,height=2.1cm]";
//            logoPath = logoPath.replace("\\", "/");
//            if(pdfPageSetupBean.isSetImageActualsize()) {
//                logoScaleCode=" ";
//                if(logoPosition == 3) {
//                    returnValue = "\\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
////                  "{\\huge{\\textbf{" + instituteName + "}}}\\tabularnewline		\n" +
//                        "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}} "  +
//                        "\\end{tabular}\\end{center}";
//                } else {
//                  returnValue = "\\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
//                  "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}}  " +
////                           "\\tabularnewline		\n" +
////                " \\vspace*{0.01in} {\\huge{\\textbf{" + instituteName + "}}} 	"+
//                        "\\end{tabular}\\end{center}";
//                }
//            } else {
//                if(secondPageSetupBean.isShowDepartmentName()) {
//                    if(logoPosition == 0) {
//                        returnValue = "\\begin{center}\\begin{tabular}{>{\\raggedright}p{1.9cm}  >{\\centering}p{15.4cm} }\\multirow{2}{*}[\\normalbaselineskip]{\\includegraphics"+logoScaleCode+"{"+logoPath+"}}&{\\huge{\\textbf{" + instituteName + "}}} \\tabularnewline				&                  \\fbox{\\LARGE\\textbf{" + departmentName + "}}\\end{tabular}\\end{center}";
//                    } else if(logoPosition == 1) {
//                        returnValue = "\\begin{center}\\begin{tabular}{ >{\\centering}p{15.4cm} >{\\raggedleft}p{1.9cm} }{\\huge{\\textbf{" + instituteName + "}}} & \\multirow{2}{*}[\\normalbaselineskip]{\\includegraphics"+logoScaleCode+"{"+logoPath+"}} \\tabularnewline		\\fbox{\\LARGE\\textbf{" + departmentName + "}}	&      \\end{tabular}\\end{center}";
//                    } else if(logoPosition == 2) {
//                        returnValue = "\\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
//                            "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}}  \\tabularnewline		\n"
//                            + " \\vspace*{0.01in} {\\huge{\\textbf{" + instituteName + "}}} \\tabularnewline	\\fbox{\\LARGE\\textbf{" + 
//                            departmentName + "}}\\end{tabular}\\end{center}";
//                    } else if(logoPosition == 3) {
//                        returnValue = "\\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
//                            "{\\huge{\\textbf{" + instituteName + "}}}\\tabularnewline		\n"
//                            + "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}} \\tabularnewline	\\fbox{\\LARGE\\textbf{" +
//                            departmentName + "}}\\end{tabular}\\end{center}";
//                    }
//                } else {
//                    if(logoPosition == 0) {
//                        returnValue = "\\begin{center}\\begin{tabular}{>{\\raggedright}p{1.9cm}  >{\\centering}p{15.4cm} }\\multirow{2}{*}[\\normalbaselineskip]{\\includegraphics"+logoScaleCode+"{"+logoPath+"}}&{\\huge{\\textbf{" + instituteName + "}}} \\tabularnewline				&                  \\end{tabular}\\end{center}";
//                    } else if(logoPosition == 1) {
//                        returnValue = "\\begin{center}\\begin{tabular}{ >{\\centering}p{15.4cm} >{\\raggedleft}p{1.9cm} }{\\huge{\\textbf{" + instituteName + "}}} & \\multirow{2}{*}[\\normalbaselineskip]{\\includegraphics"+logoScaleCode+"{"+logoPath+"}} \\tabularnewline		&      \\end{tabular}\\end{center}";
//                    } else if(logoPosition == 2) {
//                        returnValue = "\\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
//                            "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}}  \\tabularnewline		\n"
//                            + " \\vspace*{0.01in} {\\huge{\\textbf{" + instituteName + "}}} \\end{tabular}\\end{center}";
//                    } else if(logoPosition == 3) {
//                        returnValue = "\\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
//                            "{\\huge{\\textbf{" + instituteName + "}}}\\tabularnewline		\n"
//                            + "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}} \\end{tabular}\\end{center}";
//                    }
//                }
//            }
//        } else {
//            if(secondPageSetupBean.isShowDepartmentName()) 
//                returnValue = "\\begin{center}{\\Huge{\\textbf{" + instituteName + "}}}\\\\\\vspace*{0.08in}\\fbox{\\LARGE\\textbf{" + departmentName + "}}\\end{center}";
//            else
//                returnValue = "\\begin{center}{\\Huge{\\textbf{" + instituteName + "}}}\\end{center}";
//        }
//        return returnValue;
//    }
    
    
//    private String getInstituteName(String instituteName) {
//        return "\\begin{center}{\\Huge{\\textbf{" + instituteName + "}}}\\end{center}";
//        
//    }
    
    private String getInstituteName(String instituteName) {
        int logoPosition = pdfPageSetupBean.getLogoPosition(); 
        String returnValue = "";
        if (pdfPageSetupBean.isSetLogo()) {
            String logoPath = pdfPageSetupBean.getLogoPath();
            String logoScaleCode = "[width=2.1cm,height=2.1cm]";
            logoPath = logoPath.replace("\\", "/");
            if(pdfPageSetupBean.isSetImageActualsize()) {
                logoScaleCode=" ";
                if(logoPosition == 3) {
                    returnValue = "\\vspace*{-0.4in} \\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
                                "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}} "  +
                                "\\end{tabular}\\end{center}";
                } else {
                    returnValue = "\\vspace*{-0.4in} \\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
                                "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}}  " +
                                "\\end{tabular}\\end{center}";
                }
            } else {
                if(logoPosition == 3) {
                    returnValue = "\\vspace*{-0.4in} \\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
                                "{\\huge{\\textbf{" + instituteName + "}}}\\tabularnewline		\n"
                                + "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}} \\end{tabular}\\end{center}";
                } else if(logoPosition == 2) {
                    returnValue = "\\vspace*{-0.4in} \\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
                                "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}}  \\tabularnewline		\n"
                                + " \\vspace*{0.01in} {\\huge{\\textbf{" + instituteName + "}}} \\end{tabular}\\end{center}";
                } else if(logoPosition == 1) {
                    returnValue = "\\vspace*{-0.4in} \\begin{table}[ht] 	\\centering	\\renewcommand{\\arraystretch}{2.5} \\begin{tabular}{>{\\centering}m{15.4cm}  >{\\raggedleft\\arraybackslash}m{1.9cm} }{\\huge{\\textbf{" + instituteName + "}}} & {\\includegraphics"+logoScaleCode+"{"+logoPath+"}}  \\end{tabular} \\end{table}";
                } else if(logoPosition==0) {
                    returnValue = "\\vspace*{-0.4in} \\begin{table}[ht] 	\\centering  \\renewcommand{\\arraystretch}{2.5} 	\\begin{tabular}{>{\\raggedright}m{1.9cm}  >{\\centering\\arraybackslash}m{15.4cm} }{\\includegraphics"+logoScaleCode+"{"+logoPath+"}}&{\\huge{\\textbf{" + instituteName + "}}}  \\end{tabular} \\end{table}";
                }
            }
        } else {
            returnValue = "\\begin{center}{\\Huge{\\textbf{" + instituteName + "}}}\\end{center}";
        }
        
        return returnValue;
    }
    
    private String getSystemDate(){
        String returnValue = "";
        DateFormat dateFormat = new SimpleDateFormat("E dd-MMM-yyyy");
        returnValue = dateFormat.format(System.currentTimeMillis());
        return returnValue;
    }
    
    private String getTeacherHeaderText(String printingPaperType,ArrayList<SubjectBean> selectedSubjectList,ArrayList<ChapterBean> chapterList) {
        String returnString = "";
        if(printingPaperType.equalsIgnoreCase("ChapterWise")) {
            returnString += selectedSubjectList.get(0).getSubjectName().trim()+" : "+chapterList.get(0).getChapterName().trim();
        } else if(printingPaperType.equalsIgnoreCase("UnitWise")) {
            returnString += selectedSubjectList.get(0).getSubjectName().trim()+" Units Test";
        } else if(printingPaperType.equalsIgnoreCase("SubjectWise")) {
            returnString += selectedSubjectList.get(0).getSubjectName().trim()+" Subject Test";
        } else if(printingPaperType.equalsIgnoreCase("GroupWise")) {
            returnString += new ProcessManager().getGroupName().trim()+" Group Test";
        } else if(printingPaperType.equalsIgnoreCase("YearWise")) {
            returnString += new ProcessManager().getGroupName().trim()+" Old Paper Test";
        }
        return returnString;
    }
}

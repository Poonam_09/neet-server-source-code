/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.bean.RegistrationBean;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Aniket
 */
public class ValidateDate {
    public boolean getValidateDate(RegistrationBean infoBean) {
        boolean returnValue = false;
        String userPinKey = infoBean.getPinKey().trim().toUpperCase();
        userPinKey = new SwappingKeyRetrive().swapPinKey(userPinKey, infoBean.getCustMobile());
        String userSdKey = userPinKey.substring(21, 27);
        String userEdKey = userPinKey.substring(7, 13);
        
        String mobileRev = reverseMobile(infoBean.getCustMobile());
        DecryptDate vd = new DecryptDate();
        String activationDate = vd.getDate(userSdKey, mobileRev);
        String expiryDate = vd.getDate(userEdKey, infoBean.getCustMobile());
        
        System.out.println("Act Date:"+activationDate+"\nExpiry Date:"+expiryDate);
        if(!activationDate.contains("*") && !expiryDate.contains("*")) {
            if(isThisDateValid(activationDate) || isThisDateValid(expiryDate)) {
                if(isValidateSystemDate(activationDate)) {
                    infoBean.setExpiryDate(expiryDate);
                    returnValue = true;
                } else {
                //    JOptionPane.showMessageDialog(null, "Invalid Installation Date");
                    returnValue = false;
                }
            } else {
                returnValue = false;
            }
        } else {
            returnValue = false;
        }
        
        return returnValue;
    }
    
    private String reverseMobile(String str) {
        int len = str.length();
        char[] target = new char[len];
        int revIndex = len - 1;
        for(int i=0;i<len;i++) 
            target[i] = str.charAt(revIndex--);
        String reverseString = String.valueOf(target);
        if(reverseString.equals(str)) 
            reverseString = "0123456789";
        
        return reverseString;
    }
    
    private boolean isThisDateValid(String dateToValidate) {

        if(dateToValidate == null)
            return false;

        if(dateToValidate.contains("*"))
            return false;
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
        sdf.setLenient(false);
        try {
            Date date = sdf.parse(dateToValidate);
        } catch (ParseException ex) {
            return false;
        }
        return true;
    }
    
    private boolean isValidateSystemDate(String activeDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
        try {
            Date date1 = sdf.parse(activeDate);
            Date date2 = sdf.parse(getSystemDate(0));

            if (date1.before(date2) || date1.equals(date2)) {
                return true;
            }
        } catch (ParseException ex) {
            return false;
        }
        return false;
    }
    
    private String getSystemDate(int count){
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, count);
        String returnDate = dateFormat.format(cal.getTimeInMillis());
        return returnDate;
    }
}

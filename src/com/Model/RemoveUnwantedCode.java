/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.bean.QuestionBean;
import com.db.operations.MasterQuestionOperation;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class RemoveUnwantedCode {
    public void RefineData() {
        ArrayList<QuestionBean> questionList = new MasterQuestionOperation().getQuestuionsList();
        ArrayList<QuestionBean> updateQuestionList = null;
        
        if(questionList != null) {
            String question,opA,opB,opC,opD,hint,year;
            for(QuestionBean questionsBean : questionList) {
                question = questionsBean.getQuestion().trim();
                opA = questionsBean.getOptionA().trim();
                opB = questionsBean.getOptionB().trim();
                opC = questionsBean.getOptionC().trim();
                opD = questionsBean.getOptionD().trim();
                hint = questionsBean.getHint().trim();
                year = questionsBean.getYear().trim();
                if(question.contains("\\textbf{\\hspace{15pt}}") || questionsBean.getQuestion().trim().contains("\\hspace{15pt}") ||
                    opA.contains("\\textbf{\\hspace{15pt}}") || opA.contains("\\hspace{15pt}") ||
                    opB.contains("\\textbf{\\hspace{15pt}}") || opB.contains("\\hspace{15pt}") ||
                    opC.contains("\\textbf{\\hspace{15pt}}") || opC.contains("\\hspace{15pt}") ||
                    opD.contains("\\textbf{\\hspace{15pt}}") || opD.contains("\\hspace{15pt}") ||
                    hint.contains("\\textbf{\\hspace{15pt}}") || hint.contains("\\hspace{15pt}") ||
                    isUpdateYear(year) || year.contains("\\&")) {
                    
                    question = question.replace("\\textbf{\\hspace{15pt}}","");
                    question = question.replace("\\hspace{15pt}","");
                    question = question.replace("\\mbox{ ", "\\mbox{");
                    question = question.replace("\\mbox{  ", "\\mbox{");
                    question = question.replace("\\mbox{   ", "\\mbox{");
                    
                    opA = opA.replace("\\textbf{\\hspace{15pt}}","");
                    opA = opA.replace("\\hspace{15pt}","");
                    opA = opA.replace("\\mbox{ ", "\\mbox{");
                    opA = opA.replace("\\mbox{  ", "\\mbox{");
                    opA = opA.replace("\\mbox{   ", "\\mbox{");
                            
                    opB = opB.replace("\\textbf{\\hspace{15pt}}","");
                    opB = opB.replace("\\hspace{15pt}","");
                    opB = opB.replace("\\mbox{ ", "\\mbox{");
                    opB = opB.replace("\\mbox{  ", "\\mbox{");
                    opB = opB.replace("\\mbox{   ", "\\mbox{");
                    
                    opC = opC.replace("\\textbf{\\hspace{15pt}}","");
                    opC = opC.replace("\\hspace{15pt}","");
                    opC = opC.replace("\\mbox{ ", "\\mbox{");
                    opC = opC.replace("\\mbox{  ", "\\mbox{");
                    opC = opC.replace("\\mbox{   ", "\\mbox{");
                    
                    opD = opD.replace("\\textbf{\\hspace{15pt}}","");
                    opD = opD.replace("\\hspace{15pt}","");
                    opD = opD.replace("\\mbox{ ", "\\mbox{");
                    opD = opD.replace("\\mbox{  ", "\\mbox{");
                    opD = opD.replace("\\mbox{   ", "\\mbox{");
                    
                    hint = hint.replace("\\textbf{\\hspace{15pt}}","");
                    hint = hint.replace("\\hspace{15pt}","");
                    hint = hint.replace("\\mbox{ ", "\\mbox{");
                    hint = hint.replace("\\mbox{  ", "\\mbox{");
                    hint = hint.replace("\\mbox{   ", "\\mbox{");
                    
                    
                    
                    if(!year.equalsIgnoreCase("") && !year.equalsIgnoreCase("\\mbox{}")) {
                        if(year.startsWith("{"))
                            year = "["+year.substring(1, year.length());
                        else if(year.startsWith("("))
                            year = "["+year.substring(1, year.length());
                        else if(!year.startsWith("["))
                            year = "[" + year;

                        if(year.endsWith("}"))
                            year = year.substring(0, year.length()-1) + "]";
                        else if(year.endsWith(")"))
                            year = year.substring(0, year.length()-1) + "]";
                        else if(!year.endsWith("]"))
                            year = year + "]";
                    
                        year = year.replace("\\&", "&");
                    }
                    questionsBean.setQuestion(question);
                    questionsBean.setOptionA(opA);
                    questionsBean.setOptionB(opB);
                    questionsBean.setOptionC(opC);
                    questionsBean.setOptionD(opD);
                    questionsBean.setHint(hint);
                    questionsBean.setYear(year);
                    
                    if(updateQuestionList == null)
                        updateQuestionList = new ArrayList<QuestionBean>();
                    updateQuestionList.add(questionsBean);
                }
            }
            
            if(updateQuestionList != null) {
                System.out.println("Size:"+updateQuestionList.size());
                new MasterQuestionOperation().updateRemoveUnwanted(updateQuestionList);
            }
        }
    }
    
    private boolean isUpdateYear(String year) {
        if(!year.equalsIgnoreCase("") && !year.equalsIgnoreCase("\\mbox{}")) {
            if(year.startsWith("{") || year.endsWith("}") ||
             year.startsWith("(") || year.endsWith(")") ||
             !year.startsWith("[") || !year.endsWith("]"))
                return true;
        }
        return false;
    }
    
    public static void main(String[] args) {
        new RemoveUnwantedCode().RefineData();
    }
}

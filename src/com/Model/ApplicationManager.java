/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Aniket
 */
public class ApplicationManager {
    public boolean getApplicationRunningStatus(String applicationName) {
        boolean returnValue = false;
        String line;
        String pidInfo ="";
        try {
            Process p =Runtime.getRuntime().exec(System.getenv("windir") +"\\system32\\"+"tasklist.exe");
            BufferedReader input =  new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null)
                pidInfo+=line; 
            input.close();
            if(pidInfo.contains(applicationName))
                returnValue = true;
        } catch (IOException ex) {
            returnValue = false;
            ex.printStackTrace();
        }
        return returnValue;
    }
}

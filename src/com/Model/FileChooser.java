/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.Pdf.Generation.PdfFinalPanel;
import com.Pdf.Generation.PdfPageSetup;
import com.Word.Generation.WordFinalPanel;
import com.pages.HomePage;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

/**
 *
 * @author Aniket
 */
public class FileChooser {
    
    public String getFileLocation() {
        String directoryTosave = "";
        String fileName = "";
        try {
            JFileChooser saveFile = new JFileChooser();//new save dialog  
            saveFile.resetChoosableFileFilters();
            saveFile.setFileFilter(new FileFilter()//adds new filter into list  
            {
                String description = "PDF Files(*.pdf)";//the filter you see  
                String extension = "pdf";//the filter passed to program  

                public String getDescription() {
                    return description;
                }

                public boolean accept(File f) {
                    if (f == null) {
                        return false;
                    }
                    if (f.isDirectory()) {
                        return true;
                    }
                    return f.getName().toLowerCase().endsWith(extension);
                }
            });
            saveFile.setCurrentDirectory(new File("*.pdf"));
            saveFile.showSaveDialog(saveFile);
            // strSaveFileMenuItem is the parent Component which calls this method, ex: a toolbar, button or a menu item.  
            fileName = saveFile.getSelectedFile().getName();
            directoryTosave = saveFile.getSelectedFile().getPath();
            directoryTosave = saveFile.getCurrentDirectory().getPath();
            System.out.println(fileName + "     path : " + directoryTosave);
            // the file name selected by the user is now in the string 'strFilename'.  
        } catch (Exception er) {
            return "";
        }
        return directoryTosave+","+fileName;
    }
    
    public String getPdfFileLocation(PdfFinalPanel pdfFinalPanel) {
        String returnString = null;
        String directoryTosave = "";
        String fileName = "";
        try {
            JFileChooser saveFile = new JFileChooser();//new save dialog  
            String defaultPath = new DefaultFolderLocation().getFolderLocation();
//            String path = System.getProperty("user.home") + File.separator + "Documents";
//            path += File.separator + "CT-PDF";
//            File customDir = new File(path);
//            
//            if(!customDir.exists())
//                customDir.mkdirs();
            saveFile.setCurrentDirectory(new File(defaultPath));
            
            saveFile.resetChoosableFileFilters();
            saveFile.setFileFilter(new FileFilter()//adds new filter into list 
            {
                String description = "PDF Files(*.pdf)";//the filter you see  
                String extension = "pdf";//the filter passed to program  

                public String getDescription() {
                    return description;
                }

                public boolean accept(File f) {
                    if (f == null) {
                        return false;
                    }
                    if (f.isDirectory()) {
                        return true;
                    }
                    return f.getName().toLowerCase().endsWith(extension);
                }
            });
            saveFile.setCurrentDirectory(new File("*.pdf"));
            saveFile.showSaveDialog(pdfFinalPanel);
            
            File pdfFile = new File(saveFile.getSelectedFile().toString()+ ".pdf");
            if (pdfFile.exists()) {
                int response = JOptionPane.showConfirmDialog(pdfFinalPanel, //
                            "Do you want to replace the existing file?", //
                            "Confirm", JOptionPane.YES_NO_OPTION, //
                            JOptionPane.QUESTION_MESSAGE);
                if (response != JOptionPane.YES_OPTION) {
                    return null;
                } 
                pdfFile.delete();
            }
            
            fileName = saveFile.getSelectedFile().getName();
            directoryTosave = saveFile.getSelectedFile().getPath();
            directoryTosave = saveFile.getCurrentDirectory().getPath();
            System.out.println(fileName + "     path : " + directoryTosave);
            returnString = directoryTosave+","+fileName;
            // the file name selected by the user is now in the string 'strFilename'.  
        } catch (Exception ex) {
            returnString = null;
        }
        
        return returnString;
    }
    
    public String getPngFileLocation(PdfPageSetup pdfPageSetup) {
        String returnString = null;
        try {
            JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            jfc.setDialogTitle("Select an image");
            jfc.setAcceptAllFileFilterUsed(false);
            FileNameExtensionFilter filter = new FileNameExtensionFilter("PNG and GIF and JPG images", "png", "gif","jpg","jpeg");
            jfc.addChoosableFileFilter(filter);

            int returnValue = jfc.showOpenDialog(pdfPageSetup);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                    returnString = jfc.getSelectedFile().getName();
                    returnString += "," + jfc.getSelectedFile().getPath();
            }
        } catch (Exception ex) {
            returnString = null;
        }
        return returnString;
    }
    
    public String getWordFileLocation(WordFinalPanel wordFinalPanel) {
        String returnString = null;
        String directoryTosave = "";
        String fileName = "";
        try {
            JFileChooser saveFile = new JFileChooser();//new save dialog  

//            saveFile.setCurrentDirectory(new File(System.getProperty("user.home"), "Desktop"));
            String defaultPath = new DefaultFolderLocation().getFolderLocation();
//            String path = System.getProperty("user.home") + File.separator + "Documents";
//            path += File.separator + "CT-WORD";
//            File customDir = new File(path);
//            
//            if(!customDir.exists())
//                customDir.mkdirs();
            
            saveFile.setCurrentDirectory(new File(defaultPath));
            
            saveFile.resetChoosableFileFilters();
            saveFile.setFileFilter(new FileFilter()//adds new filter into list 
            {
                String description = "Word Files(*.docx)";//the filter you see  
                String extension = "docx";//the filter passed to program  

                public String getDescription() {
                    return description;
                }

                public boolean accept(File f) {
                    if (f == null) {
                        return false;
                    }
                    if (f.isDirectory()) {
                        return true;
                    }
                    return f.getName().toLowerCase().endsWith(extension);
                }
            });
            saveFile.setCurrentDirectory(new File("*.docx"));
            saveFile.showSaveDialog(wordFinalPanel);
            
            File pdfFile = new File(saveFile.getSelectedFile().toString()+ ".docx");
            if (pdfFile.exists()) {
                int response = JOptionPane.showConfirmDialog(wordFinalPanel, //
                            "Do you want to replace the existing file?", //
                            "Confirm", JOptionPane.YES_NO_OPTION, //
                            JOptionPane.QUESTION_MESSAGE);
                if (response != JOptionPane.YES_OPTION) {
                    return null;
                } 
                pdfFile.delete();
            }
            
            fileName = saveFile.getSelectedFile().getName();
            directoryTosave = saveFile.getSelectedFile().getPath();
            directoryTosave = saveFile.getCurrentDirectory().getPath();
            System.out.println(fileName + "     path : " + directoryTosave);
            returnString = directoryTosave+","+fileName;
            // the file name selected by the user is now in the string 'strFilename'.  
        } catch (Exception ex) {
            returnString = null;
        }
        
        return returnString;
    }
    
    public String getPdfFileLocation(HomePage homePage) {
        String returnString = null;
        String directoryTosave = "";
        String fileName = "";
        try {
            JFileChooser saveFile = new JFileChooser();//new save dialog  
            
            String defaultPath = new DefaultFolderLocation().getFolderLocation();
//            String defaultPath = System.getProperty("user.home") + File.separator + "Documents";
//            defaultPath += File.separator + "CT-PDF";
//            File customDir = new File(defaultPath);
//            
//            if(!customDir.exists())
//                customDir.mkdirs();
            
            
            saveFile.setCurrentDirectory(new File(defaultPath));
            saveFile.resetChoosableFileFilters();
            saveFile.setFileFilter(new FileFilter()//adds new filter into list 
            {
                String description = "PDF Files(*.pdf)";//the filter you see  
                String extension = "pdf";//the filter passed to program  

                public String getDescription() {
                    return description;
                }

                public boolean accept(File f) {
                    if (f == null) {
                        return false;
                    }
                    if (f.isDirectory()) {
                        return true;
                    }
                    return f.getName().toLowerCase().endsWith(extension);
                }
            });
            saveFile.setCurrentDirectory(new File("*.pdf"));
            saveFile.showSaveDialog(homePage);
            
            File pdfFile = new File(saveFile.getSelectedFile().toString()+ "-OMR.pdf");
            if (pdfFile.exists()) {
                int response = JOptionPane.showConfirmDialog(homePage, //
                            "Do you want to replace the existing file?", //
                            "Confirm", JOptionPane.YES_NO_OPTION, //
                            JOptionPane.QUESTION_MESSAGE);
                if (response != JOptionPane.YES_OPTION) {
                    return null;
                } 
                pdfFile.delete();
            }
            
            fileName = saveFile.getSelectedFile().getName();
            directoryTosave = saveFile.getSelectedFile().getPath();
            directoryTosave = saveFile.getCurrentDirectory().getPath();
            System.out.println(fileName + "     path : " + directoryTosave);
            returnString = directoryTosave+","+fileName;
            // the file name selected by the user is now in the string 'strFilename'.  
        } catch (Exception ex) {
            returnString = null;
        }
        
        return returnString;
    }
    
    public String getFolderLocation(HomePage homePage,String path) {
        String returnString = null;
        try {
            JFileChooser chooser = new JFileChooser(); 
            String choosertitle = "";
            //new java.io.File(System.getProperty("user.home") + File.separator + "Documents")
            chooser.setCurrentDirectory(new File(path));
            chooser.setDialogTitle(choosertitle);
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            //
            // disable the "All files" option.
            //
            chooser.setAcceptAllFileFilterUsed(false);
            //    
            if (chooser.showOpenDialog(homePage) == JFileChooser.APPROVE_OPTION) { 
                returnString = chooser.getSelectedFile().toString();
            } else {
                returnString = null;
            }
        } catch (Exception ex) {
            returnString = null;
        }
        return returnString;
    }
    public String getExcelFileLocation(HomePage homepage) {
        String returnString = null;
        String directoryTosave = "";
        String fileName = "";
        try {
            JFileChooser saveFile = new JFileChooser();//new save dialog  
            String defaultPath = "C:\\Users\\Aniket\\Desktop";
//            String path = System.getProperty("user.home") + File.separator + "Documents";
//            path += File.separator + "CT-PDF";
//            File customDir = new File(path);
//            
//            if(!customDir.exists())
//                customDir.mkdirs();
            saveFile.setCurrentDirectory(new File(defaultPath));
            
            saveFile.resetChoosableFileFilters();
            saveFile.setFileFilter(new FileFilter()//adds new filter into list 
            {
                String description = "Excel Files(*.xls)";//the filter you see  
                String extension = "xls";//the filter passed to program  

                public String getDescription() {
                    return description;
                }

                public boolean accept(File f) {
                    if (f == null) {
                        return false;
                    }
                    if (f.isDirectory()) {
                        return true;
                    }
                    return f.getName().toLowerCase().endsWith(extension);
                }
            });
            saveFile.setCurrentDirectory(new File("*.xls"));
            saveFile.showSaveDialog(homepage);
            
            File pdfFile = new File(saveFile.getSelectedFile().toString()+ ".xls");
            if (pdfFile.exists()) {
                int response = JOptionPane.showConfirmDialog(homepage, //
                            "Do you want to replace the existing file?", //
                            "Confirm", JOptionPane.YES_NO_OPTION, //
                            JOptionPane.QUESTION_MESSAGE);
                if (response != JOptionPane.YES_OPTION) {
                    return null;
                } 
                pdfFile.delete();
            }
            
            fileName = saveFile.getSelectedFile().getName();
            directoryTosave = saveFile.getSelectedFile().getPath();
            directoryTosave = saveFile.getCurrentDirectory().getPath();
            System.out.println(fileName + "     path : " + directoryTosave);
            returnString = directoryTosave+","+fileName;
            // the file name selected by the user is now in the string 'strFilename'.  
        } catch (Exception ex) {
            returnString = null;
        }
        
        return returnString;
    }
}

package com.Word.Generation;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.Model.ProcessManager;
import com.Pdf.Generation.*;
import com.bean.ChapterBean;
import com.bean.PdfSecondFormatBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.bean.WordPageSetupBean;
import com.pages.HomePage;
import com.pages.MultipleChapterQuestionsSelection;
import com.pages.MultipleYearQuestionsSelection;
import com.pages.SingleChapterQuestionsSelection;
import java.awt.Color;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import com.Model.TitleInfo;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Sagar
 */
public class WordHeadingSetup extends javax.swing.JFrame {

    private ArrayList<QuestionBean> selectedQuestionList;
    private ArrayList<QuestionBean> questionsList;
    private ArrayList<ChapterBean> chaptersList;
    private WordPageSetupBean wordPageSetupBean;
    private String printingPaperType;
    private PdfSecondFormatBean pdfSecondFormatBean;
    private ArrayList<SubjectBean> selectedSubjectList;
    private int subFirstQuesCount;
    private int subSecondQuesCount;
    private int subThirdQuesCount;
    private int totalMarks;
    private WordPageSetup wordPageSetup;
    private String groupName;
    private Object quesPageObject;
    
    //Aniket
    public WordHeadingSetup(ArrayList<QuestionBean> selectedQuestionList,ArrayList<SubjectBean> selectedSubjectList,ArrayList<ChapterBean> chaptersList,WordPageSetupBean wordPageSetupBean,ArrayList<QuestionBean> questionsList,String printingPaperType,Object quesPageObject,WordPageSetup wordPageSetup) {
        initComponents();
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        setLocation(0, 0);
        setLocationRelativeTo(null);
        this.getContentPane().setBackground(new Color(204, 204, 204));
        this.selectedQuestionList = selectedQuestionList;
        this.chaptersList = chaptersList;
        this.wordPageSetupBean = wordPageSetupBean;
        this.questionsList = questionsList;
        this.printingPaperType = printingPaperType;
        this.selectedSubjectList = selectedSubjectList;
        totalMarks = 0;
        pdfSecondFormatBean = null;
        this.quesPageObject = quesPageObject;
        this.wordPageSetup = wordPageSetup;
        groupName = new ProcessManager().getGroupName().trim();
        loadSettings();
    }
    
    private void loadSettings() {
        CmbDivDep.removeAllItems();
        CmbDivDep.addItem("XII - A Div");
        CmbDivDep.addItem("XII - B Div");
        CmbDivDep.addItem("XII - C Div");
        CmbDivDep.addItem("XII - D Div");
        CmbDivDep.addItem("XII - E Div");
        CmbDivDep.addItem("XII - F Div");
        CmbDivDep.addItem("XII - G Div");
        CmbDivDep.addItem("XII - H Div");
        CmbDivDep.addItem(groupName+" CELL");
        if(printingPaperType.equalsIgnoreCase("ChapterWise") || printingPaperType.equalsIgnoreCase("UnitWise") || printingPaperType.equalsIgnoreCase("SubjectWise")) {
            LblSubject.setText("Subject Name");
            String subjectOne = selectedSubjectList.get(0).getSubjectName().trim();
            CmbSubject.removeAllItems();
            CmbSubject.addItem(subjectOne);
            CmbSubject.addItem(subjectOne+"-I");
            CmbSubject.addItem(subjectOne+"-II");
            CmbSubject.addItem(groupName +" "+ subjectOne);
            if(printingPaperType.equalsIgnoreCase("ChapterWise"))
                CmbSubject.addItem(subjectOne+" : "+chaptersList.get(0).getChapterName().trim());
            else if(printingPaperType.equalsIgnoreCase("UnitWise"))
                CmbSubject.addItem(subjectOne+" Units Test");
            else
                CmbSubject.addItem(subjectOne+" Subject Test");
            CmbSubject.setSelectedIndex(4);
            
            CmbDivDep.addItem(subjectOne+" Department");
            
            LblSubFirst.setText(subjectOne);
            subFirstQuesCount = selectedQuestionList.size();
            subSecondQuesCount = 0;
            subThirdQuesCount = 0;
            
            LblSubFirstTotal.setText(""+subFirstQuesCount);
            LblSubSecond.setVisible(false);
            LblSubSecondTotal.setVisible(false);
            LblSubSecondMultiply.setVisible(false);
            CmbSecondSubjectTotal.setVisible(false);
            LblSubSecondEqualTo.setVisible(false);
            TxtSecondSubTotal.setVisible(false);
            
            LblSubThird.setVisible(false);
            LblSubThirdTotal.setVisible(false);
            LblSubThirdMultiply.setVisible(false);
            CmbThirdSubjectTotal.setVisible(false);
            LblSubThirdEqualTo.setVisible(false);
            TxtThirdSubTotal.setVisible(false);
            
        } else {
            
            CmbSubject.removeAllItems();
            LblSubject.setText("Group Name");
            if(printingPaperType.equalsIgnoreCase("GroupWise")) {
                LblSubject.setText("Group Name");
                CmbSubject.addItem(groupName+" Group Test");
            } else if(printingPaperType.equalsIgnoreCase("YearWise")) {
                LblSubject.setText("Year Name");
                CmbSubject.addItem(groupName+" Old Paper Test");
            }
            
            subFirstQuesCount = 0;
            subSecondQuesCount = 0;
            subThirdQuesCount = 0;
            
            for (QuestionBean questionsBean : selectedQuestionList) {
                if(questionsBean.getSubjectId() == selectedSubjectList.get(0).getSubjectId())
                    subFirstQuesCount ++;
                else if(questionsBean.getSubjectId() == selectedSubjectList.get(1).getSubjectId())
                    subSecondQuesCount ++;
                else if(questionsBean.getSubjectId() == selectedSubjectList.get(2).getSubjectId())
                    subThirdQuesCount ++;
            }
            LblSubFirst.setText(selectedSubjectList.get(0).getSubjectName().trim());
            LblSubSecond.setText(selectedSubjectList.get(1).getSubjectName().trim());
            LblSubThird.setText(selectedSubjectList.get(2).getSubjectName().trim());
            
            LblSubFirstTotal.setText(""+subFirstQuesCount);
            LblSubSecondTotal.setText(""+subSecondQuesCount);
            LblSubThirdTotal.setText(""+subThirdQuesCount);
        }
        setTotalValues();
        CmbTime.setSelectedIndex(3);
    }
    
    private PdfSecondFormatBean getSetupBean() {
        PdfSecondFormatBean returnBean = new PdfSecondFormatBean();
        returnBean.setSubjectName(CmbSubject.getSelectedItem().toString());
        returnBean.setSelectedTime(CmbTime.getSelectedItem().toString());
        returnBean.setDivisionName(CmbDivDep.getSelectedItem().toString());
        returnBean.setTotalMarks(totalMarks);
        return returnBean;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        HeaderPanel = new javax.swing.JPanel();
        LblHeaderMsg = new javax.swing.JLabel();
        LeftBodyPanel = new javax.swing.JPanel();
        LblSubject = new javax.swing.JLabel();
        CmbSubject = new javax.swing.JComboBox();
        LblTime = new javax.swing.JLabel();
        CmbTime = new javax.swing.JComboBox();
        MiddleBodyPanel = new javax.swing.JPanel();
        LblImage = new javax.swing.JLabel();
        RightBodyPanel = new javax.swing.JPanel();
        LblTotalMarks = new javax.swing.JLabel();
        SubFirstPanel = new javax.swing.JPanel();
        LblSubFirst = new javax.swing.JLabel();
        LblSubFirstMultiply = new javax.swing.JLabel();
        CmbFirstSubjectTotal = new javax.swing.JComboBox();
        LblSubFirstEqualTo = new javax.swing.JLabel();
        TxtFirstSubTotal = new javax.swing.JTextField();
        LblSubFirstTotal = new javax.swing.JLabel();
        SubSecondPanel = new javax.swing.JPanel();
        LblSubSecond = new javax.swing.JLabel();
        LblSubSecondMultiply = new javax.swing.JLabel();
        CmbSecondSubjectTotal = new javax.swing.JComboBox();
        LblSubSecondEqualTo = new javax.swing.JLabel();
        TxtSecondSubTotal = new javax.swing.JTextField();
        LblSubSecondTotal = new javax.swing.JLabel();
        SubThirdPanel = new javax.swing.JPanel();
        LblSubThird = new javax.swing.JLabel();
        LblSubThirdMultiply = new javax.swing.JLabel();
        CmbThirdSubjectTotal = new javax.swing.JComboBox();
        LblSubThirdEqualTo = new javax.swing.JLabel();
        TxtThirdSubTotal = new javax.swing.JTextField();
        LblSubThirdTotal = new javax.swing.JLabel();
        BodyFooterPanel = new javax.swing.JPanel();
        LblDivDep = new javax.swing.JLabel();
        CmbDivDep = new javax.swing.JComboBox();
        jPanel5 = new javax.swing.JPanel();
        BtnBack = new javax.swing.JButton();
        BtnNext = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setAlwaysOnTop(true);
        setBackground(new java.awt.Color(255, 255, 255));
        setFocusCycleRoot(false);
        setForeground(new java.awt.Color(255, 255, 255));
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        HeaderPanel.setBackground(new java.awt.Color(11, 45, 55));
        HeaderPanel.setName("HeaderPanel"); // NOI18N

        LblHeaderMsg.setBackground(new java.awt.Color(11, 45, 55));
        LblHeaderMsg.setFont(new java.awt.Font("Calibri", 0, 32)); // NOI18N
        LblHeaderMsg.setForeground(new java.awt.Color(255, 255, 255));
        LblHeaderMsg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblHeaderMsg.setText("Customize First Page");
        LblHeaderMsg.setName("LblHeaderMsg"); // NOI18N

        javax.swing.GroupLayout HeaderPanelLayout = new javax.swing.GroupLayout(HeaderPanel);
        HeaderPanel.setLayout(HeaderPanelLayout);
        HeaderPanelLayout.setHorizontalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblHeaderMsg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        HeaderPanelLayout.setVerticalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, HeaderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LblHeaderMsg, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
                .addContainerGap())
        );

        LeftBodyPanel.setBackground(new java.awt.Color(11, 45, 55));
        LeftBodyPanel.setName("LeftBodyPanel"); // NOI18N
        LeftBodyPanel.setPreferredSize(new java.awt.Dimension(265, 155));

        LblSubject.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        LblSubject.setForeground(new java.awt.Color(255, 255, 255));
        LblSubject.setText("Subject Name");
        LblSubject.setName("LblSubject"); // NOI18N

        CmbSubject.setEditable(true);
        CmbSubject.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbSubject.setToolTipText("Marks Per Question");
        CmbSubject.setName("CmbSubject"); // NOI18N

        LblTime.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        LblTime.setForeground(new java.awt.Color(255, 255, 255));
        LblTime.setText("Time");
        LblTime.setName("LblTime"); // NOI18N

        CmbTime.setEditable(true);
        CmbTime.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbTime.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "15  min", "30  min", "45  min", "1.00  hr", "1.30  hr", "2.00  hr", "2.30  hr", "3.00  hr" }));
        CmbTime.setToolTipText("Marks Per Question");
        CmbTime.setName("CmbTime"); // NOI18N

        javax.swing.GroupLayout LeftBodyPanelLayout = new javax.swing.GroupLayout(LeftBodyPanel);
        LeftBodyPanel.setLayout(LeftBodyPanelLayout);
        LeftBodyPanelLayout.setHorizontalGroup(
            LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LeftBodyPanelLayout.createSequentialGroup()
                .addGroup(LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(LeftBodyPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(LeftBodyPanelLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(CmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(LblSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(LblTime, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(LeftBodyPanelLayout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addComponent(CmbTime, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(39, Short.MAX_VALUE))
        );
        LeftBodyPanelLayout.setVerticalGroup(
            LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LeftBodyPanelLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(LblSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblTime, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CmbTime)
                .addGap(28, 28, 28))
        );

        MiddleBodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        MiddleBodyPanel.setName("MiddleBodyPanel"); // NOI18N

        LblImage.setBackground(new java.awt.Color(255, 255, 255));
        LblImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/FormatSecondNextPage.png"))); // NOI18N
        LblImage.setName("LblImage"); // NOI18N

        javax.swing.GroupLayout MiddleBodyPanelLayout = new javax.swing.GroupLayout(MiddleBodyPanel);
        MiddleBodyPanel.setLayout(MiddleBodyPanelLayout);
        MiddleBodyPanelLayout.setHorizontalGroup(
            MiddleBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblImage, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        MiddleBodyPanelLayout.setVerticalGroup(
            MiddleBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MiddleBodyPanelLayout.createSequentialGroup()
                .addComponent(LblImage)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        RightBodyPanel.setBackground(new java.awt.Color(11, 45, 55));
        RightBodyPanel.setName("RightBodyPanel"); // NOI18N
        RightBodyPanel.setPreferredSize(new java.awt.Dimension(265, 155));

        LblTotalMarks.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        LblTotalMarks.setForeground(new java.awt.Color(255, 255, 255));
        LblTotalMarks.setText("Total Marks");
        LblTotalMarks.setName("LblTotalMarks"); // NOI18N

        SubFirstPanel.setBackground(new java.awt.Color(11, 45, 55));
        SubFirstPanel.setName("SubFirstPanel"); // NOI18N

        LblSubFirst.setBackground(new java.awt.Color(255, 255, 255));
        LblSubFirst.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubFirst.setForeground(new java.awt.Color(255, 255, 255));
        LblSubFirst.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LblSubFirst.setText("Physics");
        LblSubFirst.setName("LblSubFirst"); // NOI18N
        LblSubFirst.setPreferredSize(new java.awt.Dimension(94, 27));

        LblSubFirstMultiply.setBackground(new java.awt.Color(255, 255, 255));
        LblSubFirstMultiply.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubFirstMultiply.setForeground(new java.awt.Color(255, 255, 255));
        LblSubFirstMultiply.setText("*");
        LblSubFirstMultiply.setName("LblSubFirstMultiply"); // NOI18N

        CmbFirstSubjectTotal.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbFirstSubjectTotal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        CmbFirstSubjectTotal.setToolTipText("Marks Per Question");
        CmbFirstSubjectTotal.setName("CmbFirstSubjectTotal"); // NOI18N
        CmbFirstSubjectTotal.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbFirstSubjectTotalItemStateChanged(evt);
            }
        });

        LblSubFirstEqualTo.setBackground(new java.awt.Color(255, 255, 255));
        LblSubFirstEqualTo.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubFirstEqualTo.setForeground(new java.awt.Color(255, 255, 255));
        LblSubFirstEqualTo.setText("=");
        LblSubFirstEqualTo.setName("LblSubFirstEqualTo"); // NOI18N

        TxtFirstSubTotal.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        TxtFirstSubTotal.setName("TxtFirstSubTotal"); // NOI18N

        LblSubFirstTotal.setBackground(new java.awt.Color(255, 255, 255));
        LblSubFirstTotal.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubFirstTotal.setForeground(new java.awt.Color(255, 255, 255));
        LblSubFirstTotal.setText("999");
        LblSubFirstTotal.setName("LblSubFirstTotal"); // NOI18N

        javax.swing.GroupLayout SubFirstPanelLayout = new javax.swing.GroupLayout(SubFirstPanel);
        SubFirstPanel.setLayout(SubFirstPanelLayout);
        SubFirstPanelLayout.setHorizontalGroup(
            SubFirstPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SubFirstPanelLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(LblSubFirst, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblSubFirstTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LblSubFirstMultiply)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CmbFirstSubjectTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblSubFirstEqualTo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TxtFirstSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        SubFirstPanelLayout.setVerticalGroup(
            SubFirstPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SubFirstPanelLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(SubFirstPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(SubFirstPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(LblSubFirstMultiply, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(CmbFirstSubjectTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(LblSubFirstEqualTo, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(TxtFirstSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(LblSubFirstTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(LblSubFirst, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2))
        );

        LblSubFirstMultiply.getAccessibleContext().setAccessibleName("");
        LblSubFirstMultiply.getAccessibleContext().setAccessibleDescription("");
        CmbFirstSubjectTotal.getAccessibleContext().setAccessibleName("");
        CmbFirstSubjectTotal.getAccessibleContext().setAccessibleDescription("");
        LblSubFirstEqualTo.getAccessibleContext().setAccessibleName("");
        LblSubFirstEqualTo.getAccessibleContext().setAccessibleDescription("");
        TxtFirstSubTotal.getAccessibleContext().setAccessibleName("");
        TxtFirstSubTotal.getAccessibleContext().setAccessibleDescription("");

        SubSecondPanel.setBackground(new java.awt.Color(11, 45, 55));
        SubSecondPanel.setName("SubSecondPanel"); // NOI18N

        LblSubSecond.setBackground(new java.awt.Color(255, 255, 255));
        LblSubSecond.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubSecond.setForeground(new java.awt.Color(255, 255, 255));
        LblSubSecond.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LblSubSecond.setText("Chemistry");
        LblSubSecond.setName("LblSubSecond"); // NOI18N
        LblSubSecond.setPreferredSize(new java.awt.Dimension(94, 27));

        LblSubSecondMultiply.setBackground(new java.awt.Color(255, 255, 255));
        LblSubSecondMultiply.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubSecondMultiply.setForeground(new java.awt.Color(255, 255, 255));
        LblSubSecondMultiply.setText("*");
        LblSubSecondMultiply.setName("LblSubSecondMultiply"); // NOI18N

        CmbSecondSubjectTotal.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbSecondSubjectTotal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        CmbSecondSubjectTotal.setToolTipText("Marks Per Question");
        CmbSecondSubjectTotal.setName("CmbSecondSubjectTotal"); // NOI18N
        CmbSecondSubjectTotal.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbSecondSubjectTotalItemStateChanged(evt);
            }
        });

        LblSubSecondEqualTo.setBackground(new java.awt.Color(255, 255, 255));
        LblSubSecondEqualTo.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubSecondEqualTo.setForeground(new java.awt.Color(255, 255, 255));
        LblSubSecondEqualTo.setText("=");
        LblSubSecondEqualTo.setName("LblSubSecondEqualTo"); // NOI18N

        TxtSecondSubTotal.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        TxtSecondSubTotal.setName("TxtSecondSubTotal"); // NOI18N

        LblSubSecondTotal.setBackground(new java.awt.Color(255, 255, 255));
        LblSubSecondTotal.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubSecondTotal.setForeground(new java.awt.Color(255, 255, 255));
        LblSubSecondTotal.setText("999");
        LblSubSecondTotal.setName("LblSubSecondTotal"); // NOI18N

        javax.swing.GroupLayout SubSecondPanelLayout = new javax.swing.GroupLayout(SubSecondPanel);
        SubSecondPanel.setLayout(SubSecondPanelLayout);
        SubSecondPanelLayout.setHorizontalGroup(
            SubSecondPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SubSecondPanelLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(LblSubSecond, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblSubSecondTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LblSubSecondMultiply)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CmbSecondSubjectTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblSubSecondEqualTo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TxtSecondSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        SubSecondPanelLayout.setVerticalGroup(
            SubSecondPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SubSecondPanelLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(SubSecondPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(SubSecondPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(LblSubSecondMultiply, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(CmbSecondSubjectTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(LblSubSecondEqualTo, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(TxtSecondSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(LblSubSecondTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(LblSubSecond, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2))
        );

        SubThirdPanel.setBackground(new java.awt.Color(11, 45, 55));
        SubThirdPanel.setName("SubThirdPanel"); // NOI18N

        LblSubThird.setBackground(new java.awt.Color(255, 255, 255));
        LblSubThird.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubThird.setForeground(new java.awt.Color(255, 255, 255));
        LblSubThird.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LblSubThird.setText("Mathematics");
        LblSubThird.setName("LblSubThird"); // NOI18N
        LblSubThird.setPreferredSize(new java.awt.Dimension(94, 27));

        LblSubThirdMultiply.setBackground(new java.awt.Color(255, 255, 255));
        LblSubThirdMultiply.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubThirdMultiply.setForeground(new java.awt.Color(255, 255, 255));
        LblSubThirdMultiply.setText("*");
        LblSubThirdMultiply.setName("LblSubThirdMultiply"); // NOI18N

        CmbThirdSubjectTotal.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbThirdSubjectTotal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        CmbThirdSubjectTotal.setToolTipText("Marks Per Question");
        CmbThirdSubjectTotal.setName("CmbThirdSubjectTotal"); // NOI18N
        CmbThirdSubjectTotal.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbThirdSubjectTotalItemStateChanged(evt);
            }
        });

        LblSubThirdEqualTo.setBackground(new java.awt.Color(255, 255, 255));
        LblSubThirdEqualTo.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubThirdEqualTo.setForeground(new java.awt.Color(255, 255, 255));
        LblSubThirdEqualTo.setText("=");
        LblSubThirdEqualTo.setName("LblSubThirdEqualTo"); // NOI18N

        TxtThirdSubTotal.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        TxtThirdSubTotal.setName("TxtThirdSubTotal"); // NOI18N

        LblSubThirdTotal.setBackground(new java.awt.Color(255, 255, 255));
        LblSubThirdTotal.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubThirdTotal.setForeground(new java.awt.Color(255, 255, 255));
        LblSubThirdTotal.setText("999");
        LblSubThirdTotal.setName("LblSubThirdTotal"); // NOI18N

        javax.swing.GroupLayout SubThirdPanelLayout = new javax.swing.GroupLayout(SubThirdPanel);
        SubThirdPanel.setLayout(SubThirdPanelLayout);
        SubThirdPanelLayout.setHorizontalGroup(
            SubThirdPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SubThirdPanelLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(LblSubThird, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblSubThirdTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LblSubThirdMultiply)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CmbThirdSubjectTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblSubThirdEqualTo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TxtThirdSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        SubThirdPanelLayout.setVerticalGroup(
            SubThirdPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SubThirdPanelLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(SubThirdPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(SubThirdPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(LblSubThirdMultiply, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(CmbThirdSubjectTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(LblSubThirdEqualTo, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(TxtThirdSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(LblSubThirdTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(LblSubThird, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2))
        );

        javax.swing.GroupLayout RightBodyPanelLayout = new javax.swing.GroupLayout(RightBodyPanel);
        RightBodyPanel.setLayout(RightBodyPanelLayout);
        RightBodyPanelLayout.setHorizontalGroup(
            RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RightBodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LblTotalMarks, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(SubThirdPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(SubSecondPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(SubFirstPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        RightBodyPanelLayout.setVerticalGroup(
            RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RightBodyPanelLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(LblTotalMarks, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SubFirstPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(SubSecondPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SubThirdPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15))
        );

        BodyFooterPanel.setBackground(new java.awt.Color(11, 45, 55));
        BodyFooterPanel.setName("BodyFooterPanel"); // NOI18N

        LblDivDep.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        LblDivDep.setForeground(new java.awt.Color(255, 255, 255));
        LblDivDep.setText("Division / Department");
        LblDivDep.setName("LblDivDep"); // NOI18N

        CmbDivDep.setEditable(true);
        CmbDivDep.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbDivDep.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "XII - A Div", "XII - B Div", "XII - C Div", "XII - D Div", "XII - E Div", "XII - F Div", "XII - G Div", "XII - H Div", "MH CET", "JEE", "NEET", "Medical CET", "JEE MAIN", "JEE Advance" }));
        CmbDivDep.setToolTipText("Marks Per Question");
        CmbDivDep.setName("CmbDivDep"); // NOI18N

        javax.swing.GroupLayout BodyFooterPanelLayout = new javax.swing.GroupLayout(BodyFooterPanel);
        BodyFooterPanel.setLayout(BodyFooterPanelLayout);
        BodyFooterPanelLayout.setHorizontalGroup(
            BodyFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyFooterPanelLayout.createSequentialGroup()
                .addGap(262, 262, 262)
                .addComponent(LblDivDep, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(CmbDivDep, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(358, 358, 358))
        );
        BodyFooterPanelLayout.setVerticalGroup(
            BodyFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyFooterPanelLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(BodyFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblDivDep, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CmbDivDep, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );

        jPanel5.setBackground(new java.awt.Color(11, 45, 55));
        jPanel5.setName("jPanel5"); // NOI18N

        BtnBack.setBackground(new java.awt.Color(208, 87, 96));
        BtnBack.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        BtnBack.setForeground(new java.awt.Color(255, 255, 255));
        BtnBack.setText("Back");
        BtnBack.setName("BtnBack"); // NOI18N
        BtnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackActionPerformed(evt);
            }
        });

        BtnNext.setBackground(new java.awt.Color(208, 87, 96));
        BtnNext.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        BtnNext.setForeground(new java.awt.Color(255, 255, 255));
        BtnNext.setText("Next");
        BtnNext.setName("BtnNext"); // NOI18N
        BtnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnNextActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(202, 202, 202)
                .addComponent(BtnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(406, 406, 406)
                .addComponent(BtnNext, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnNext, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 1, Short.MAX_VALUE)
                        .addComponent(LeftBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(MiddleBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(RightBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(BodyFooterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 1108, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(HeaderPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(HeaderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(LeftBodyPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
                    .addComponent(MiddleBodyPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(RightBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 157, Short.MAX_VALUE))
                .addGap(11, 11, 11)
                .addComponent(BodyFooterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackActionPerformed
        // TODO add your handling code here:
        wordPageSetup.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnBackActionPerformed

    private void CmbFirstSubjectTotalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbFirstSubjectTotalItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED)
            setTotalValues();
    }//GEN-LAST:event_CmbFirstSubjectTotalItemStateChanged


    private void setTotalValues() {
        int valueOne = 0;
        valueOne = (CmbFirstSubjectTotal.getSelectedIndex() + 1) * subFirstQuesCount;
        TxtFirstSubTotal.setText("" + valueOne);
        
        int valueTwo = 0;
        int valueThree = 0;
        if(printingPaperType.equalsIgnoreCase("GroupWise") || printingPaperType.equalsIgnoreCase("YearWise")) {
            valueTwo = (CmbSecondSubjectTotal.getSelectedIndex() + 1) * subSecondQuesCount;
            TxtSecondSubTotal.setText("" + valueTwo);
            valueThree = (CmbThirdSubjectTotal.getSelectedIndex() + 1) * subThirdQuesCount;
            TxtThirdSubTotal.setText("" + valueThree);
        }
        totalMarks = valueOne + valueTwo + valueThree;
    }
    
    private void BtnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnNextActionPerformed
        // TODO add your handling code here:
        pdfSecondFormatBean = getSetupBean();
        if(pdfSecondFormatBean != null) {
            new WordFinalPanel(wordPageSetupBean, pdfSecondFormatBean, selectedQuestionList, selectedSubjectList, printingPaperType, totalMarks, questionsList, this, quesPageObject, wordPageSetup).setVisible(true);
            this.setVisible(false);
        }
    }//GEN-LAST:event_BtnNextActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
    // TODO add your handling code here:
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(rootPane, "Are You Sure go to Home Page?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            if(SingleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
                SingleChapterQuestionsSelection frm = (SingleChapterQuestionsSelection)quesPageObject;
                frm.dispose();
            } else if(MultipleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
                MultipleChapterQuestionsSelection frm = (MultipleChapterQuestionsSelection)quesPageObject;
                frm.dispose();
            } else if(MultipleYearQuestionsSelection.class.isInstance(quesPageObject)) {
                MultipleYearQuestionsSelection frm = (MultipleYearQuestionsSelection)quesPageObject;
                frm.dispose();
            }
            wordPageSetup.dispose();
            new HomePage().setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_formWindowClosing

    private void CmbSecondSubjectTotalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbSecondSubjectTotalItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) 
            setTotalValues();
    }//GEN-LAST:event_CmbSecondSubjectTotalItemStateChanged

    private void CmbThirdSubjectTotalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbThirdSubjectTotalItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) 
            setTotalValues();
    }//GEN-LAST:event_CmbThirdSubjectTotalItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(QuestFirstPageSettingA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(QuestFirstPageSettingA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(QuestFirstPageSettingA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(QuestFirstPageSettingA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>
        /* Create and display the form */

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BodyFooterPanel;
    private javax.swing.JButton BtnBack;
    private javax.swing.JButton BtnNext;
    private javax.swing.JComboBox CmbDivDep;
    private javax.swing.JComboBox CmbFirstSubjectTotal;
    private javax.swing.JComboBox CmbSecondSubjectTotal;
    private javax.swing.JComboBox CmbSubject;
    private javax.swing.JComboBox CmbThirdSubjectTotal;
    private javax.swing.JComboBox CmbTime;
    private javax.swing.JPanel HeaderPanel;
    private javax.swing.JLabel LblDivDep;
    private javax.swing.JLabel LblHeaderMsg;
    private javax.swing.JLabel LblImage;
    private javax.swing.JLabel LblSubFirst;
    private javax.swing.JLabel LblSubFirstEqualTo;
    private javax.swing.JLabel LblSubFirstMultiply;
    private javax.swing.JLabel LblSubFirstTotal;
    private javax.swing.JLabel LblSubSecond;
    private javax.swing.JLabel LblSubSecondEqualTo;
    private javax.swing.JLabel LblSubSecondMultiply;
    private javax.swing.JLabel LblSubSecondTotal;
    private javax.swing.JLabel LblSubThird;
    private javax.swing.JLabel LblSubThirdEqualTo;
    private javax.swing.JLabel LblSubThirdMultiply;
    private javax.swing.JLabel LblSubThirdTotal;
    private javax.swing.JLabel LblSubject;
    private javax.swing.JLabel LblTime;
    private javax.swing.JLabel LblTotalMarks;
    private javax.swing.JPanel LeftBodyPanel;
    private javax.swing.JPanel MiddleBodyPanel;
    private javax.swing.JPanel RightBodyPanel;
    private javax.swing.JPanel SubFirstPanel;
    private javax.swing.JPanel SubSecondPanel;
    private javax.swing.JPanel SubThirdPanel;
    private javax.swing.JTextField TxtFirstSubTotal;
    private javax.swing.JTextField TxtSecondSubTotal;
    private javax.swing.JTextField TxtThirdSubTotal;
    private javax.swing.JPanel jPanel5;
    // End of variables declaration//GEN-END:variables
}

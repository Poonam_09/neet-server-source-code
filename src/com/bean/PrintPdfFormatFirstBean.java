/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class PrintPdfFormatFirstBean {
    private String pageType;
    private String pageBorder;
    private String headerFooterOnFirstPage;
    private String waterMark;
    private String clgDepartment;
    private String fourFields;
    private String rollDivisionTime;
    private String paperName;
    private String mainString;
    private String headerLeft;
    private String headerCenter;
    private String headerRight;
    private String footerLeft;
    private String footerCenter;
    private String footerRight;

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public String getPageBorder() {
        return pageBorder;
    }

    public void setPageBorder(String pageBorder) {
        this.pageBorder = pageBorder;
    }

    public String getHeaderFooterOnFirstPage() {
        return headerFooterOnFirstPage;
    }

    public void setHeaderFooterOnFirstPage(String headerFooterOnFirstPage) {
        this.headerFooterOnFirstPage = headerFooterOnFirstPage;
    }

    public String getWaterMark() {
        return waterMark;
    }

    public void setWaterMark(String waterMark) {
        this.waterMark = waterMark;
    }

    public String getClgDepartment() {
        return clgDepartment;
    }

    public void setClgDepartment(String clgDepartment) {
        this.clgDepartment = clgDepartment;
    }

    public String getFourFields() {
        return fourFields;
    }

    public void setFourFields(String fourFields) {
        this.fourFields = fourFields;
    }

    public String getRollDivisionTime() {
        return rollDivisionTime;
    }

    public void setRollDivisionTime(String rollDivisionTime) {
        this.rollDivisionTime = rollDivisionTime;
    }

    public String getPaperName() {
        return paperName;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    public String getMainString() {
        return mainString;
    }

    public void setMainString(String mainString) {
        this.mainString = mainString;
    }

    public String getHeaderLeft() {
        return headerLeft;
    }

    public void setHeaderLeft(String headerLeft) {
        this.headerLeft = headerLeft;
    }

    public String getHeaderCenter() {
        return headerCenter;
    }

    public void setHeaderCenter(String headerCenter) {
        this.headerCenter = headerCenter;
    }

    public String getHeaderRight() {
        return headerRight;
    }

    public void setHeaderRight(String headerRight) {
        this.headerRight = headerRight;
    }

    public String getFooterLeft() {
        return footerLeft;
    }

    public void setFooterLeft(String footerLeft) {
        this.footerLeft = footerLeft;
    }

    public String getFooterCenter() {
        return footerCenter;
    }

    public void setFooterCenter(String footerCenter) {
        this.footerCenter = footerCenter;
    }

    public String getFooterRight() {
        return footerRight;
    }

    public void setFooterRight(String footerRight) {
        this.footerRight = footerRight;
    }
    
    
}

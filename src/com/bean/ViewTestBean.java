/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;
/**
 *
 * @author Aniket
 */
public class ViewTestBean {
    private String titleName;
    private String selectedSubjects;
    private String saveTime;
    private int totalQuestions;
    private PrintedTestBean printedTestBean; 

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public String getSelectedSubjects() {
        return selectedSubjects;
    }

    public void setSelectedSubjects(String selectedSubjects) {
        this.selectedSubjects = selectedSubjects;
    }

    public String getSaveTime() {
        return saveTime;
    }

    public void setSaveTime(String saveTime) {
        this.saveTime = saveTime;
    }

    public int getTotalQuestions() {
        return totalQuestions;
    }

    public void setTotalQuestions(int totalQuestions) {
        this.totalQuestions = totalQuestions;
    }

    public PrintedTestBean getPrintedTestBean() {
        return printedTestBean;
    }

    public void setPrintedTestBean(PrintedTestBean printedTestBean) {
        this.printedTestBean = printedTestBean;
    }
}

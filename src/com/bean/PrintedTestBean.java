/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

import java.sql.Timestamp;

/**
 *
 * @author Aniket
 */
public class PrintedTestBean implements Comparable<PrintedTestBean>{
    private int printedTestId;
    private String testName;
    private String quesIds;
    private int totalQues;
    private String quesType;
    private String subjectIds;
    private Timestamp testDateTime;
    private byte[] questionListObject;

    public int getPrintedTestId() {
        return printedTestId;
    }

    public void setPrintedTestId(int printedTestId) {
        this.printedTestId = printedTestId;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getQuesIds() {
        return quesIds;
    }

    public void setQuesIds(String quesIds) {
        this.quesIds = quesIds;
    }

    public int getTotalQues() {
        return totalQues;
    }

    public void setTotalQues(int totalQues) {
        this.totalQues = totalQues;
    }

    public String getQuesType() {
        return quesType;
    }

    public void setQuesType(String quesType) {
        this.quesType = quesType;
    }

    public String getSubjectIds() {
        return subjectIds;
    }

    public void setSubjectIds(String subjectIds) {
        this.subjectIds = subjectIds;
    }

    public Timestamp getTestDateTime() {
        return testDateTime;
    }

    public void setTestDateTime(Timestamp testDateTime) {
        this.testDateTime = testDateTime;
    }

    public byte[] getQuestionListObject() {
        return questionListObject;
    }

    public void setQuestionListObject(byte[] questionListObject) {
        this.questionListObject = questionListObject;
    }

    @Override
    public int compareTo(PrintedTestBean printedTestBean) {
        if(printedTestId == printedTestBean.printedTestId)
            return 0;
        else if(printedTestId < printedTestBean.printedTestId)
            return 1;
        else
            return -1;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author 007
 */
//rollno INTEGER NOT NULL,fullname VARCHAR(100) NOT NULL,pass VARCHAR(100) NOT NULL,standard INTEGER NOT NULL,division VARCHAR(30) NOT NULL,academicYear VARCHAR(30) NOT NULL,PRIMARY KEY (rollno))");
public class StudentBean implements Comparable<StudentBean> {

    int rollno;
    String fullname, pass, division, academicYear,standard;
    String StudentName;
    String MobileNo;

    
    public StudentBean(int rollno, String fullname, String pass, String standard, String division, String academicYear,String StudentName,String MobileNo) {
        this.rollno = rollno;
        this.standard = standard;
        this.fullname = fullname;
        this.academicYear = academicYear;
        this.pass = pass;
        this.division = division;
        this.StudentName=StudentName;
        this.MobileNo=MobileNo;
    }   

    public StudentBean() {
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getFullname() {  
        return fullname;   //form name rollno or UserId.
    }

    public void setFullname(String fullname) { //form name rollno or UserId.
        this.fullname = fullname;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getRollno() {    //LoginId
        return rollno;
    }

    public void setRollno(int rollno) { //LoginId
        this.rollno = rollno;     
    }

    public String getStandard() { 
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }
    
    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String MobileNo) {
        this.MobileNo = MobileNo;
    }
    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String StudentName) {
        this.StudentName = StudentName;
    }

    @Override
    public int compareTo(StudentBean studentBean1) {
        if(rollno == studentBean1.rollno)
            return 0;
        else if(rollno < studentBean1.rollno)
            return 1;
        else
            return -1;
    }
}

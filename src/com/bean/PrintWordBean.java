/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class PrintWordBean {
    private String instituteName;
    private String testType;
    private String timePaperTotalText;
    private String mainString;

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public String getTimePaperTotalText() {
        return timePaperTotalText;
    }

    public void setTimePaperTotalText(String timePaperTotalText) {
        this.timePaperTotalText = timePaperTotalText;
    }

    public String getMainString() {
        return mainString;
    }

    public void setMainString(String mainString) {
        this.mainString = mainString;
    }
}

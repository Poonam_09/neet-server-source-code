/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattern.operations;

import com.bean.GroupBean;
import com.bean.MasterSubjectBean;
import com.db.operations.MasterGroupOperation;
import com.db.operations.QuestionPaperOperation;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Aniket
 */
public class PatternOperation {
    public boolean sortQuestions(int index) {
        boolean returnValue = false;
        ArrayList<GroupBean> groupList = new MasterGroupOperation().getGroupList();
         System.out.println("index" +index);
         System.out.println("groupList.size()"+groupList.size());
        GroupBean selectedGroupBean = groupList.get(index);
        System.out.println(""+selectedGroupBean.getGroupId()+" "+selectedGroupBean.getGroupName());
             
        ArrayList<MasterSubjectBean> selectedSubjectList = new ArrayList<MasterSubjectBean>();
        selectedSubjectList.add(selectedGroupBean.getSubjectList().get(0));
        selectedSubjectList.add(selectedGroupBean.getSubjectList().get(1));
        selectedSubjectList.add(selectedGroupBean.getSubjectList().get(2));
        
        
        if (new SortDataOperation().sortData(selectedGroupBean, selectedSubjectList)) {
            new QuestionPaperOperation().setInitialUsedValue();
            new QuesPaperImageRatioOperation().addQuesPaperRatio();
//            JOptionPane.showMessageDialog(null, "Question Sorted Successfully.");
            System.out.println("Question Sorted Successfully.");
            returnValue = true;
        } else {
//            JOptionPane.showMessageDialog(null, "Error in Sort.");
            System.out.println("Error in Sort.");
            returnValue = false;
        }
        return returnValue;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattern.operations;

import com.bean.ChangeIdReferenceBean;
import com.bean.ChapterBean;
import com.bean.GroupBean;
import com.bean.MasterSubjectBean;
import com.bean.PatternOptionImageDimensionsBean;
import com.bean.QuestionBean;
import com.bean.SubMasterChapterBean;
import com.bean.SubMasterChapterIdReferenceBean;
import com.bean.SubjectBean;
import com.db.operations.ChapterOperation;
import com.db.operations.MasterOptionImageDimentionOperation;
import com.db.operations.MasterQuestionOperation;
import com.db.operations.MasterSubjectOperation;
import com.db.operations.OptionImageDimentionOperation;
import com.db.operations.QuestionOperation;
import com.db.operations.SubMasterChapterOperation;
import com.db.operations.SubjectOperation;
import com.id.operations.NewIdOperation;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Aniket
 */
public class SortDataOperation {
//    private ArrayList<SubMasterChapterBean> subMasterChapterList;
    private ArrayList<SubjectBean> finalSubjectList;
    private ArrayList<SubMasterChapterIdReferenceBean> changeChapterIdList;
    
    public boolean sortData(GroupBean groupBean,ArrayList<MasterSubjectBean> selectedMasterSubjectList) {
        new ClearData();
        boolean returnValue = false;
        changeChapterIdList = null;
        finalSubjectList = getSubjectList(selectedMasterSubjectList);//1
        ArrayList<ChapterBean> finalChapterList = getChapterList(groupBean);//2
        ArrayList<QuestionBean> questionList = new MasterQuestionOperation().getChapterWiseQuestionList(changeChapterIdList);
        ArrayList<ChangeIdReferenceBean> changeQuesIdList = null;
        if(finalSubjectList.size() != 0) {
            // Insert into Subject Table
            if(!new SubjectOperation().insertSubjectList(finalSubjectList))
                System.out.println("Error in Subject Insertion.");
//                JOptionPane.showMessageDialog(null, "Error in Subject Insertion.");
        }
        if(finalChapterList.size() != 0) {
            // Insert into Chapter Table
            if(!new ChapterOperation().insertChaptertList(finalChapterList))
                System.out.println("Error in Chapter Insertion.");
//                JOptionPane.showMessageDialog(null, "Error in Chapter Insertion.");
        }
        if(questionList != null) {
            //Insert into QuestionInfo
            changeQuesIdList = new QuestionOperation().insertQuestionList(questionList, changeChapterIdList);
            if(changeQuesIdList != null) {
                System.out.println("Question Insert Successfully.");
//                JOptionPane.showMessageDialog(null, "Question Insert Successfully.");

                ArrayList<PatternOptionImageDimensionsBean> masterOptionImageDimentionList = new MasterOptionImageDimentionOperation().getMasterDimentionList(questionList);

                if(!new OptionImageDimentionOperation().insertNewAndOldQuestion(masterOptionImageDimentionList, changeQuesIdList)) {
                    System.out.println("Error in Option Image Dimention.");
//                    JOptionPane.showMessageDialog(null, "Error in Option Image Dimention.");
                } else {
                    returnValue = true;
                }
            } else {
                System.out.println("Insertion in Questions.");
//                JOptionPane.showMessageDialog(null, "Insertion in Questions.");
            }
        }
        return returnValue;
    }
    
    private ArrayList<ChapterBean> getChapterList(GroupBean groupBean) {
        ArrayList<ChapterBean> returnList = new ArrayList<ChapterBean>();
        ArrayList<SubMasterChapterBean> subMasterChapterList = new SubMasterChapterOperation().getSubMasterChapterList();
        ChapterBean chapterBean = null;
        SubMasterChapterIdReferenceBean subMasterChapterIdReferenceBean = null;
        int chapterId = new NewIdOperation().getNewId("CHAPTER_INFO");
        for(SubjectBean subjectBean : finalSubjectList) {
            for(SubMasterChapterBean subMasterChapterBean : subMasterChapterList) {
                if(subMasterChapterBean.getSubjectBean().getSubjectId() == subjectBean.getSubjectId() 
                  && groupBean.getGroupId() == subMasterChapterBean.getGroupBean().getGroupId()) {//main condition
                    chapterBean = new ChapterBean();
                    chapterBean.setChapterId(chapterId);
                    chapterBean.setChapterName(subMasterChapterBean.getChapterName());
                    chapterBean.setSubjectBean(subjectBean);
                    chapterBean.setWeightage(subMasterChapterBean.getWeightage());
                    returnList.add(chapterBean);
                    
                    if(changeChapterIdList == null)
                        changeChapterIdList = new ArrayList<SubMasterChapterIdReferenceBean>();
                    
                    subMasterChapterIdReferenceBean = new SubMasterChapterIdReferenceBean();
                    subMasterChapterIdReferenceBean.setSubMasterChapterId(subMasterChapterBean.getChapterId());
                    subMasterChapterIdReferenceBean.setMasterChapterIds(subMasterChapterBean.getChapterIds());
                    subMasterChapterIdReferenceBean.setNewChapterID(chapterId++);
                    changeChapterIdList.add(subMasterChapterIdReferenceBean);
                }
            }
        }
        
        return returnList;
    }
    
    private ArrayList<SubjectBean> getSubjectList(ArrayList<MasterSubjectBean> selectedMasterSubjectList) {
        ArrayList<SubjectBean> returnList = new ArrayList<SubjectBean>();
        ArrayList<MasterSubjectBean> masterSubjectList = new MasterSubjectOperation().getSubjectList();
        SubjectBean subjectBean = null;
        for(MasterSubjectBean bean : masterSubjectList) {
            subjectBean = new SubjectBean();
            subjectBean.setSubjectId(bean.getSubjectId());
            subjectBean.setSubjectName(bean.getSubjectName());
            subjectBean.setMarksPerQuestion(4.0);//this column is required for student preparation software
            subjectBean.setMarksPerWrong(1.0);//this column is required for student preparation software
            boolean status = false;
            for(MasterSubjectBean msb : selectedMasterSubjectList) {
                if(msb.getSubjectId() == bean.getSubjectId()) {
                    status = true;
                    break;
                }
            }
            subjectBean.setStatus(status);
            returnList.add(subjectBean);
        }
        return returnList;
    }
}
